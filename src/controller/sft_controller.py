"""
File to control SFTs with and process them as SFTs.
"""
from typing import List, Set

from src.controller.graph_controller import union_graph_edges
from src.model.sft import SFT, Gate, BasicEvent


class SFTControllerError(Exception):
    """
    An error that occurs when controlling SFTs and processing them
    """


class SFTController:
    """
    `SFTToPIDConverter` objects have the goal of converting SFTs to P&IDs.
    For now, it only contains methods. This may be extended to contain
    properties or attributes which are parameters for these methods.
    """

    def __init__(self,
                 sfts: List[SFT]):
        """
        Creates an SFTController
        :param sfts: sfts to control
        """
        self._sfts: List[SFT] = sfts

    @property
    def sfts(self) -> List[SFT]:
        """
        Returns the sfts
        """
        return self._sfts

    @sfts.setter
    def sfts(self, sfts: List[SFT]):
        """
        Setter for the sfts of an SFTController
        :sfts: The sfts of an SFTController
        """
        self._sfts = sfts

    def pre_process_sfts(self) -> List[SFT]:
        """
        Pre-processes sfts before actually converting them to PIDs using an
        SFTToPIDConverter.
        :return: A list of pre-processed SFTS.
        """
        self.reduce_sfts()
        self.check_sfts_for_internal_transfer_gates()
        return self._sfts

    def check_sfts_for_internal_transfer_gates(self):
        """
        Checks the list of SFTs for internal transfer gates, meaning that
        e.g. an RHR SFT has a gate to another RHR SFT with a different ID.
        After that, it handles this transfer gate by making a union out of
        two SFTs.
        """
        continue_checking = True
        while continue_checking:
            continue_checking = False
            res_sfts: List[SFT] = []
            sfts_to_delete: Set[str] = set()
            te_labels = {s.sft_attributes.top_event.vertex_id for
                         s in self.sfts}
            for sft in self.sfts:
                self.check_sft_for_internal_transfer_gates(res_sfts, sft, sfts_to_delete, te_labels)
            for del_sft_label in sfts_to_delete:
                self.sfts.remove([s for s in self.sfts if s.graph_id == del_sft_label][0])
                continue_checking = True
            for res_sft in res_sfts:
                double_check = [s for s in self.sfts if s.graph_id == res_sft.graph_id]
                if len(double_check) > 0:
                    self.sfts.remove(double_check.pop())
                self.sfts.append(res_sft)
                continue_checking = True

    def check_sft_for_internal_transfer_gates(self,
                                              res_sfts: List[SFT],
                                              sft: SFT,
                                              sfts_to_delete: Set[str],
                                              te_labels: Set[str]):
        """
        Checks an SFT for internal transfer gates and merges them if possible.
        :param res_sfts: A list of SFTs to which merged SFTs can be added
        :param sft: The SFT which needs to be checked
        :param sfts_to_delete: A set of SFT labels to which SFTs can be added which should be
        deleted.
        :param te_labels: A set of TE labels to check against for transfer gates.
        """
        transfer_gates = [g.vertex_id for g in sft.sft_attributes.gates if
                          g.vertex_id != sft.sft_attributes.top_event.vertex_id
                          and g.vertex_id in te_labels]
        if len(transfer_gates) > 0:
            res_sft = sft
            for transfer_gate in transfer_gates:
                sft_to_merge = [s for s in self.sfts if
                                s.sft_attributes.top_event.vertex_id == transfer_gate][0]
                res_sft = union(res_sft, sft_to_merge)
                sfts_to_delete.add(sft_to_merge.graph_id)
            res_sfts.append(res_sft)

    def reduce_sfts(self):
        """
        Reduces the SFTs. For now, only prunes the vertices without incidence.
        """
        for sft in self.sfts:
            self.reduce_sft(sft, False)

    @staticmethod
    def reduce_sft(sft, prune_gates_without_children: bool):
        """
        Reduces an sft. For now, only 1) prunes vertices without incidence and
        2) prunes gates without children.
        :param sft: SFT to be reduced
        :param prune_gates_without_children: true if the children of a gate also
        needs removing, instead of only looking at incidence.
        """
        SFTController.delete_basic_events_without_incidence(sft)
        SFTController.delete_gates_without_incidence(sft)
        if prune_gates_without_children:
            SFTController.delete_gates_without_children(sft)

    @staticmethod
    def delete_gates_without_children(sft):
        """
        Deletes all Gates in an SFT without children.
        If a Gate does not have any children, it first deletes de gate and then the gate's edges.
        :param sft: The SFT of which the Gates should be cleaned up
        """
        gates = sft.sft_attributes.gates.copy()
        for gate in gates:
            adjacent_neighbours = [n for n in gate.neighbours if
                                   gate.graph.is_adjacent(gate, n)]
            if len(adjacent_neighbours) == 0:
                sft.delete_gate(gate)
                for edge in gate.incidence:
                    sft.del_edge(edge)

    @staticmethod
    def delete_gates_without_incidence(sft):
        """
        Deletes all Gates in an SFT without incidence to other vertices.
        :param sft: The SFT of which the Gates should be cleaned up
        """
        gates_to_delete = [gate for gate in
                           sft.sft_attributes.gates if
                           len(gate.incidence) == 0]
        for gate in gates_to_delete:
            sft.delete_gate(gate)

    @staticmethod
    def delete_basic_events_without_incidence(sft: SFT):
        """
        Deletes all BEs in an SFT without incidence to other vertices.
        :param sft: The SFT of which the BEs should be cleaned up
        """
        basic_events_to_delete = [be for be in
                                  sft.sft_attributes.basic_events if
                                  len(be.incidence) == 0]
        for basic_event in basic_events_to_delete:
            sft.delete_basic_event(basic_event)


def union(sft_a: SFT, sft_b: SFT) -> SFT:
    """
    Does the union operation on the two SFTs a and b.
    :param sft_a: SFT A
    :param sft_b: SFT B
    :return: SFT that is the union of input graphs a and b
    """
    sft = SFT(directed=True, sft_id=sft_a.graph_id)
    deep_copy_sft_basic_events(sft, sft_a, sft_b)
    deep_copy_sft_gates(sft, sft_a, sft_b)
    union_graph_edges(sft, sft_a, sft_b)
    return sft


def deep_copy_sft_basic_events(sft: SFT,
                               sft_a: SFT,
                               sft_b: SFT):
    """
    Makes a deep copy of the two sets of BEs in sft_a and sft_b and puts them into the set of BEs
    in sft.
    Assumes the TE of sft_a is also the TE of the resulting SFT.
    :param sft: The sft in which there should be the deep copy of the BEs.
    :param sft_a: sft_a of which the BEs should be deep copied
    :param sft_b: sft_b of which the BEs should be deep copied
    """
    basic_events_to_copy = set(sft_a.sft_attributes.basic_events).union(set(
        sft_b.sft_attributes.basic_events))
    basic_events = []
    for basic_event in basic_events_to_copy:
        new_be = BasicEvent(vertex_id=basic_event.vertex_id,
                            sft=sft,
                            faultcode=basic_event.faultcode)
        basic_events.append(new_be)
    sft.add_basic_events(basic_events)


def deep_copy_sft_gates(sft: SFT,
                        sft_a: SFT,
                        sft_b: SFT):
    """
    Makes a deep copy of the two sets of Gates in sft_a and sft_b and puts them into the set of
    Gates in sft. If the ID of a new Gate equals the ID of sft_a's TE, this new gate becomes the
    new TE.
    :param sft: The sft in which there should be the deep copy of the Gates.
    :param sft_a: sft_a of which the Gates should be deep copied
    :param sft_b: sft_b of which the Gates should be deep copied
    """
    gates_to_copy = set(sft_b.sft_attributes.gates).union(set(
        sft_a.sft_attributes.gates))
    gates = []
    for gate in gates_to_copy:
        new_gate = Gate(vertex_id=gate.vertex_id,
                        sft=sft,
                        gate_type=gate.gate_type,
                        k_value=gate.k_value)
        gates.append(new_gate)
        if new_gate.vertex_id == sft_a.sft_attributes.top_event.vertex_id:
            sft.sft_attributes.top_event = new_gate
    sft.add_gates(gates)

"""
File to define classes for inferring P&IDs from SFTs.
"""
from typing import List, Tuple, Set

from src.controller.sft_controller import SFTController
from src.model.pid import Link
from src.controller.pid_controller import union
from src.model.pid import PID, Component, ComponentType, ComponentTypeEnum
from src.model.sft import SFT, Gate, BasicEvent, GateType, FaultCode


class SFTPODFInvalidChildTypeError(Exception):
    """
    An error that occurs while traversing the SFT Post-Order Depth-First.
    This happens when a child type is invalid. (not a gate or a BE)
    """


class SFTWrongTopEventStructureError(Exception):
    """
    An error shown when the SFT has a wrong structure at the top of the SFT.
    It should start with a Basic Event followed by a Gate.
    """


class MappingToPIDComponentError(Exception):
    """
    An error shown when the SFT has a wrong structure at the top of the SFT.
    It should start with a Basic Event followed by a Gate.
    """


class SFTToPIDConverter:
    """
    `SFTToPIDConverter` objects have the goal of converting SFTs to P&IDs.
    For now, it only contains methods. This may be extended to contain
    properties or attributes which are parameters for these methods.
    """

    def __init__(self,
                 sfts: List[SFT],
                 node_counter: int = 1):
        """
        Creates an SFTToPIDConverter
        :param sfts: sfts which should be converted
        :param node_counter: counter for creating nodes in fresh PIDs
        """
        self._sfts: List[SFT] = sfts
        self._pids: List[PID] = []
        self._node_counter: int = node_counter

    @property
    def pids(self) -> List[PID]:
        """
        Returns the resulting pids
        """
        return self._pids

    @property
    def sfts(self) -> List[SFT]:
        """
        Returns the original sfts pids
        """
        return self._sfts

    @property
    def node_counter(self) -> int:
        """
        Returns the node_counter for determining fresh node ids for links
        """
        return self._node_counter

    @node_counter.setter
    def node_counter(self, node_counter: int):
        """
        Changes the node_counter of this converter
        :node_counter: The new node counter
        """
        self._node_counter = node_counter

    def infer_pids_from_sfts(self) -> List[PID]:
        """
        Actually performs the conversion.
        :return: Dict of PIDs.
        """
        sft: SFT
        for sft in self._sfts:
            pid: PID = self.process_sft_to_fresh_pid(sft)
            already_existing_pids = {p for p in self._pids if pid.graph_id in
                                     p.graph_id}
            if len(already_existing_pids) == 1:
                pid_to_replace = already_existing_pids.pop()
                pid_union = union(pid_to_replace, pid)
                self._pids.remove(pid_to_replace)
                self._pids.append(pid_union)
            else:
                self._pids.append(pid)
        return self._pids

    def process_sft_to_fresh_pid(self,
                                 sft: SFT) -> PID:
        """
        Processes an SFT
        :param sft: The SFT to be processed
        :return: A PID
        """
        res = PID(False, sft.graph_id.split("-")[0])
        self.insert_pid_components_based_on_sft(res, sft)
        self.delete_sft_maintenance_groups_to_prevent_linking(sft)
        self.insert_pid_links_based_on_sft(res, sft)
        return res

    @staticmethod
    def insert_pid_components_based_on_sft(pid,
                                           sft):
        """
        Inserts fresh pid components based on an SFT
        :param pid: The PID in which we need to insert components
        :param sft: The source SFT
        """
        component: Component
        for basic_event in sft.sft_attributes.basic_events:
            basic_event_id = basic_event.vertex_id
            component = Component(basic_event_id, pid, {})
            match basic_event_id[0:2]:
                case "GT":
                    component.component_type = ComponentType(
                        ComponentTypeEnum.TURBINE)
                case "HX":
                    component.component_type = ComponentType(
                        ComponentTypeEnum.HEAT_EXCHANGER)
                case "VC":
                    component.component_type = ComponentType(
                        ComponentTypeEnum.CHECK_VALVE)
                case "VM":
                    component.component_type = ComponentType(
                        ComponentTypeEnum.EXPSA_MO_VALVE)
                case "VH":
                    component.component_type = ComponentType(
                        ComponentTypeEnum.MANUAL_VALVE)
                case "VI":
                    component.component_type = ComponentType(
                        ComponentTypeEnum.MFW_ISOLATION_VALVE)
                case "VS":
                    component.component_type = ComponentType(
                        ComponentTypeEnum.DPS_PRESSURE_RELIEF_VALVE)
                case "PM":
                    component.component_type = ComponentType(
                        ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
                case "TK":
                    component.component_type = ComponentType(
                        ComponentTypeEnum.TANK)
                case _:
                    component.component_type = ComponentType(
                        ComponentTypeEnum.NODE)
            if (component.component_type.component_type_enum
                    == ComponentTypeEnum.NODE
                    and basic_event.faultcode == FaultCode.M):
                component.component_type = ComponentType(
                    ComponentTypeEnum.MAINTENANCE_GROUP)
            pid.add_vertex(component)

    @staticmethod
    def delete_sft_maintenance_groups_to_prevent_linking(sft):
        """
        Deletes all BEs which have a faultcode M, as well as all edges linked to these BEs.
        Then reduces the SFT to check for gates without childs / neighbours.
        :param sft: SFT in which the BEs with faultcode M should be deleted.
        :return: SFT without BEs having a faultcode M
        """
        basic_events = sft.sft_attributes.basic_events.copy()
        for basic_event in basic_events:
            if basic_event.faultcode == FaultCode.M:
                be_to_remove = sft.find_basic_event(basic_event.vertex_id)
                sft.delete_basic_event(be_to_remove)
                for edge in basic_event.incidence:
                    sft.del_edge(edge)
        continue_reducing = True
        while continue_reducing:
            vertices_set_size_start = len(sft.vertices)
            SFTController.reduce_sft(sft, True)
            continue_reducing = vertices_set_size_start != len(sft.vertices)

    def insert_pid_links_based_on_sft(self,
                                      pid: PID,
                                      sft: SFT):
        """
        Inserts fresh pid links based on an SFT. Requires the PID to have
        components based on the SFT.
        :param pid: The PID in which we need to insert links
        :param sft: The source SFT
        """
        _, _, link_string_tuples = self.post_order_depth_first_traverse_element(
            sft.sft_attributes.top_event)
        link_counter = 1
        for link_string_tuple in link_string_tuples:
            self.insert_link_in_pid(pid, link_counter, link_string_tuple)
            link_counter += 1

    def post_order_depth_first_traverse_element(self, element: Gate | BasicEvent) -> \
            (str, str, Set[Tuple[str, str]]):
        """
        Traverses an element in an SFT in the podf algorithm.
        :param element: The element of the SFT it traverses
        :return: A three-tuple consisting of: 1) an input; 2) an output and;
        3) a set of string tuples representing links
        """
        input_str, output_str, cla_tuples = "", "", set()
        if isinstance(element, BasicEvent):
            input_str, output_str, cla_tuples = element.vertex_id, element.vertex_id, set()
        elif element.gate_type == GateType.OR:
            input_str, output_str, cla_tuples = self.podf_traverse_or_gate(element)
        elif element.gate_type in (GateType.AND, GateType.VOT):
            input_str, output_str, cla_tuples = self.podf_traverse_and_vot_gate(element)
        return input_str, output_str, cla_tuples

    def podf_traverse_or_gate(self, element: Gate) -> (str, str, Set[Tuple[str, str]]):
        """
        Traverses an element which is an OR gate.
        :param element: The OR gate
        :return: A three-tuple consisting of: 1) the input; 2) the output and;
        3) the set of string tuples representing links
        """
        input_str, output_str, links = "", "", set()
        children = [n for n in element.neighbours if
                    element.graph.is_adjacent(element, n)]
        for child in children:
            child_input, child_output, child_links = \
                self.post_order_depth_first_traverse_element(child)
            if input_str == "":
                input_str = child_input
            if output_str != "" and child_input != "":
                links = links.union({(output_str, child_input)})
            links = links.union(child_links)
            output_str = child_output
        return input_str, output_str, links

    def podf_traverse_and_vot_gate(self, element: Gate) -> (str, str, Set[Tuple[str, str]]):
        """
        Traverses an element which is an AND or an VOT gate.
        Creates an input and an output node for this structure.
        Keeps track of whether any links are actually added when traversing this gate's children.
        If so, keeps the input/output nodes.
        If not, backtracks the node_counter and passes through an empty input, output and no links.
        :param element: The element of the SFT it traverses
        :return: A three-tuple consisting of: 1) an input; 2) an output and;
        3) a set of string tuples representing links
        """
        input_str = element.graph.graph_id.split("-")[0] + "-NODE" + str(self.node_counter)
        self.node_counter += 1
        output_str = element.graph.graph_id.split("-")[0] + "-NODE" + str(self.node_counter)
        self.node_counter += 1
        links = set()
        children = [n for n in element.neighbours if
                    element.graph.is_adjacent(element, n)]
        if len(children) == 1:
            return self.post_order_depth_first_traverse_element(children.pop())
        for child in children:
            child_input, child_output, child_links = \
                self.post_order_depth_first_traverse_element(child)
            if len(child_links) > 0:
                links = links.union(child_links)
            if child_input != "":
                links = links.union({(input_str, child_input)})
            if child_output != "":
                links = links.union({(child_output, output_str)})
        return input_str, output_str, links

    @staticmethod
    def insert_link_in_pid(pid, link_counter, link_string_tuple):
        """
        Inserts a link into a PID.
        :param pid: The PID to which a Link will be added
        :param link_counter: The current link counter for determining the Link's ID.
        :param link_string_tuple: A tuple of strings signalling the tail and head of the Link
        """
        left_component_str: str = str(link_string_tuple[0]) \
            .rsplit('-', maxsplit=1)[-1]
        right_component_str: str = str(link_string_tuple[1]) \
            .rsplit('-', maxsplit=1)[-1]
        SFTToPIDConverter.add_possible_node_component_for_link_insertion(left_component_str, pid)
        SFTToPIDConverter.add_possible_node_component_for_link_insertion(right_component_str, pid)
        left_component: Component = pid.find_component(left_component_str)
        right_component: Component = pid.find_component(right_component_str)
        if left_component.component_type.component_type_enum is not \
                ComponentTypeEnum.BASIC_EVENT and \
                right_component.component_type.component_type_enum is not \
                ComponentTypeEnum.BASIC_EVENT:
            pid.add_edge(Link(pid.find_component(left_component_str),
                              pid.find_component(right_component_str),
                              str(link_counter)))

    @staticmethod
    def add_possible_node_component_for_link_insertion(component_str, pid):
        """
        Checks whether a node component is present in a string.
        If so, checks whether this node already exists in a PID.
        If not, adds the node to the PID.
        :param component_str: The string to check
        :param pid: The PID to which a potential node needs to be added
        """
        if 'NODE' in component_str \
                and pid.find_component(component_str) is None:
            node_component = Component(component_str, pid, {})
            node_component.component_type = ComponentType(
                ComponentTypeEnum.NODE)
            pid.add_vertex(node_component)

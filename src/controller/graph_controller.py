"""
This file defines several operations between multiple graphs defined in graph.py
"""
from src.model.pid import PID
from src.model.graph import Graph, Vertex, Edge


def union(graph_a: Graph, graph_b: Graph) -> Graph | PID:
    """
    Does the union operation on the two graphs a and b.
    Common elements between the two graphs are recognized by
    their hash.
    :param graph_a: graph
    :param graph_b: graph
    :return: graph that is the union of input graphs a and b
    """
    graph = Graph(directed=False, graph_id=graph_a.graph_id)

    vertices_to_copy = set(graph_a.vertices).union(set(graph_b.vertices))

    vertices = set()
    for vertex in vertices_to_copy:
        vertices.add(Vertex(vertex_id=vertex.vertex_id,
                            graph=graph,
                            attributes=vertex.attributes))
    edges = set(graph_a.edges).union(set(graph_b.edges))

    for vertex in vertices:
        graph.add_vertex(vertex)

    for edge in edges:
        graph.add_edge(edge)

    return graph


def intersection(graph_a: Graph, graph_b: Graph):
    """
    Does the intersection operation on the two graphs a and b. Common
    elements between the two graphs are recognized by
    their hash.
    :param graph_a: graph
    :param graph_b: graph
    :return: graph that is the intersection of input graphs a and b
    """
    graph = Graph(directed=False, graph_id=graph_a.graph_id)

    vertices_to_copy = set()

    for vertex_a in graph_a.vertices:
        for vertex_b in graph_b.vertices:
            if vertex_a.vertex_id == vertex_b.vertex_id:
                vertices_to_copy.add(vertex_a)

    vertices = set()
    for vertex in vertices_to_copy:
        vertices.add(Vertex(vertex_id=vertex.vertex_id,
                            graph=graph,
                            attributes=vertex.attributes))
    edges = set(graph_a.edges).intersection(set(graph_b.edges))

    for vertex in vertices:
        graph.add_vertex(vertex)

    for edge in edges:
        graph.add_edge(edge)

    return graph


def subtraction(graph_a: Graph, graph_b: Graph):
    """
    Subtracts graph b from graph a by finding the intersection
    of a and b and then finding the difference between
    that intersection and graph a.
    Edges still preserve their head and tail even if any of them are subtracted.
    :param graph_a: graph
    :param graph_b: graph
    :return: graph that is the subtraction of graph b from graph a
    """
    graph = Graph(directed=False, graph_id=graph_a.graph_id)

    vertices_intersection_to_copy = set()

    for vertex_a in graph_a.vertices:
        for vertex_b in graph_b.vertices:
            if vertex_a.vertex_id == vertex_b.vertex_id:
                vertices_intersection_to_copy.add(vertex_a)

    vertices_to_copy = set(graph_a.vertices).difference(
        vertices_intersection_to_copy)

    vertices = set()

    for vertex in vertices_to_copy:
        vertices.add(Vertex(vertex_id=vertex.vertex_id,
                            graph=graph,
                            attributes=vertex.attributes))
    edges = set(graph_a.edges).difference(set(graph_a.edges).intersection(
        set(graph_b.edges)))

    for vertex in vertices:
        graph.add_vertex(vertex)

    for edge in edges:
        graph.add_edge(edge)

    return graph


def union_graph_edges(graph, graph_a, graph_b):
    """
    Merges the edges of graph_a and graph_b into graph
    :param graph: The graph which needs to have the union of edges
    :param graph_a: graph_a containing edges
    :param graph_b: graph_b containing edges.
    :return:
    """
    new_edge_id = 0
    for edge in graph_a.edges:
        copy_edge_to_res_merged_graph(edge, graph, new_edge_id)
    for edge in graph_b.edges:
        copy_edge_to_res_merged_graph(edge, graph, new_edge_id)


def copy_edge_to_res_merged_graph(edge: Edge,
                                  graph: Graph,
                                  new_edge_id: int):
    """
    Copies an original edge into a resulting merged graph.
    :param edge: The edge to copy
    :param graph: The resulting merged graph
    :param new_edge_id: An edge id counter
    """
    if len(graph.edges) != 0:
        new_edge_id = int(max(graph.edges,
                              key=lambda e: int(e.edge_id)).edge_id) + 1
    tail = graph.find_vertex(edge.tail.vertex_id)
    head = graph.find_vertex(edge.head.vertex_id)
    graph.add_edge(Edge(tail, head, str(new_edge_id)))

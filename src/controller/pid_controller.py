"""
This file defines several operations between multiple graphs defined in graph.py
"""
from src.controller.graph_controller import union_graph_edges
from src.model.pid import PID, Component


def union(pid_a: PID, pid_b: PID) -> PID:
    """
    Does the union operation on the two pids a and b.
    Common elements between the two graphs are recognized by
    their hash.
    :param pid_a: pid
    :param pid_b: pid
    :return: pid that is the union of input graphs a and b
    """
    pid = PID(directed=False, pid_id=pid_a.graph_id)

    components_to_copy = set(pid_a.vertices).union(set(pid_b.vertices))

    components = set()
    for component in components_to_copy:
        new_component = Component(vertex_id=component.vertex_id,
                                  graph=pid,
                                  attributes=component.attributes)
        new_component.component_type = component.component_type
        components.add(new_component)

    for component in components:
        pid.add_vertex(component)

    union_graph_edges(pid, pid_a, pid_b)

    return pid

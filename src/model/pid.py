"""
Class to define a Piping and Instrumentation Diagram (P&ID)
"""
from enum import Enum
from typing import List

from src.model.graph import Graph, Vertex, Edge


class ComponentTypeEnum(Enum):
    """
    Types of components which exist.
    """
    MAINTENANCE_GROUP = "maintenance_group"
    BASIC_EVENT = "basic_event"
    TURBINE = "turbine"
    HEAT_EXCHANGER = "heat_exchanger"
    CHECK_VALVE = "check_valve"
    EXPSA_MO_VALVE = "EXPSA_MO_valve"
    MANUAL_VALVE = "manual_valve"
    MFW_ISOLATION_VALVE = "MFW_isolation_valve"
    DPS_PRESSURE_RELIEF_VALVE = "DPS_pressure_relief_valve"
    MOTOR_DRIVEN_PUMP = "_pump"
    TANK = "tank"
    NODE = "node"


class ComponentType:
    """
    Class defining the type of component. Contains an enum, .rsa representation
    and .kbi representation
    """

    def __init__(self,
                 component_type_enum: ComponentTypeEnum):
        self._component_type_enum = component_type_enum

    @property
    def component_type_enum(self):
        """
        Returns the type enum of the component
        """
        return self._component_type_enum

    def __str__(self):
        return self.__class__.__name__


class Component(Vertex):
    """
    Class defining a component of a PID.
    """

    def __init__(self,
                 vertex_id: str,
                 graph: "Graph",
                 attributes: dict):
        super().__init__(vertex_id, graph, attributes)
        self._component_type: ComponentType = ComponentType(ComponentTypeEnum.BASIC_EVENT)
        self._node_vot_gate_value: int = 0

    @property
    def component_type(self):
        """
        Returns the type of the component
        """
        return self._component_type

    @component_type.setter
    def component_type(self, component_type: ComponentTypeEnum):
        """
        Changes the component type of this component
        :component_type: The new component type of tihs component
        """
        self._component_type = component_type

    @property
    def node_vot_gate_value(self):
        """
        Returns the voting gate value of a node.
        """
        return self._node_vot_gate_value

    @node_vot_gate_value.setter
    def node_vot_gate_value(self, node_vot_gate_value: int):
        """
        Changes the node voting gate value of this component
        :node_voting_gate_value: The new voting gate value of this component
        """
        self._node_vot_gate_value = node_vot_gate_value


class Link(Edge):
    """
    Class defining a link of a PID.
    """
    def __init__(self, tail: Component, head: Component, link_id: str):
        super().__init__(tail, head, link_id)
        self._tail = tail
        self._head = head

    @property
    def tail(self) -> Component:
        """
        In case the graph is directed, this represents the tail of the arrow.
        Override because of dynamic typing issues. This method needs
        to return a Component instead of a Vertex in the case of PIDs
        :return: The tail of this edge
        """
        return self._tail

    @property
    def head(self) -> Component:
        """
        In case the graph is directed, this represents the head of the arrow.
        Override because of dynamic typing issues. This method needs
        to return a Component instead of a Vertex in the case of PIDs
        :return: The head of this edge
        """
        return self._head


class PID(Graph):
    """
    Class exists of components and links. Is a graph structure. Components are
    vertices, links are edges.
    """

    def __init__(self,
                 directed: bool = False,
                 pid_id: str = None):
        super().__init__(directed,
                         pid_id)
        self._v: [Component] = []
        self._e: [Link] = []

    @property
    def vertices(self) -> List["Component"]:
        """
        Override because of dynamic typing issues. This method needs
        to return a List of Components instead of a List of Vertices in the
        case of PIDs
        :return: The `list` of components of the pid
        """
        return self._v

    @property
    def edges(self) -> List["Link"]:
        """
        Override because of dynamic typing issues. This method needs
        to return a List of Links instead of a List of Edges in the
        case of PIDs
        :return: The `list` of links of the pid
        """
        return self._e

    def find_component(self, component_id: str) -> Component:
        """
        Returns the first component it finds with a certain component_id
        :param component_id: The component_id of the vertex which
        we want to find
        :return: A component with component_id
        """
        return next((x for x in self.vertices if x.vertex_id == component_id),
                    None)

    def __repr__(self):
        """
        A programmer-friendly representation of the PID.
        :return: The string to approximate the constructor arguments of the
        `PID
        """
        return f'PID(id={self._graph_id}, ' \
               f'directed={self._directed}, ' \
               f'#edges={len(self._e)}, ' \
               f'#vertices={len(self._v)}, '

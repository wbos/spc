"""
This is a module for working with directed and undirected multi-graphs.
"""

from typing import List, Set


class GraphError(Exception):
    """
    An error that occurs while manipulating a `Graph`
    """


class Vertex:
    """
    `Vertex` objects have a property `graph` pointing to the graph they are
    part of, and an attribute `label` which can be anything: it is not used
    for any methods, except for `__str__`.
    """

    def __init__(self, vertex_id: str, graph: "Graph", attributes: dict):
        """
        Creates a vertex.
        (Labels of different vertices may be chosen the same; this does
        not influence correctness of the methods, but will make the string
        representation of the graph ambiguous.)
        :param vertex_id: id of the Vertex
        :param graph: graph to which the vertex belongs
        :param attributes: Additional properties of this Vertex
        """
        self._incidence = {}
        self._id = vertex_id
        self._graph = graph
        self.attributes = attributes

    def __repr__(self):
        """
        A programmer-friendly representation of the vertex. :return: The
        string to approximate the constructor arguments of the `Vertex'
        """
        return f'Vertex(label={self.__str__()}, ' \
               f'#incident={len(self._incidence)}, ' \
               f'#graph={self._graph.graph_id})'

    def __str__(self) -> str:
        """
        A user-friendly representation of the vertex, that is, its label.
        :return: The string representation of the label.
        """
        return str(self._id)

    def __eq__(self, other):
        if isinstance(other, Vertex):
            return (self._id == other._id) and \
                (self.attributes == other.attributes)
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(
            f"{self._id}{sum(map(hash, str(self.attributes.items())))}")

    def is_adjacent(self, other: "Vertex") -> bool:
        """
        Returns True iff `self` is adjacent to `other` vertex.
        :param other: The other vertex
        """
        return other in self._incidence

    def add_incidence(self, edge: "Edge"):
        """
        Adds an edge to the incidence map
        :param edge: The edge that is used to add the incidence
        """
        other = edge.other_end(self)

        if other not in self._incidence:
            self._incidence[other] = set()

        self._incidence[other].add(edge)

    def remove_incidence(self, edge: "Edge"):
        """
        Removes an edge from the incidence map
        :param edge: The edge that is used to remove the incidence
        """
        other = edge.other_end(self)

        if other in self._incidence:
            self._incidence[other].remove(edge)
            if len(self._incidence[other]) == 0:
                self._incidence.pop(other)

    @property
    def graph(self) -> "Graph":
        """
        The graph of this vertex
        :return: The graph of this vertex
        """
        return self._graph

    @property
    def incidence(self) -> Set["Edge"]:
        """
        Returns the set of edges incident with the vertex.
        :return: The set of edges incident with the vertex
        """
        result = set()

        for edge_set in self._incidence.values():
            result |= edge_set

        return result

    @property
    def neighbours(self) -> List["Vertex"]:
        """
        Returns the list of neighbors of the vertex.
        """
        return list(self._incidence.keys())

    @property
    def degree(self) -> int:
        """
        Returns the degree of the vertex
        """
        return sum(map(len, self._incidence.values()))

    @property
    def vertex_id(self):
        """
        Returns the id of the vertex
        """
        return self._id


class Edge:
    """
    Edges have properties `tail` and `head` which point to the end vertices
    (`Vertex` objects). The order of these matters when the graph is directed.
    """

    def __init__(self, tail: Vertex, head: Vertex, edge_id: str):
        """
        Creates an edge between vertices `tail` and `head` :param edge_id:
        Identifier of this Edge :param tail: In case the graph is directed,
        this is the tail of the arrow. :param head: In case the graph is
        directed, this is the head of the arrow. :param attributes:
        Additional properties of this Edge
        """
        self._id = edge_id
        self._tail = tail
        self._head = head

    def __repr__(self):
        """
        A programmer-friendly representation of the edge. :return: The string
        to approximate the constructor arguments of the `Edge'
        """
        return f'Edge(label={self.__str__()}, ' \
               f'head={self.head.__str__()}, tail={self.tail.__str__()})'

    def __str__(self) -> str:
        """
        A user friendly representation of this edge
        :return: A user friendly representation of this edge
        """
        return str(self._id)

    def __eq__(self, other):
        if isinstance(other, Edge):
            return self._id == other._id
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(f"{self._id}")

    @property
    def tail(self) -> "Vertex":
        """
        In case the graph is directed, this represents the tail of the arrow.
        :return: The tail of this edge
        """
        return self._tail

    @property
    def head(self) -> "Vertex":
        """
        In case the graph is directed, this represents the head of the arrow.
        :return: The head of this edge
        """
        return self._head

    def other_end(self, vertex: Vertex) -> Vertex:
        """
        Given one end `vertex` of the edge, this returns
        the other end vertex.
        :param vertex: One end
        :return: The other end
        """
        result = None
        if self.tail == vertex:
            result = self.head
        elif self.head == vertex:
            result = self.tail
        if result is not None:
            return result
        raise GraphError(
            'edge.other_end(vertex): vertex must be head or tail of edge')

    def incident(self, vertex: Vertex) -> bool:
        """
        Returns True iff the edge is incident with the
        vertex.
        :param vertex: The vertex
        :return: Whether the vertex is incident with the edge.
        """
        return vertex in (self.head, self.tail)

    @property
    def edge_id(self):
        """
        Returns the id of the edge
        """
        return self._id


class Graph:
    """
    Graphs have properties `_v`, `_e`, `_directed` and `graph_id`.
    _v represents the vertices, _e the edges, _directed is a boolean
    indicating whether the graph is directed
    """

    def __init__(self, directed: bool, graph_id: str = None):
        """
        Creates a graph.
        :param directed: Whether the graph should behave as a directed graph.
        :param graph_id: The graph_id of a graph.
        """
        self._v: [Vertex] = []
        self._e: [Edge] = []
        self._directed: bool = directed
        self._graph_id: str = graph_id

    @property
    def graph_id(self) -> str:
        """
        Returns the level id of the graph.
        :return: the level id.
        """
        return self._graph_id

    @graph_id.setter
    def graph_id(self, graph_id: str):
        """
        Changes the level id of the graph.
        :graph_id: The new level id of the graph that is gonna be set.
        """
        self._graph_id = graph_id

    def __repr__(self):
        """
        A programmer-friendly representation of the Graph.
        :return: The string to approximate the constructor arguments of the
        `Graph'
        """
        return f'Graph(id={self._graph_id}, ' \
               f'directed={self._directed}, ' \
               f'#edges={len(self._e)}, ' \
               f'#vertices={len(self._v)})'

    def __str__(self) -> str:
        """
        A user-friendly representation of this graph.
        :return: A textual representation of the vertices and edges of this
        graph
        """
        return 'V=[' + ", ".join(map(str, self._v)) + ']~E=[' + ", ".join(
            map(str, self._e)) + ']'

    @property
    def directed(self) -> bool:
        """
        Whether the graph behaves as a directed graph
        :return: Whether the graph is directed
        """
        return self._directed

    @property
    def vertices(self) -> List["Vertex"]:
        """
        :return: The `list` of vertices of the graph
        """
        return list(self._v)

    @property
    def edges(self) -> List["Edge"]:
        """
        :return: The `list` of edges of the graph
        """
        return list(self._e)

    def __iter__(self):
        """
        :return: Returns an iterator for the vertices of the graph
        """
        return iter(self._v)

    def __len__(self) -> int:
        """
        :return: The number of vertices of the graph
        """
        return len(self._v)

    def add_vertex(self, vertex: "Vertex"):
        """
        Add a vertex to the graph.
        :param vertex: The vertex to be added.
        """
        if vertex.graph != self:
            raise GraphError("A vertex must belong to the graph it is added to")

        self._v.append(vertex)

    def add_vertices(self, vertices: List[Vertex]):
        """
        Add a list of vertices to the graph.
        :param vertices: The vertices to be added
        """
        for vertex in vertices:
            self.add_vertex(vertex)

    def add_edge(self, edge: "Edge"):
        """
        Add an edge to the graph. And if necessary also the vertices.
        Includes some checks in case the graph should stay simple.
        :param edge: The edge to be added
        """
        self._e.append(edge)

        edge.head.add_incidence(edge)
        edge.tail.add_incidence(edge)

    def add_edges(self, edges: List[Edge]):
        """
        Add a list of edges to the graph. And if necessary also the vertices.
        Includes some checks in case the graph should stay simple.
        :param edges: The edges to be added
        """
        for edge in edges:
            self.add_edge(edge)

    def find_edge(self, edge_id: str) -> Edge:
        """
        Returns the first edge it finds with a certain edge_id.
        :param edge_id: The edge_id of the edge which we want to find
        :return: An edge with edge_id
        """
        return next((e for e in self.edges if e.edge_id == edge_id), None)

    def find_vertex(self, vertex_id: str) -> Vertex:
        """
        Returns the first vertex it finds with a certain vertex_id
        :param vertex_id: The vertex_id of the vertex which we want to find
        :return: A vertex with vertex_id
        """
        return next((x for x in self.vertices if x.vertex_id == vertex_id),
                    None)

    def is_adjacent(self, vertex_u: "Vertex", vertex_v: "Vertex") -> bool:
        """
        Returns True iff vertices `u` and `v` are adjacent. If the graph is
        directed, the direction of the edges is respected. :param vertex_u:
        One vertex :param vertex_v: The other vertex :return: Whether the
        vertices are adjacent
        """
        return vertex_v in vertex_u.neighbours and \
            (not self.directed or any(e.head == vertex_v
                                      for e in vertex_u.incidence))

    def del_edge(self, edge: Edge):
        """Deletes an edge from the graph."""
        if edge in self.edges:
            self._e.remove(edge)
            edge.head.remove_incidence(edge)
            edge.tail.remove_incidence(edge)

    def del_vertex(self, vertex: Vertex):
        """Deletes a vertex from the graph."""
        if vertex in self.vertices:
            self._v.remove(vertex)

"""
Class to define a Static Fault Tree (SFT)
"""

from enum import Enum
from typing import List, Union

from src.model.graph import Graph, Vertex


class GateType(Enum):
    """
    Types of gates which exist.
    """
    AND = 1
    OR = 2
    VOT = 3


class Gate(Vertex):
    """
    A gate within an SFT.
    """
    def __init__(self,
                 vertex_id: str,
                 sft: "SFT",
                 gate_type: GateType = GateType.AND,
                 k_value: int = 0):
        super().__init__(vertex_id, sft, {})
        self._graph: SFT = sft
        self._gate_type: GateType = gate_type
        self._k_value: int = k_value

    @property
    def gate_type(self) -> GateType:
        """
        The type of this gate
        :return: The type of this gate
        """
        return self._gate_type

    @gate_type.setter
    def gate_type(self, gate_type: GateType):
        """
        Changes the gate type of the gate.
        :gate_type: The new gatetype of the gate that is gonna be set.
        """
        self._gate_type = gate_type

    @property
    def k_value(self) -> int:
        """
        The k_value of this gate
        :return: The k_value of this gate
        """
        return self._k_value

    @k_value.setter
    def k_value(self, k_value: int):
        """
        Changes the k_value of the gate.
        :k_value: The new k_value of the gate that is gonna be set.
        """
        self._k_value = k_value

    @property
    def neighbours(self) -> List[Union["BasicEvent", "Gate"]]:
        """
        Override because of dynamic typing issues. This method needs
        to return a List of either BEs or Gates instead of a List of
        Vertices
        :return the list of neighbours of the gate.
        """
        return list(self._incidence.keys())

    @property
    def graph(self) -> "SFT":
        """
        The SFT of this Gate.
        Override because of dynamic typing issues. This method needs
        to return a List of either BEs or Gates instead of a List of
        Vertices
        :return: The SFT of this Gate
        """
        return self._graph


class FaultCode(Enum):
    """
    Types of faultcodes which exist.
    """
    # Fails to start, fails to open
    A = "A"
    # Fail to stop, fail to close
    B = "B"
    # Starts inadvertantly, opens inadvertantly
    C = "C"
    # Stops operating, close inadvertantly
    D = "D"
    # Human Error
    H = "H"
    # Faulty line-up, opened valve
    T = "T"
    # Faulty line-up, closed valve
    U = "U"
    # Unavailable due to maintenance
    M = "M"
    # General failure
    X = "X"


class BasicEvent(Vertex):
    """
    A BE of an SFT
    """
    def __init__(self,
                 vertex_id: str,
                 sft: "SFT",
                 faultcode: FaultCode = None):
        super().__init__(vertex_id, sft, {})
        self._faultcode: FaultCode = faultcode

    @property
    def neighbours(self) -> List[Union["BasicEvent", "Gate"]]:
        """
        Override because of dynamic typing issues. This method needs
        to return a List of either BEs or Gates instead of a List of
        Vertices
        :return The list of neighbours of the basic event.
        """
        return list(self._incidence.keys())

    @property
    def faultcode(self) -> FaultCode:
        """
        The fault code of the BE.
        :return A string containing the fault code of the BE.
        """
        return self._faultcode

    @faultcode.setter
    def faultcode(self, faultcode: FaultCode):
        """
        Setter for the faultcode of the BE
        :faultcode: The faultcode of this BE.
        """
        self._faultcode = faultcode


class SFTAttributes:
    """
    A class containing attributes of an SFT.
    """
    def __init__(self,
                 top_event: Gate = None,
                 gates: List[Gate] = None,
                 basic_events: List[BasicEvent] = None) -> None:
        self._top_event: Gate = top_event
        self._gates: List[Gate] = gates
        if self._gates is None:
            self._gates: List[Gate] = []
        self._basic_events: List[BasicEvent] = basic_events
        if self._basic_events is None:
            self._basic_events: List[BasicEvent] = []

    @property
    def basic_events(self) -> List[BasicEvent]:
        """
        Getter for BEs of an SFT
        :return: A list of BEs of an SFT
        """
        return self._basic_events

    @basic_events.setter
    def basic_events(self, basic_events: List[BasicEvent]):
        """
        Setter for the BEs of an SFT
        :basic_events: A list of BEs of an SFT
        """
        self._basic_events = basic_events

    @property
    def gates(self) -> List[Gate]:
        """
        Getter for Gates of an SFT
        :return: A list of Gates of an SFT
        """
        return self._gates

    @gates.setter
    def gates(self, gates: List[Gate]):
        """
        Setter for the gates of an SFT
        :gates: The gates of an SFT
        """
        self._gates = gates

    @property
    def top_event(self) -> Gate:
        """
        Getter for the TE of an SFT
        :return: The TE of an SFT
        """
        return self._top_event

    @top_event.setter
    def top_event(self, top_event: Gate):
        """
        Setter for the TE of an SFT
        :top_event: The TE of an SFT
        """
        self._top_event = top_event


class SFT(Graph):
    """
    A class for a Static Fault Tree. Uses the Graph structure so that we
    get a Directed Acyclic Graph.
    """
    def __init__(self,
                 sft_id: str = "",
                 directed: bool = True,
                 sft_attributes: SFTAttributes = None) -> None:
        super().__init__(directed,
                         sft_id)
        self._sft_attributes = sft_attributes
        if sft_attributes is None:
            self._sft_attributes: SFTAttributes = SFTAttributes()

    @property
    def sft_attributes(self) -> SFTAttributes:
        """
        Getter for SFTAttributes of an SFT.
        SFTAttributes are a list of Gates, a list of BEs and the TE.
        :return: An instance of the SFTAttributes class.
        """
        return self._sft_attributes

    @sft_attributes.setter
    def sft_attributes(self, sft_attributes: SFTAttributes):
        """
        Setter for the SFTAttributes of an SFT
        SFTAttributes are a list of Gates, a list of BEs and the TE.
        :sft_attributes: An instance of the SFTAttributes class.
        """
        self._sft_attributes = sft_attributes

    def add_gate(self, gate: Gate):
        """
        Add a gate to the SFT.
        :param gate: gate to add
        """
        self.sft_attributes.gates.append(gate)
        super().add_vertex(gate)

    def add_gates(self, gates: List[Gate]):
        """
        Add a list of gates to the SFT.
        :param gates: gates to add
        """
        for gate in gates:
            self.sft_attributes.gates.append(gate)
            super().add_vertex(gate)

    def delete_gate(self, gate: Gate | str):
        """
        Delete a gate from the SFT.
        :param gate: gate to be deleted
        """
        if isinstance(gate, str):
            gate = self.find_gate(gate)
        self.sft_attributes.gates.remove(gate)
        super().del_vertex(gate)

    def find_gate(self, gate_id: str) -> Gate:
        """
        Returns the first gate it finds with a certain gate_id
        :param gate_id: The vertex_id of the gate which we want to find
        :return: A gate with vertex_id
        """
        return next((x for x in self.sft_attributes.gates if x.vertex_id
                     == gate_id), None)

    def add_basic_event(self, basic_event: BasicEvent):
        """
        Add a basic event to the SFT.
        :param basic_event: basic event to add
        """
        self.sft_attributes.basic_events.append(basic_event)
        super().add_vertex(basic_event)

    def add_basic_events(self, basic_events: List[BasicEvent]):
        """
        Add a basic event to the SFT.
        :param basic_events: basic event to add
        """
        for basic_event in basic_events:
            self.sft_attributes.basic_events.append(basic_event)
            super().add_vertex(basic_event)

    def delete_basic_event(self, basic_event: BasicEvent | str):
        """
        Delete a basic_event from the SFT.
        :param basic_event: basic_event to be deleted
        """
        if isinstance(basic_event, str):
            basic_event = self.find_basic_event(basic_event)
        self.sft_attributes.basic_events.remove(basic_event)
        super().del_vertex(basic_event)

    def find_basic_event(self, basic_event_id: str) -> BasicEvent:
        """
        Returns the first basic event it finds with a certain basic_event_id
        :param basic_event_id: The basic_event_id of the basic event which we
        want to find
        :return: A basic event with basic_event_id
        """
        return next((x for x in self.sft_attributes.basic_events if x.vertex_id
                     == basic_event_id), None)

    def update_te_gate_type(self, gate_type: GateType):
        """
        Updates the GateType of this SFT's TE.
        :param gate_type: The new GateType for this SFT's TE
        """
        self.sft_attributes.top_event.gate_type = gate_type

    def update_te_k_value(self, k_value: int):
        """
        Updates the k_value of this SFT's TE.
        :param k_value: The new k_value for this SFT's TE
        """
        self.sft_attributes.top_event.k_value = k_value

    def update_te_gate_type_and_k_value(self,
                                        gate_type: GateType,
                                        k_value: int) -> Gate:
        """
        Updates both the GateType and the k_value of this SFT's TE.
        :param gate_type: The new GateType for this SFT's TE
        :param k_value: The new k_value for this SFT's TE
        :returns This SFT's TE
        """
        self.update_te_gate_type(gate_type)
        self.update_te_k_value(k_value)
        return self.sft_attributes.top_event

    def __repr__(self):
        """
        A programmer-friendly representation of the SFT.
        :return: The string to approximate the constructor arguments of the
        `SFT'
        """
        return f'SFT(id={self._graph_id}, ' \
               f'directed={self._directed}, ' \
               f'#gates={len(self._sft_attributes.gates)}, ' \
               f'#basic_events={len(self._sft_attributes.basic_events)}, ' \
               f'#edges={len(self._e)}, ' \
               f'#vertices={len(self._v)}, ' \
               f'top_event={str(self._sft_attributes.top_event)})'

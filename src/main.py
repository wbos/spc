"""
Main file of the application.
"""
from pathlib import Path
from typing import List

from src.controller.sft_controller import SFTController
from src.model.pid import PID
from src.model.sft import SFT
from src.model_io.pid_io import file_export
from src.controller.sft_to_pid import SFTToPIDConverter
from src.model_io.sft_io import parse_sfts_from_rsa_path


def run():
    """
    Function for running the application.
    """
    for path in Path('../resources/SFTs').glob('*.RSA'):
        raw_sfts: List[SFT] = parse_sfts_from_rsa_path(path)
        processed_sfts: List[SFT] = SFTController(raw_sfts).pre_process_sfts()
        sft_to_pid_converter = SFTToPIDConverter(processed_sfts)
        pids: List[PID] = sft_to_pid_converter.infer_pids_from_sfts()
        file_export(pids=pids,
                    filedir="../resources/PIDs_result_generated/",
                    filename=path.with_suffix(".kbi").name)


run()

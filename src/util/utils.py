"""
File to define utility methods
"""


def append_to_list_if_not_present(my_list, element):
    """
    Appends an element to a list if it is not in there already. Works for
    basic types, like str, int or (str,str).
    :param my_list: List to append to.
    :param element: Element to check.
    """
    if element not in my_list:
        my_list.append(element)


def string_starts_with_t1_t2_t3_or(string: str) -> bool:
    """
    Returns true if the input string starts with "T1", "T2" or "T3".
    :param string: input string
    :return: True if the input string starts with "T1", "T2" or "T3"
    """
    return string.startswith("T1") or string.startswith("T2") or \
        string.startswith("T3")


def string_strip_startswith_mb_tag(string: str) -> str:
    """
    Strips a string from a possible `MB-` start. This happens in FTR, GAT and
    GIN inputs of the RSA file (the latter one if it's a gate as input).
    :param string: The string of which `MB-` should be removed.
    :return: Stripped string.
    """
    if string.startswith("MB-"):
        string = string[3::]
    return string

"""
Main file of handling pid dataset in recourses. Mainly to understand and
discover the data structure.
"""
# Quick and dirty look at the PID dataset. Shows the numpy arrays.
from pathlib import Path
import numpy as np

for path in Path('../../resources/PIDs_original/DigitizePID_Dataset/0').glob(
        "*.npy"):
    array = np.load(path, allow_pickle=True)
    print(array.shape)
    print(array.size)
    print(array)

# symbols import, convert to EXPSA class, put on grid
# lines import, convert to EXPSA type, put on grid

# lines2 what is last column? dashed or solid?

# linker import, convert to EXPSA links of symbol - line - symbol

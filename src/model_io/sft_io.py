"""
Includes functions for reading and writing SFTs.
"""
from pathlib import Path
from typing import List

from src.model.sft import SFT, SFTAttributes, Gate, GateType, BasicEvent, \
    FaultCode
from src.model.graph import Vertex, Edge
from src.util.utils import string_starts_with_t1_t2_t3_or, \
    string_strip_startswith_mb_tag


class SFTReadError(Exception):
    """
    An error that occurs while manipulating an `SFT`
    """


class SFTWriteError(Exception):
    """
    An error that occurs while manipulating an `SFT`
    """


def parse_sft_label_from_ftr_rsa_line(line: str) -> str:
    """
    Parses the top event label from a line of an already imported .rsa file.
    :param line: The line to be checked. Requires that the line starts with
    `FTR\t\"`
    :return: String containing the top event label.
    """
    sft_label = line.split("\"")[1].strip()
    sft_label = string_strip_startswith_mb_tag(sft_label)
    return sft_label


def parse_fault_code_from_string(string: str) -> FaultCode:
    """
    Parses a FaultCode from a string. This string should be the third part
    of a parsed BEV string of an RSA file.
    :param string: FaultCode String to be converted to FaultCode.
    :return: FaultCode having the correct enum.
    """
    faultcode = FaultCode.X
    match string:
        case "A":
            faultcode = FaultCode.A
        case "B":
            faultcode = FaultCode.B
        case "C":
            faultcode = FaultCode.C
        case "D":
            faultcode = FaultCode.D
        case "H":
            faultcode = FaultCode.H
        case "T":
            faultcode = FaultCode.T
        case "U":
            faultcode = FaultCode.U
        case "M":
            faultcode = FaultCode.M
        case "MA":
            faultcode = FaultCode.M
        case "X":
            faultcode = FaultCode.X
    return faultcode


def parse_sft_be_label_tuple_from_bev_rsa_line(line: str) -> \
        (str, str, FaultCode):
    """
    Parses the basic event label from a line of an already imported .rsa file.
    :param line: The line to be checked. Requires that the line starts with
    `BEV\t\"`
    LOOP BEs are ignored because they are not in the EXPSA definition
    (not thermo-hydraulic).
    LOOP stands for Loss of Offsite Power.
    :return: Tuple containing the sft TE label as string and the basic event
    label as string.
    """
    sft_be_fc_label_list = line.split("\"")[1] \
        .strip() \
        .replace("_", "-") \
        .split("-")
    fault_code = parse_fault_code_from_bev_rsa_line(sft_be_fc_label_list)
    if string_starts_with_t1_t2_t3_or(sft_be_fc_label_list[1]):
        return (sft_be_fc_label_list[0],
                sft_be_fc_label_list[2] + sft_be_fc_label_list[1],
                fault_code)
    return sft_be_fc_label_list[0], sft_be_fc_label_list[1], fault_code


def parse_fault_code_from_bev_rsa_line(sft_be_label_list: List[str]) -> FaultCode:
    """
    Parses the fault code from a BEV RSA line.
    :param sft_be_label_list:
    :return: The FaultCode of this BEV RSA line.
    """
    if len(sft_be_label_list) > 2:
        if (string_starts_with_t1_t2_t3_or(sft_be_label_list[1])
                and len(sft_be_label_list) > 3):
            return parse_fault_code_from_string(sft_be_label_list[3])
        return parse_fault_code_from_string(sft_be_label_list[2])
    return FaultCode.X


def parse_sft_from_rsa_line(line: str,
                            sfts: List[SFT]):
    """
    Parses an sft from a line of an already imported .rsa file. Adds an SFT to
    the list of SFTs given through the second parameter if the fresh, potential
    SFT does not exist yet.
    :param line: The line to be checked. Requires that the line starts with
    `FTR\t\"`
    :param sfts: A list of SFTs to which a potential fresh SFT should be added.
    :return: A parsed SFT.
    """
    sft_label = parse_sft_label_from_ftr_rsa_line(line)
    res = SFT(sft_label, True, SFTAttributes())
    if len({s for s in sfts if
            res.graph_id in s.graph_id}) == 0:
        sfts.append(res)


def parse_be_from_bev_rsa_line(line: str,
                               sfts: List[SFT]):
    """
    Parses a basic event from a line of an already imported .rsa file.
    The line should begin with `BEV\t\", then the SFT TE label followed by a
    `-` character and then the BE label.
    :param line: The line to be checked. Requires that the line starts with
    `BEV\t\"`
    :param sfts: A list of SFTs to which the BE should be added.
    """
    if len(line.split("\"")[1].strip().replace("_", "-").split("-")) < 2:
        return
    sft_label, be_label, fault_code = \
        parse_sft_be_label_tuple_from_bev_rsa_line(line)
    sfts_containing_sft_label = {s for s in sfts if sft_label in s.graph_id}
    for sft in sfts_containing_sft_label:
        basic_event_to_add = BasicEvent(be_label, sft, fault_code)
        if basic_event_to_add not in sft.vertices:
            sft.add_basic_event(basic_event_to_add)


def parse_sfts_from_rsa_path(path: Path) -> List[SFT]:
    """
    Reads a .rsa file and converts it into an SFT
    :param path: path pointing to an .rsa file to read
    :return: SFT based on the .rsa file
    """
    res: List[SFT] = []
    lines = path.read_text(encoding="utf-16le").split("\n")
    parse_sfts_from_lines(lines, res)
    return res


def parse_gate_from_gat_rsa_line(line: str,
                                 sfts: List[SFT]) -> Gate:
    """
    Parses a gate from a line of an already imported .rsa file.
    The line should begin with `GAT\t\", then the SFT TE label followed by a
    `-` character and then the Gate label.
    :param line: The line to be checked. Requires that:
    1) The line starts with `GAT\t\".
    2) The next element contains the FTR system including
    `@` as prefix char. Then it includes the gate label.
    4) It then is followed by the gate type (AND, OR or an int for a VOT gate)
    5) The next integer can be ignored
    6) The following element can be ignored
    7) Then, an integer shows whether the gate is linked to the TE.
    :param sfts: A list of SFTs to which the Gate should be added.
    :return The added Gate.
    """
    line = line.replace("_", "-")
    gate_label, gate_type_str, link_to_te, sft_label = parse_gat_rsa_line_values(line)
    existing_sfts: List[SFT] = \
        [s for s in sfts if sft_label == s.graph_id]
    parse_gate_from_gat_rsa_line_check_whether_sft_exists(
        existing_sfts,
        gate_label,
        gate_type_str,
        sft_label
    )
    existing_sft: SFT = existing_sfts.pop()
    gate_type: GateType
    k_value: int
    gate_type, k_value = \
        parse_gate_type_and_k_value_from_gat_rsa_line(gate_type_str)
    gate: Gate = add_gate_to_sft(existing_sft,
                                 gate_label,
                                 gate_type,
                                 k_value,
                                 link_to_te)
    return gate


def parse_gat_rsa_line_values(line):
    """
    Parses the following things from an GAT RSA line as strings:
    1) A potential gate label
    2) A potential gate type
    3) A potential 'link_to_te' value used when adding a potential gate to an SFT
    4) A potential SFT label signalling to which SFT this gate belongs
    :param line: The GAT RSA line
    :return: A four-tuple with aforementioned values
    """
    sft_label: str = string_strip_startswith_mb_tag(
        line.split("\"")[3].strip())
    gate_label: str = parse_gate_label_from_gat_rsa_line(line)
    gate_type_str: str = line.split("\"")[2].strip().split('\t')[0]
    link_to_te: str = line.split("\"")[4].strip()
    return gate_label, gate_type_str, link_to_te, sft_label


def parse_gate_from_gat_rsa_line_check_whether_sft_exists(
        existing_sfts: List[SFT],
        gate_label: str,
        gate_type_str: str,
        sft_label: str):
    """
    Checks whether an SFT exists when attempting to parse a gate from an GAT RSA line.
    Should throw an SFTReadError if there is no SFT found.
    The SFTReadError uses the remaining three paramters to show information.
    :param existing_sfts: List of SFTs which should have exactly one SFT.
    :param gate_label: The potential gate label
    :param gate_type_str: The potential gate type
    :param sft_label:  The potential SFT
    """
    if len(existing_sfts) == 0:
        raise SFTReadError("There should exist an SFT "
                           + sft_label
                           + " to which I can add Gate "
                           + gate_label + " of type "
                           + gate_type_str)


def add_gate_to_sft(existing_sft: SFT,
                    gate_label: str,
                    gate_type: GateType,
                    k_value: int,
                    link_to_te: str) -> Gate:
    """
    Adds a gate to the SFT. Also links the gate to the top event gate if
    link_to_te is set to 1. Returns the created gate (or the top event if
    gate_label is empty since this signals only a change of the TE Gate's
    type and/or k_value
    :param existing_sft: SFT to add a gate to
    :param gate_label: The gate label
    :param gate_type: The gate type
    :param k_value: The gate k_value
    :param link_to_te: Whether a fresh gate should become the TE.
    :return: The fresh gate (or TE) for use as previous_gate for the GIN
    lines after the GAT lines.
    """
    gate: Gate = Gate(gate_label, existing_sft, gate_type, k_value)
    potential_existing_gate: Gate = existing_sft.find_gate(gate_label)
    if len(gate_label) == 0:
        gate = existing_sft.update_te_gate_type_and_k_value(gate_type, k_value)
    elif potential_existing_gate is None:
        existing_sft.add_gate(gate)
        if link_to_te.isnumeric() and int(link_to_te) == 1:
            existing_sft.sft_attributes.top_event = gate
    else:
        potential_existing_gate.gate_type = gate_type
        potential_existing_gate.k_value = k_value
    return gate


def parse_gate_type_and_k_value_from_gat_rsa_line(gate_type_str: str) \
        -> (GateType, int):
    """
    Parses both the gate type and the k_value of a gate from a part of the GAT
    rsa line. Only changes the k_value to something different than 0 if the
    gate type
    is a VOT gate.
    :param gate_type_str: Prepared part of the GAT rsa line which can be
    parsed to a GateType and a k_value
    :return: gate_type and k_value
    """
    gate_type: GateType = GateType.AND
    k_value: int = 0
    if gate_type_str.isnumeric():
        gate_type = GateType.VOT
    else:
        match gate_type_str:
            case "AND":
                gate_type = GateType.AND
            case "OR":
                gate_type = GateType.OR
    if gate_type == GateType.VOT:
        k_value = int(gate_type_str)
    return gate_type, k_value


def parse_gate_label_from_gat_rsa_line(line: str):
    """
    Parses the gate label from a GAT rsa line. Needs an sft_label since that
    should be removed from the line tro retrieve the actual gate label.
    :param line: The line to be parsed.
    :return: A gate label parsed from a GAT rsa line
    """
    gate_label = string_strip_startswith_mb_tag(
        line.split("\"")[1].strip())
    if gate_label.startswith("-"):
        gate_label = gate_label[1:]
    return gate_label


def parse_input_label_from_gin_rsa_line(line: str):
    """
    Parses the gate input label from a line of an already imported .rsa file.
    The line should begin with `GIN\t\", then a gate or BE followed by a
    `"` character and then the BE or Gate label. The label starts with an `@`
    char if it is a gate. At the end, an integer signals
    whether the input is inverted (-1 means inverted, 0 means normal)
    Returns a string to which the previous "GAT" line should be connected.
    :param line: The line to be checked. Requires that the line starts with
    `BEV\t\"`
    :return string containing the node label to which a previous GAT line
    should be connected.
    """
    input_label = string_strip_startswith_mb_tag(
        line.replace("_", "-").split("\"")[1].strip())
    return input_label


def parse_gate_input_from_gin_rsa_line(previous_gate: Gate,
                                       line: str,
                                       detected_acp: bool):
    """
    Parses a gin rsa line, creates the gate input node if it doesn't exist
    yet in the corresponding sft and links it to the `previous_gate`
    param. The previous gate is the last gate parsed by a GAT line in
    the RSA file. The gin rsa line should contain an already existing SFT.
    :param previous_gate: gate which has been parsed before from a GAT line
    in the RSA file. Cannot be None.
    :param line: GIN line of an RSA file. Should contain the SFT of the
    previous_gate, otherwise the line is ignored. If the SFT does not exist
    yet, this line also is ignored.
    :param detected_acp: boolean to signal whether we found an ACP gate. Used
    when parsing GIN lines, since linking to ACP gates can be ignored.
    """
    if detected_acp:
        return
    input_label = parse_input_label_from_gin_rsa_line(line)
    sft: SFT = previous_gate.graph
    # Ignore the line if input_label does not contain the first part of
    # sft.graph_id
    if sft.graph_id.split("-")[0] != input_label.split("-")[0].replace("@", ""):
        return
    if "BEV" in line:
        # For BE input labels, we just want the BE label.
        if string_starts_with_t1_t2_t3_or(input_label.split("-")[1]):
            input_label = input_label.split("-")[2] + input_label.split("-")[1]
        else:
            input_label = input_label.split("-")[1]
        attempt_linking_gate_input_to_gate(input_label, previous_gate, sft, True)
    else:
        attempt_linking_gate_input_to_gate(input_label, previous_gate, sft, False)


def attempt_linking_gate_input_to_gate(input_label: str,
                                       previous_gate: Gate,
                                       sft: SFT,
                                       gate_input_is_be_type: bool):
    """
    Attempts to link a gate input to a gate. It fails when there exists more
    than one potential inputs. When there is no potential input, it either
    creates one (in the case that our input_label signals that we need a
    gate) or it fails (in the case that our input_label signals we need a
    basic_event which should have been define earlier in the RSA file).
    :param input_label: The label of the node we want to link as gate input
    :param previous_gate: The gate which we want to link a node to
    :param sft: The sft in which we try to make this link
    :param gate_input_is_be_type: whether the gate input should be a gate or a be
    """
    potential_inputs: List[Vertex] = \
        [s for s in sft.vertices if
         input_label == s.vertex_id]
    previous_gate = {s for s in sft.vertices if
                     previous_gate.vertex_id == s.vertex_id}.pop()
    if len(potential_inputs) == 1:
        link_gate_input_to_gate(potential_inputs,
                                previous_gate,
                                sft)
    elif not gate_input_is_be_type:
        create_new_gate_and_link_to_existing_gate(input_label,
                                                  previous_gate,
                                                  sft)
    else:
        create_new_be_and_link_to_existing_gate(input_label,
                                                previous_gate,
                                                sft)


def link_gate_input_to_gate(potential_inputs: List[Vertex],
                            previous_gate: Gate,
                            sft: SFT):
    """
    Links an already found unique gate input to a gate.
    :param potential_inputs: All potential inputs. Should be of length 1.
    :param previous_gate: The previous gate to which we link the input.
    :param sft: The SFT in which we make the link.
    """
    gate_input: Vertex = potential_inputs[0]
    if not previous_gate.is_adjacent(gate_input):
        new_edge_id = 0
        if len(sft.edges) != 0:
            new_edge_id = int(max(sft.edges,
                              key=lambda e: int(e.edge_id)).edge_id) + 1
        edge = Edge(previous_gate,
                    gate_input,
                    str(new_edge_id))
        sft.add_edge(edge)


def create_new_gate_and_link_to_existing_gate(input_label: str,
                                              previous_gate: Gate,
                                              sft: SFT):
    """
    Creates a new gate and links it to an existing gate. If the input label
    does not signal that we need to create a gate, then it either is a BE (
    which should have been declared) or a fresh edge case which is uncovered.
    :param input_label: The input label for the new gate.
    :param previous_gate: The previous gate to link the fresh gate to.
    :param sft: The sft in which we link the gates.
    """
    gate = Gate(input_label, sft)
    sft.add_gate(gate)
    new_edge_id = 0
    if len(sft.edges) != 0:
        new_edge_id = int(max(sft.edges,
                              key=lambda e: int(e.edge_id)).edge_id) + 1
    sft.add_edge(Edge(previous_gate, gate, str(new_edge_id)))


def create_new_be_and_link_to_existing_gate(input_label: str,
                                            previous_gate: Gate,
                                            sft: SFT):
    """
    Creates a new basic event and links it to an existing gate. If the input label
    does not signal that we need to create a gate, then it either is a BE (
    which should have been declared) or a fresh edge case which is uncovered.
    :param input_label: The input label for the new gate.
    :param previous_gate: The previous gate to link the fresh gate to.
    :param sft: The sft in which we link the gates.
    """
    basic_event = BasicEvent(input_label, sft)
    sft.add_basic_event(basic_event)
    new_edge_id = 0
    if len(sft.edges) != 0:
        new_edge_id = int(max(sft.edges,
                              key=lambda e: int(e.edge_id)).edge_id) + 1
    sft.add_edge(Edge(previous_gate, basic_event, str(new_edge_id)))


def parse_sfts_from_lines(lines: List[str],
                          sfts: List[SFT]):
    """
    Reads sfts from a list of lines read from an RSA file.
    Uses a `previous_gate` local variable to keep track to which gate
    a potential gate input should be linked.
    :param lines: the list of lines
    :param sfts: result list of SFTs
    """
    previous_gate: Gate | None = None
    detected_acp: bool = False
    for line in lines:
        line = line.strip()
        previous_gate, detected_acp = parse_rsa_line(line,
                                                     previous_gate,
                                                     sfts,
                                                     detected_acp)


def parse_rsa_line(line: str,
                   previous_gate: Gate,
                   sfts: List[SFT],
                   detected_acp: bool):
    """
    Parses one single rsa line.
    :param line: The line to be parsed
    :param previous_gate: parameter used when we need to link a gate input
    to a gate.
    :param sfts: resulting list of sfts
    :param detected_acp: boolean to signal whether we found an ACP gate. Used
    when parsing GIN lines, since linking to ACP gates can be ignored.
    :return: a potentially updated previous_gate
    """
    match line:
        case line if "ACP" in line and not line.startswith("GAT"):
            return previous_gate, detected_acp
        case line if line.startswith("FTR\t\""):
            parse_sft_from_rsa_line(line, sfts)
        case line if line.startswith("BEV\t\""):
            parse_be_from_bev_rsa_line(line, sfts)
        case line if line.startswith("GAT") and "ACP" in line:
            detected_acp = True
            return previous_gate, detected_acp
        case line if line.startswith("GAT"):
            previous_gate = parse_gate_from_gat_rsa_line(line, sfts)
            detected_acp = False
        case line if line.startswith("GIN\tBEV") \
                     or line.startswith("GIN\tGAT"):
            parse_gate_input_from_gin_rsa_line(previous_gate,
                                               line,
                                               detected_acp)
    return previous_gate, detected_acp

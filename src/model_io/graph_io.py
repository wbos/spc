"""
Includes functions for reading and writing graphs.
"""
import json
import ntpath
import os
import sys
from pathlib import Path
from typing import IO, List, Union

from src.model.graph import Graph, Edge, Vertex

LABEL = 'label="'
EXTRA_LABEL = 'extra_label="'
WHITESPACE_STRING = '    "'


class GraphReadError(Exception):
    """
    An error that occurs while manipulating a `Graph`
    """


class GraphWriteError(Exception):
    """
    An error that occurs while manipulating a `Graph`
    """


def read_gv_file(file: IO[str]) -> Graph:
    """
    Reads a .gv file and converts it into a Graph
    :param file: .gv file to read
    :return: Site based on the .gv file
    """
    lines = file.readlines()
    graph = Graph(graph_id=Path(file.name).parts[-1], directed=True)
    edge_id_counter = 0
    for line in lines:
        if 'label' in line and not ('--' in line or '->' in line):
            read_vertex_from_line(graph, line)
        elif '--' in line or '->' in line:
            ids = line.strip().split('--')
            head = graph.find_vertex(ids[0]
                                     .replace('\'', '')
                                     .replace('\"', '')
                                     .replace(' ', ''))
            tail = graph.find_vertex(ids[1].split('\"')[1])
            graph.add_edge(Edge(edge_id=str(edge_id_counter), head=head,
                                tail=tail))
            edge_id_counter += 1
    return graph


def read_vertex_from_line(graph, line):
    """
    Reads a vertex from a line of a .gv file and adds it to the graph.
    :param graph: Graph to which the vertex should be added.
    :param line: Line to be read.
    """
    blocks = line.split(",", maxsplit=2)
    if len(blocks) > 1:
        vertex_id = blocks[1].replace(LABEL, '').replace('"', '')
        attributes_json = blocks[2] \
            .replace(EXTRA_LABEL, '') \
            .replace('"', '') \
            .replace("`", "\"")
        if attributes_json[-2] == ']':
            attributes_json = attributes_json[:-2] + attributes_json[-1:]
        attributes = json.loads(attributes_json)
        if not vertex_id.isalnum():
            raise GraphReadError("Vertex_id is not an alphanumeric! VID: "
                                 + vertex_id)
        vertex = Vertex(vertex_id=vertex_id,
                        attributes=attributes,
                        graph=graph)
        graph.add_vertex(vertex)


def write_line(file: IO[str], line: str):
    """
    Write a line to a file
    :param file: The file
    :param line: The line
    """
    file.write(line + '\n')


def write_graph_summary_list(graph_list: List[Graph],
                             file: IO[str],
                             options=None):
    """
    Write the summaries of a graph list to an output.
    The graph list can also contain just one graph.
    :param graph_list: The list of graphs
    :param file: the file
    :param options: the (optional) options to write to the file.
    """
    # we may only write options that cannot be seen as an integer:
    if options is not None:
        for option in options:
            try:
                int(option)
            except ValueError:
                write_line(file, str(option))

    for graph_index, graph in enumerate(graph_list):
        number_of_vertices = len(graph)
        write_line(file, '# Number of vertices:')
        write_line(file, str(number_of_vertices))

        # Give the vertices (temporary) labels from 0 to n-1:
        label = {}
        for vertex_index, vertex in enumerate(graph):
            label[vertex] = vertex_index

        write_line(file, '# Edge list:')

        for edge in graph.edges:
            write_line(file,
                       str(label[edge.tail]) + ',' + str(label[edge.head]))

        if graph_index + 1 < len(graph_list):
            write_line(file, '--- Next graph:')


def write_graph_or_graphlist(graph_list: Union[Graph, List[Graph]],
                             file: IO[str],
                             options=None):
    """
    Write a summary of a graph, or a list of graphs to a file.
    :param graph_list: The graph, or a list of graphs.
    :param file: The file
    :param options: the (optional) options to write to the file.
    """
    if isinstance(graph_list, list):
        write_graph_summary_list(graph_list, file, options)
    else:
        write_graph_summary_list([graph_list], file, options)


def print_graph_or_graphlist(graph_list: Union[Graph, List[Graph]],
                             options=None):
    """
    Print a summary of a graph, or a list of graphs to sys.stdout
    :param graph_list: The graph, or list of graphs.
    :param options: The (optional) options to print.
    """
    if isinstance(graph_list, list):
        write_graph_summary_list(graph_list, sys.stdout, options)
    else:
        write_graph_summary_list([graph_list], sys.stdout, options)


def write_vertices(graph: Graph, file: IO[str] = sys.stdout):
    """
    Writes the vertices of a given graph to a file in .gv format
    :param graph: Graph to write to a file
    :param file: File to write the graph to
    :return: Dictionary of Vertices to use in writing the edges of the graph
    to the same file
    """
    name = {}
    for vertex in graph:
        name[vertex] = vertex.vertex_id
        options = 'penwidth=3,'

        options += LABEL + str(name[vertex]) + '",'
        options += EXTRA_LABEL
        options += str(get_label_from_attributes(vertex.attributes)) + '",'
        string_to_write = WHITESPACE_STRING + str(name[vertex])
        string_to_write += '" [' + options[:-1] + ']\n'
        file.write(string_to_write)
    file.write('\n')
    return name


def write_single_graph_to_gv(graph: Graph, file: IO[str]):
    """
    Writes a given graph to a file in .gv format.
    :param graph: The graph. If its vertices contain
    attributes `label`, `colourtext` or `colournum`, these are also
    included in the file. If its edges contain an
    attribute `weight`, these are also included in the file.
    :param file: The file.
    """
    if graph.directed:
        file.write(f'digraph {graph.graph_id}' + ' {\n')
    else:
        file.write(f'graph {graph.graph_id}' + '{\n')

    vertices = write_vertices(graph, file)

    for edge in graph.edges:
        options = ''
        options += LABEL + str(edge.edge_id) + '",'
        options = ' [' + options[:-1] + ']'

        if graph.directed:
            string_to_write = WHITESPACE_STRING
            string_to_write += str(graph.find_vertex(edge.tail.vertex_id))
            string_to_write += '" -> "' + str(vertices[edge.head])
            string_to_write += '"' + options + '\n'
            file.write(string_to_write)
        else:
            string_to_write = WHITESPACE_STRING
            string_to_write += str(graph.find_vertex(edge.tail.vertex_id))
            string_to_write += '" -- "'
            string_to_write += str(graph.find_vertex(edge.head.vertex_id))
            string_to_write += '"' + options + '\n'
            file.write(string_to_write)
    file.write('}')


def get_label_from_attributes(attributes: dict = None):
    """
    Obtain the label gathered from all attributes combined.
    :param attributes: attributes of a vertex or an edge
    :return: string label formatted as [key]~[value]*
    """
    label = json.dumps("{}")
    if attributes is not None:
        label = json.dumps(attributes)

    return label.replace('\"', '`')


def write_graphs_to_gv_files(graphs: list[Graph], path: ntpath):
    """
    Writes all graphs to gv files.
    :param graphs: A list of floors (which are graphs)
    :param path: os.path.join path directory where the file
    should be. Will make a new directory if it doesn't exist.
    """
    for graph in graphs:
        filename = os.path.join(path, 'graph' + str(graph.graph_id) + '.gv')
        if not os.path.exists(path):
            os.makedirs(path)
        with open(filename, 'w', encoding="utf-8") as output:
            write_single_graph_to_gv(graph=graph, file=output)

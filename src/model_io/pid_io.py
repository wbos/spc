"""
Includes functions for reading and writing PIDs.
"""
import os
from typing import List

from src.model.pid import PID, Component, ComponentTypeEnum, Link


class PIDReadError(Exception):
    """
    An error that occurs while reading a `PID`
    """


class PIDWriteError(Exception):
    """
    An error that occurs while writing a `PID`
    """


def file_export(pids: List[PID], filedir: str, filename: str):
    """
    Exports the pids and components to a .kbi file which can be imported
    in an already existing RiskSpectrum ModelBuilder study.
    Export contains only the systems as pages and the components as nodes
    within those systems.
    :param filedir: directory of the file in which it should be exported
    :param filename: name of the file to be exported
    :param pids: All pids which need to be saved
    :return: Nothing.
    """
    with open(os.path.join(filedir, filename),
              "w",
              encoding="utf-8") as kbi_file:
        text2save = "<?xml version=\"1.0\" " \
                    "encoding=\"UTF-8\" standalone=\"no\" ?>\n" \
                    "<KB3Import Nom=\"Appendix 2A\">\n\n"
        pids_kbi_string, pid_components_kbi_string, pid_links_kbi_string = \
            get_pids_kbi_strings(pids)
        text2save += pids_kbi_string
        text2save += "\t<Objet Nom=\"Frequency\"/>\n\n" \
                     "\t<Objet Nom=\"MTTR\"/>\n\n" \
                     "\t<Objet Nom=\"Mission_Time\"/>\n\n" \
                     "\t<Objet Nom=\"Probability\"/>\n\n" \
                     "\t<Objet Nom=\"Test_Interval\"/>\n\n" \
                     "\t<Objet Nom=\"Time_to_First_Test\"/>\n\n"
        text2save += pid_components_kbi_string
        text2save += pid_links_kbi_string
        text2save += "</KB3Import>\n"
        kbi_file.write(text2save)
        kbi_file.close()


def get_link_kbi_string(link: Link,
                        pid: PID):
    """
    Returns a string containing a link which needs to be
    added to the export file (P&ID). The link is part of a PID.
    :param link: the link to be made an export string of.
    :param pid: the pid the link is part of.
    :return: String containing a link which needs to be added to the
    export file (P&ID).
    """
    res = ""
    if link.tail.component_type.component_type_enum is not \
            ComponentTypeEnum.BASIC_EVENT and \
            link.head.component_type.component_type_enum is not \
            ComponentTypeEnum.BASIC_EVENT:
        res += "\t<Objet Action=\"CREER\" " \
               "Nom=\"" + pid.graph_id + "_fl_" + link.edge_id + \
               "\" Page=\"" + pid.graph_id + \
               "\" Type=\"fluid_link" + \
               "\">\n\t</Objet>\n\n" + \
               "\t<Objet Nom=\"" + pid.graph_id + "_fl_" + link.edge_id + \
               "\">\n\t\t<Cnx CnxObj=\"" + \
               pid.graph_id + "_" + link.head.vertex_id + \
               "\" CnxPt=\"in\" MonPt=\"ARRIVEE\"/>\n\t\t<Cnx CnxObj=\"" + \
               pid.graph_id + "_" + link.tail.vertex_id + \
               "\" CnxPt=\"out\" MonPt=\"DEPART\"/>\n\t</Objet>\n\n"
    return res


def pid_links(pid: PID):
    """
    Converts the links of a PID to a kbi string.
    :param pid: The PID string which should be converted
    :return: String in kbi format describing the links of given PID.
    """
    pid_links_kbi_string = ""
    for link in pid.edges:
        pid_links_kbi_string += get_link_kbi_string(link, pid)
    return pid_links_kbi_string


def get_pids_kbi_strings(pids: List[PID]) -> (str, str, str):
    """
    Gets the kbi strings from a list of pids. Returns two strings.

    The first string contains the `pages` of a kbi, which defines which PIDs
    we have and where RSMB should place them in the Main page.

    The second string contains the Components of all PIDs. For each PID, the
    kbi strings are retrieved of all components of that PID.
    :param pids: The list of PIDs for which we need the kbi string.
    :return: A kbi string of all PIDs, a kbi string of all components and a
    kbi string of all links.
    """
    pids_kbi_string = ""
    pid_components_kbi_string = ""
    pid_links_kbi_string = ""
    pid_x = 80
    pid_y = 60
    pid: PID
    for pid in pids:
        pids_kbi_string += "\t<Page Action=\"CREER\" " \
                           "Mere=\"Main_page\" " \
                           "Nom=\"" + str(pid.graph_id)
        pids_kbi_string += "\" X=\"" + str(pid_x)
        pids_kbi_string += "\" Y=\"" + str(pid_y) + "\"/>\n\n"
        pid_components_kbi_string += pid_components_to_kbi_string(pid)
        pid_links_kbi_string += pid_links(pid)
        pid_x += 120
        if pid_x > 360:
            pid_x %= 360
            pid_y += 60
    return pids_kbi_string, pid_components_kbi_string, pid_links_kbi_string


def pid_components_to_kbi_string(pid: PID):
    """
    Converts the components of a PID to a kbi string.
    :param pid: The PID string which should be converted
    :return: String in kbi format describing the components of given PID.
    """
    component_x = 80
    component_y = 60
    pid_components_kbi_string = ""
    for component in pid.vertices:
        pid_components_kbi_string += get_component_kbi_string(component,
                                                              pid,
                                                              component_x,
                                                              component_y)
        component_x += 120
        if component_x > 360:
            component_x %= 360
            component_y += 60
    return pid_components_kbi_string


def get_component_kbi_string(component: Component,
                             pid: PID,
                             component_x: int,
                             component_y: int):
    """
    Returns a string containing a component which needs to be
    added to the export file (P&ID). The component is part of a PID.

    :param component: the component to be made an export string of.
    :param pid: the pid the component is part of.
    :param component_x: x coordinate location of component
    :param component_y: y coordinate location of component
    :return: String containing a component which needs to be added to the
    export file (P&ID).
    """
    res = "\t<Objet Action=\"CREER\" " \
          "Nom=\"" + pid.graph_id + "_" + component.vertex_id + \
          "\" Page=\"" + pid.graph_id + \
          "\" Type=\""
    res += check_for_mdp(component, pid)
    res += component.component_type.component_type_enum.value
    res += "\">\n\t\t<Position X=\"" + str(component_x) + \
           "\" Y=\"" + str(component_y) + \
           "\"/>\n\t</Objet>\n\n"
    return res


def check_for_mdp(component: Component,
                  pid: PID):
    """
    Checks whether the component is a motor-driven-pump. If so, pid.graph_id
    should be returned as string
    :param component: Component to be checked.
    :param pid: pid of which the id may have to be added
    :return: optionally edited string depending on component
    """
    if component.component_type.component_type_enum == \
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP:
        return str(pid.graph_id)
    return ""

"""
Class to test the sft.py file
"""
import unittest

from src.model.sft import Gate, GateType, SFT, BasicEvent, FaultCode, SFTAttributes


class GateTest(unittest.TestCase):
    """Class to test the Gate class."""

    def setUp(self) -> None:
        """Sets up this class for our test cases."""
        self.gate1 = Gate("1",
                          SFT())

    def test_gate_type(self):
        """Tests the gate_type property."""
        self.assertEqual(GateType.AND, self.gate1.gate_type)
        self.gate1.gate_type = GateType.OR
        self.assertEqual(GateType.OR, self.gate1.gate_type)

    def test_get_k_value(self):
        """Tests the get_k_value method."""
        self.assertEqual(0, self.gate1.k_value)
        self.gate1.k_value = 3
        self.assertEqual(3, self.gate1.k_value)

    def test_neighbours(self):
        """Tests the neighbours property."""
        self.assertEqual([], self.gate1.neighbours)
        self.assertEqual(0, len(self.gate1.neighbours))


class BasicEventTest(unittest.TestCase):
    """Class to test the BasicEvent class."""

    def setUp(self) -> None:
        """Sets up this class for our test cases."""
        self.basic_event1 = BasicEvent("1", SFT())

    def test_neighbours(self):
        """Tests the neighbours property."""
        self.assertEqual([], self.basic_event1.neighbours)
        self.assertEqual(0, len(self.basic_event1.neighbours))

    def test_faultcode(self):
        """Tests the faultcode property."""
        self.assertIsNone(self.basic_event1.faultcode)
        self.basic_event1.faultcode = FaultCode.X
        self.assertEqual(FaultCode.X, self.basic_event1.faultcode)


class SFTAttributesTest(unittest.TestCase):
    """Class to test the SFTAttributes class."""

    def setUp(self) -> None:
        """Sets up this class for our test cases."""
        self.sft = SFT()
        self.sft_attributes1 = SFTAttributes()

    def test_basic_events(self) -> None:
        """Tests the basic_events property."""
        self.assertEqual(0, len(self.sft_attributes1.basic_events))
        basic_events1 = [BasicEvent("BE1", self.sft)]
        self.sft_attributes1.basic_events = basic_events1

    def test_gates(self) -> None:
        """Tests the gates property."""
        self.assertEqual(0, len(self.sft_attributes1.gates))
        gates1 = [Gate("Gate1", self.sft)]
        self.sft_attributes1.gates = gates1

    def test_top_event(self) -> None:
        """Tests the top_event property."""
        self.assertIsNone(self.sft_attributes1.top_event)
        self.sft_attributes1.top_event = Gate("Gate1", self.sft)
        self.assertEqual(Gate("Gate1", self.sft),
                         self.sft_attributes1.top_event)


class SFTTest(unittest.TestCase):
    """Class to test the SFT class."""

    def setUp(self) -> None:
        """Sets up this class for our test cases."""
        self.sft1 = SFT()
        self.gate1 = Gate("0",
                          self.sft1,
                          GateType.AND)
        self.basic_event1 = BasicEvent("BE1",
                                       self.sft1)

    def test_sft_attributes(self):
        """Tests the sft_attributes property."""
        sftattributes = SFTAttributes()
        top_event = sftattributes.top_event
        self.assertEqual(top_event, self.sft1.sft_attributes.top_event)
        self.assertEqual([], self.sft1.sft_attributes.gates)
        self.assertEqual([], self.sft1.sft_attributes.basic_events)
        self.sft1.sft_attributes = SFTAttributes(Gate("TE", self.sft1))
        top_event = Gate("TE", self.sft1)
        self.assertEqual(top_event, self.sft1.sft_attributes.top_event)
        self.assertEqual([], self.sft1.sft_attributes.gates)
        self.assertEqual([], self.sft1.sft_attributes.basic_events)

    def test_add_gate(self):
        """Tests the add_gate method."""
        self.sft1.add_gate(self.gate1)
        self.assertEqual(self.gate1, self.sft1.sft_attributes.gates[0])

    def test_add_gates(self):
        """Tests the add_gates method."""
        self.sft1.add_gates([self.gate1])
        self.assertEqual(self.gate1, self.sft1.sft_attributes.gates[0])

    def test_delete_gate(self):
        """Tests the delete_gate method."""
        self.sft1.add_gate(self.gate1)
        self.assertEqual(1, len(self.sft1.sft_attributes.gates))
        self.sft1.delete_gate(self.gate1)
        self.assertEqual(0, len(self.sft1.sft_attributes.gates))
        self.sft1.add_gate(self.gate1)
        self.assertEqual(1, len(self.sft1.sft_attributes.gates))
        self.sft1.delete_gate(self.gate1.vertex_id)
        self.assertEqual(0, len(self.sft1.sft_attributes.gates))

    def test_find_gate(self):
        """Tests the find_gate method."""
        self.sft1.add_gates([self.gate1])
        self.assertEqual(self.gate1, self.sft1.sft_attributes.gates[0])
        self.assertEqual(self.gate1,
                         self.sft1.find_gate(self.gate1.vertex_id))
        self.assertIsNone(self.sft1.find_gate("Koekjes"))

    def test_add_basic_event(self):
        """Tests the add_basic_event method."""
        self.sft1.add_basic_event(self.basic_event1)
        self.assertEqual(self.basic_event1, self.sft1.sft_attributes.basic_events[0])

    def test_add_basic_events(self):
        """Tests the add_basic_events method."""
        self.sft1.add_basic_events([self.basic_event1])
        self.assertEqual(self.basic_event1, self.sft1.sft_attributes.basic_events[0])

    def test_delete_basic_event(self):
        """Tests the delete_basic_event method."""
        self.sft1.add_basic_event(self.basic_event1)
        self.assertEqual(1, len(self.sft1.sft_attributes.basic_events))
        self.sft1.delete_basic_event(self.basic_event1)
        self.assertEqual(0, len(self.sft1.sft_attributes.basic_events))
        self.sft1.add_basic_event(self.basic_event1)
        self.assertEqual(1, len(self.sft1.sft_attributes.basic_events))
        self.sft1.delete_basic_event(self.basic_event1.vertex_id)
        self.assertEqual(0, len(self.sft1.sft_attributes.basic_events))

    def test_find_basic_event(self):
        """Tests the find_basic_event method."""
        self.sft1.add_basic_events([self.basic_event1])
        self.assertEqual(self.basic_event1, self.sft1.sft_attributes.basic_events[0])
        self.assertEqual(self.basic_event1,
                         self.sft1.find_basic_event(self.basic_event1.vertex_id))
        self.assertIsNone(self.sft1.find_basic_event("Koekjes"))

    def test_update_te_gate_type(self):
        """Tests the update_te_gate_type method."""
        self.sft1.sft_attributes.top_event = Gate("TE",
                                                  self.sft1)
        self.assertEqual(GateType.AND,
                         self.sft1.sft_attributes.top_event.gate_type)
        self.sft1.update_te_gate_type(GateType.OR)
        self.assertEqual(GateType.OR, self.sft1.sft_attributes.top_event.gate_type)

    def test_update_te_k_value(self):
        """Tests the update_te_k_value method."""
        self.sft1.sft_attributes.top_event = Gate("TE",
                                                  self.sft1)
        self.assertEqual(0,
                         self.sft1.sft_attributes.top_event.k_value)
        self.sft1.update_te_k_value(3)
        self.assertEqual(3, self.sft1.sft_attributes.top_event.k_value)

    def test_update_te_gate_type_and_k_value(self):
        """Tests the update_te_gate_type_and_k_value method."""
        self.sft1.sft_attributes.top_event = Gate("TE",
                                                  self.sft1)
        self.assertEqual(GateType.AND,
                         self.sft1.sft_attributes.top_event.gate_type)
        self.assertEqual(0,
                         self.sft1.sft_attributes.top_event.k_value)
        top_event = self.sft1.update_te_gate_type_and_k_value(GateType.OR,
                                                  3)
        self.assertEqual(GateType.OR, self.sft1.sft_attributes.top_event.gate_type)
        self.assertEqual(3, self.sft1.sft_attributes.top_event.k_value)
        self.assertEqual(self.sft1.sft_attributes.top_event, top_event)


if __name__ == '__main__':
    unittest.main()

"""
Class to test the pid.py file
"""
import unittest

from src.model.pid import PID, Component, Link, ComponentType, ComponentTypeEnum


class PIDTest(unittest.TestCase):
    """Class to test the PID class."""

    def setUp(self) -> None:
        """Sets up this class for our test cases."""
        self.pid_1 = PID(False)
        self.pid_2 = PID(False)
        self.component1 = Component("1",
                                    self.pid_1,
                                    {})
        self.component2 = Component("2",
                                    self.pid_1,
                                    {})
        self.component3 = Component("3",
                                    self.pid_2,
                                    {})
        self.component4 = Component("4",
                                    self.pid_2,
                                    {})
        self.component1.component_type = ComponentType(
            ComponentTypeEnum.TURBINE)
        self.component2.component_type = ComponentType(
            ComponentTypeEnum.TANK)
        self.component3.component_type = ComponentType(
            ComponentTypeEnum.DPS_PRESSURE_RELIEF_VALVE)
        self.component4.component_type = ComponentType(
            ComponentTypeEnum.HEAT_EXCHANGER)
        self.pipe1 = Link(self.component1, self.component2, "1")

    def test_add_component(self):
        """Tests the add_component method."""
        self.assertFalse(self.component1 in self.pid_1.vertices)
        self.pid_1.add_vertex(self.component1)
        self.assertTrue(self.component1 in self.pid_1.vertices)
        self.assertIsInstance(
            self.pid_1.find_component(self.component1.vertex_id),
            Component)
        self.component1.component_type = ComponentType(
            ComponentTypeEnum.CHECK_VALVE)
        self.assertEqual(ComponentTypeEnum.CHECK_VALVE,
                         self.pid_1.find_component(self.component1.vertex_id)
                         .component_type.component_type_enum)

    def test_add_pipe(self):
        """Tests the add_pipe method."""
        self.pid_1.add_vertex(self.component1)
        self.pid_1.add_vertex(self.component2)
        self.assertFalse(self.pipe1 in self.pid_1.edges)
        self.pid_1.add_edge(self.pipe1)
        self.assertTrue(self.pipe1 in self.pid_1.edges)
        self.component1.component_type = ComponentType(
            ComponentTypeEnum.CHECK_VALVE)
        self.assertEqual(ComponentTypeEnum.CHECK_VALVE,
                         self
                         .pid_1
                         .edges[0]
                         .tail
                         .component_type
                         .component_type_enum)

    def test_delete_component(self):
        """Tests the delete_component method."""
        self.pid_1.add_vertex(self.component1)
        self.assertTrue(self.component1 in self.pid_1.vertices)
        self.pid_1.del_vertex(self.component1)
        self.assertFalse(self.component1 in self.pid_1.vertices)

    def test_delete_pipe(self):
        """Tests the delete_pipe method."""
        self.pid_1.add_vertex(self.component1)
        self.pid_1.add_vertex(self.component2)
        self.pid_1.add_edge(self.pipe1)
        self.assertTrue(self.pipe1 in self.pid_1.edges)
        self.pid_1.del_edge(self.pipe1)
        self.assertFalse(self.pipe1 in self.pid_1.edges)

    def test_find_component(self):
        """Tests the find_component method."""
        self.pid_1.add_vertex(self.component1)
        self.pid_1.add_vertex(self.component2)
        self.pid_1.add_edge(self.pipe1)
        self.pid_2.add_vertex(self.component3)
        self.pid_2.add_vertex(self.component4)
        self.assertEqual(self.component1, self.pid_1.find_component("1"))
        self.assertIsNone(self.pid_1.find_component("dkjlfds"))


class ComponentTest(unittest.TestCase):
    """Class to test the Component class."""

    def setUp(self) -> None:
        """Sets up this class for our test cases."""
        self.pid1 = PID()
        self.component1 = Component("1", self.pid1, {})

    def test_component(self):
        """Tests a simple component instance"""
        self.assertEqual(ComponentTypeEnum.BASIC_EVENT,
                         self.component1.component_type.component_type_enum)

    def test_node_vot_gate_value(self):
        """Tests the node_vot_gate_value property"""
        self.assertEqual(0,
                         self.component1.node_vot_gate_value)
        self.component1.node_vot_gate_value = 4
        self.assertEqual(4,
                         self.component1.node_vot_gate_value)


class ComponentTypeTest(unittest.TestCase):
    """Class to test the ComponentType Enum."""

    def setUp(self) -> None:
        """Sets up this class for our test cases."""
        self.component_type1 = ComponentType(ComponentTypeEnum.BASIC_EVENT)

    def test_component_type(self):
        """Tests creating a component type."""
        self.assertTrue(ComponentTypeEnum.BASIC_EVENT,
                        self.component_type1.component_type_enum)


if __name__ == '__main__':
    unittest.main()

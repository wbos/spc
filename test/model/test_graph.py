"""
This file contains a class for testing graph.py
"""
import unittest

from src.controller.graph_controller import union
from src.model.graph import Graph, Vertex, Edge, GraphError


class VertexTest(unittest.TestCase):
    """Class for testing the Vertex class in graph.py"""

    def setUp(self) -> None:
        """Sets up the test cases for graph_computation.py"""
        self.graph_a = Graph(False, "a")
        self.vertex_a = Vertex("1", self.graph_a, {})
        self.vertex_b = Vertex("2", self.graph_a, {})
        self.vertex_c = Vertex("3", self.graph_a, {})
        self.edge_a = Edge(self.vertex_a, self.vertex_b, "1")
        self.edge_b = Edge(self.vertex_b, self.vertex_c, "2")
        self.edge_c = Edge(self.vertex_a, self.vertex_c, "3")
        self.edge_d = Edge(self.vertex_b, self.vertex_a, "4")

    def test_is_adjacent(self):
        """Tests the is_adjacent method."""
        self.assertFalse(self.vertex_a.is_adjacent(self.vertex_b))
        self.graph_a.add_edge(self.edge_a)
        self.assertTrue(self.vertex_a.is_adjacent(self.vertex_b))

    def test_add_incidence(self):
        """Tests the add_incidence method."""
        self.assertNotIn(self.edge_a, self.vertex_a.incidence)
        self.vertex_a.add_incidence(self.edge_a)
        self.assertIn(self.edge_a, self.vertex_a.incidence)

    def test_remove_incidence(self):
        """Tests the remove_incidence method."""
        self.assertEqual(0, len(self.vertex_a.incidence))
        self.vertex_a.add_incidence(self.edge_a)
        self.vertex_a.add_incidence(self.edge_d)
        self.assertIn(self.edge_a, self.vertex_a.incidence)
        self.assertIn(self.edge_d, self.vertex_a.incidence)
        self.assertEqual(2, len(self.vertex_a.incidence))
        self.vertex_a.remove_incidence(self.edge_a)
        self.assertNotIn(self.edge_a, self.vertex_a.incidence)
        self.assertEqual(1, len(self.vertex_a.incidence))
        # Test first other branch if statement
        self.vertex_a.remove_incidence(self.edge_c)
        self.assertNotIn(self.edge_c, self.vertex_a.incidence)
        self.assertEqual(1, len(self.vertex_a.incidence))
        # Test second incidence branch if statement
        self.assertEqual(0, len(self.vertex_b.incidence))
        self.vertex_b.add_incidence(self.edge_b)
        self.assertIn(self.edge_b, self.vertex_b.incidence)
        self.assertEqual(1, len(self.vertex_b.incidence))
        self.vertex_b.remove_incidence(self.edge_b)
        self.assertNotIn(self.edge_b, self.vertex_b.incidence)
        self.assertEqual(0, len(self.vertex_b.incidence))

    def test_incidence(self):
        """Tests the incidence method."""
        self.assertNotIn(self.edge_a, self.vertex_a.incidence)
        self.vertex_a.add_incidence(self.edge_a)
        self.assertIn(self.edge_a, self.vertex_a.incidence)

    def test_neighbours(self):
        """Tests the neighbours method."""
        self.assertNotIn(self.vertex_b, self.vertex_a.neighbours)
        self.vertex_a.add_incidence(self.edge_a)
        self.assertIn(self.vertex_b, self.vertex_a.neighbours)

    def test_degree(self):
        """Tests the degree method."""
        self.assertEqual(0, self.vertex_a.degree)
        self.vertex_a.add_incidence(self.edge_a)
        self.assertEqual(1, self.vertex_a.degree)
        self.vertex_a.add_incidence(self.edge_c)
        self.assertEqual(2, self.vertex_a.degree)


class EdgeTest(unittest.TestCase):
    """Class for testing the Edge class in graph.py"""

    def setUp(self) -> None:
        """Sets up the test cases for graph_computation.py"""
        self.graph_a = Graph(False, "a")
        self.vertex_a = Vertex("1", self.graph_a, {})
        self.vertex_b = Vertex("2", self.graph_a, {})
        self.vertex_c = Vertex("3", self.graph_a, {})
        self.edge_a = Edge(self.vertex_a, self.vertex_b, "1")
        self.edge_b = Edge(self.vertex_b, self.vertex_c, "2")

    def test_tail(self):
        """Tests the tail method."""
        self.assertEqual(self.vertex_b, self.edge_a.head)

    def test_head(self):
        """Tests the head method."""
        self.assertEqual(self.vertex_a, self.edge_a.tail)

    def test_other_end(self):
        """Tests the other_end method."""
        self.assertEqual(self.vertex_b, self.edge_a.other_end(self.vertex_a))
        self.assertEqual(self.vertex_a, self.edge_a.other_end(self.vertex_b))
        with self.assertRaises(GraphError) as context:
            self.edge_a.other_end(self.vertex_c)
        self.assertEqual('edge.other_end(vertex): vertex must be head or tail of edge',
                         str(context.exception))

    def test_incident(self):
        """Tests the incident method."""
        self.assertTrue(self.edge_a.incident(self.vertex_a))
        self.assertFalse(self.edge_a.incident(self.vertex_c))


class GraphTest(unittest.TestCase):
    """Class for testing the Graph class in graph.py"""

    def setUp(self) -> None:
        """Sets up the test cases"""
        self.graph_a = Graph(False, "a")
        self.graph_b = Graph(False, "b")
        self.vertex_a = Vertex("1", self.graph_a, {})
        self.vertex_b = Vertex("2", self.graph_a, {})
        self.vertex_c = Vertex("3", self.graph_a, {})
        self.edge_a = Edge(self.vertex_a, self.vertex_b, "1")
        self.edge_b = Edge(self.vertex_b, self.vertex_c, "2")

    def test_get_graph_id(self):
        """Tests the get_graph_id method."""
        self.assertEqual("a", self.graph_a.graph_id)

    def test_set_graph_id(self):
        """Tests the set_graph_id method."""
        self.assertEqual("a", self.graph_a.graph_id)
        self.graph_a.graph_id = "b"
        self.assertEqual("b", self.graph_a.graph_id)

    def test_directed(self):
        """Tests the directed method."""
        self.assertFalse(self.graph_a.directed)

    def test_vertices(self):
        """Tests the vertices method."""
        self.assertEqual(0, len(self.graph_a.vertices))

    def test_edges(self):
        """Tests the edges method."""
        self.assertEqual(0, len(self.graph_a.edges))

    def test_add_vertex(self):
        """Tests the add_vertex method."""
        self.graph_a.add_vertex(self.vertex_a)
        self.assertEqual(1, len(self.graph_a.vertices))
        self.assertEqual(self.vertex_a, self.graph_a.vertices[0])
        graph_dkd = Graph(True, "asdf")
        with self.assertRaises(GraphError) as context:
            self.graph_a.add_vertex(Vertex("adsf", graph_dkd, {}))
        self.assertEqual('A vertex must belong to the graph it is added to',
                         str(context.exception))

    def test_add_vertices(self):
        """Tests the add_vertex method."""
        self.graph_a.add_vertices([self.vertex_a, self.vertex_b])
        self.assertEqual(2, len(self.graph_a.vertices))
        self.assertEqual(self.vertex_a, self.graph_a.vertices[0])
        self.assertEqual(self.vertex_b, self.graph_a.vertices[1])

    def test_add_edge(self):
        """Tests the add_edge method."""
        self.graph_a.add_vertex(self.vertex_a)
        self.graph_a.add_vertex(self.vertex_b)
        self.graph_a.add_edge(self.edge_a)
        self.assertEqual(1, len(self.graph_a.edges))
        self.assertEqual(self.edge_a, self.graph_a.edges[0])

    def test_add_edges(self):
        """Tests the add_edges method."""
        self.graph_a.add_vertex(self.vertex_a)
        self.graph_a.add_vertex(self.vertex_b)
        self.graph_a.add_vertex(self.vertex_c)
        edges = [self.edge_a, self.edge_b]
        self.graph_a.add_edges(edges)
        self.assertEqual(2, len(self.graph_a.edges))
        self.assertEqual(self.edge_a, self.graph_a.edges[0])
        self.assertEqual(self.edge_b, self.graph_a.edges[1])

    def test_find_edge(self):
        """Tests the find_edge method."""
        self.graph_a.add_vertex(self.vertex_a)
        self.graph_a.add_vertex(self.vertex_b)
        self.graph_a.add_vertex(self.vertex_c)
        edges = [self.edge_a, self.edge_b]
        self.graph_a.add_edges(edges)
        self.assertEqual(self.edge_a, self.graph_a.find_edge("1"))
        self.assertEqual(self.edge_b, self.graph_a.find_edge("2"))
        self.assertIsNone(self.graph_a.find_edge("5"))

    def test_find_vertex(self):
        """Tests the find_vertex method."""
        self.graph_a.add_vertex(self.vertex_a)
        self.graph_a.add_vertex(self.vertex_b)
        self.assertEqual(self.vertex_a, self.graph_a.find_vertex("1"))
        self.assertEqual(self.vertex_b, self.graph_a.find_vertex("2"))
        self.assertIsNone(self.graph_a.find_vertex("5"))

    def test_is_adjacent(self):
        """Tests the is_adjacent method."""
        self.graph_a.add_vertex(self.vertex_a)
        self.graph_a.add_vertex(self.vertex_b)
        self.graph_a.add_vertex(self.vertex_c)
        self.graph_a.add_edge(self.edge_a)
        self.assertTrue(self.graph_a.is_adjacent(self.vertex_a,
                                                 self.vertex_b))
        self.assertFalse(self.graph_a.is_adjacent(self.vertex_b,
                                                  self.vertex_c))
        graph_b = Graph(True, "b")
        vertex_ba = Vertex("ba", graph_b, {})
        vertex_bb = Vertex("bb", graph_b, {})
        edge_ab = Edge(vertex_ba, vertex_bb, "0")
        graph_b.add_vertices([vertex_ba, vertex_bb])
        graph_b.add_edge(edge_ab)
        self.assertTrue(graph_b.is_adjacent(vertex_ba, vertex_bb))
        self.assertFalse(graph_b.is_adjacent(vertex_bb, vertex_ba))

    def test_del_edge(self):
        """Tests the del_edge method."""
        self.graph_a.add_vertex(self.vertex_a)
        self.graph_a.add_vertex(self.vertex_b)
        self.graph_a.add_edge(self.edge_a)
        self.assertEqual(1, len(self.graph_a.edges))
        self.assertIn(self.edge_a, self.graph_a.edges)
        self.graph_a.del_edge(self.edge_a)
        self.assertEqual(0, len(self.graph_a.edges))
        self.assertNotIn(self.edge_a, self.graph_a.edges)
        self.graph_a.del_edge(self.edge_a)
        self.assertEqual(0, len(self.graph_a.edges))
        self.assertNotIn(self.edge_a, self.graph_a.edges)

    def test_del_vertex(self):
        """Tests the del_vertex method."""
        self.graph_a.add_vertex(self.vertex_a)
        self.assertEqual(1, len(self.graph_a.vertices))
        self.assertIn(self.vertex_a, self.graph_a.vertices)
        self.graph_a.del_vertex(self.vertex_a)
        self.assertEqual(0, len(self.graph_a.vertices))
        self.assertNotIn(self.vertex_a, self.graph_a.vertices)
        self.graph_a.del_vertex(self.vertex_a)
        self.assertEqual(0, len(self.graph_a.vertices))
        self.assertNotIn(self.vertex_a, self.graph_a.vertices)

    def test_union(self):
        """Tests a union of two graphs."""
        vertex_d = Vertex("4", self.graph_b, {})
        self.graph_a.add_vertex(self.vertex_a)
        self.graph_a.add_vertex(self.vertex_b)
        self.graph_a.add_vertex(self.vertex_c)
        self.graph_b.add_vertex(vertex_d)
        edges = [self.edge_a, self.edge_b]
        self.graph_a.add_edges(edges)
        self.assertFalse(vertex_d in self.graph_a)
        self.graph_a = union(self.graph_a, self.graph_b)
        self.assertTrue(vertex_d in self.graph_a)


if __name__ == '__main__':
    unittest.main()

"""
This file contains a class for testing graph_io.py
"""
import json
import os
import os.path
import shutil
import unittest

from test.util.graph_test_helpers import setup_graph_vertex_edge_vocabularies, \
    fill_graphs_bare, captured_output
from src.model.graph import Graph
from src.model_io.graph_io import print_graph_or_graphlist, \
    get_label_from_attributes, write_single_graph_to_gv, \
    write_graphs_to_gv_files, write_graph_or_graphlist, read_vertex_from_line, \
    GraphReadError, read_gv_file

FILE_ROOT = os.path.dirname(os.path.abspath(__file__))


class GraphIOTest(unittest.TestCase):
    """Class for testing graph_io.py"""
    GRAPHS1GV = "graphs1.gv"
    GRAPHS2GV = "graphs2.gv"

    def setUp(self) -> None:
        """Runs the same setup for each test case."""
        self.graphs, self.vertices, self.edges = \
            setup_graph_vertex_edge_vocabularies()
        fill_graphs_bare(self.graphs, self.vertices, self.edges)
        os.mkdir(os.path.join(FILE_ROOT, "test_graphs"))

    def tearDown(self) -> None:
        """Runs the same teardown for each test case."""
        shutil.rmtree(os.path.join(FILE_ROOT, "test_graphs"))

    def test_print_graph_or_graphlist(self):
        """Tests the print_graphs method on what the output is with the bare
        fill graphs."""
        self.graphs = list(self.graphs.values())
        with captured_output() as (out, _):
            print_graph_or_graphlist(self.graphs)
            expected_output = "# Number of vertices:\n0\n# Edge list:\n--- " \
                              "Next graph:\n# Number of vertices:\n2\n# Edge " \
                              "list:\n1,0\n--- Next graph:\n# Number of " \
                              "vertices:\n2\n# Edge list:\n1,0"
            output = out.getvalue().strip()
            self.assertEqual(output, expected_output)
        with captured_output() as (out, _):
            print_graph_or_graphlist(self.graphs[0])
            output = out.getvalue().strip()
            self.assertEqual(output, "# Number of vertices:\n0\n# Edge list:")
        with captured_output() as (out, _):
            print_graph_or_graphlist(self.graphs[0], "36")
            output = out.getvalue().strip()
            self.assertEqual(output, "# Number of vertices:\n0\n# Edge list:")
        with captured_output() as (out, _):
            print_graph_or_graphlist(self.graphs[0], "a")
            output = out.getvalue().strip()
            self.assertEqual(output, "a\n# Number of vertices:\n0\n# Edge "
                                     "list:")

    def test_write_graph_or_graphlist(self):
        """Tests the write_graph_or_graphlist method on what the output is with
        the bare fill graphs."""
        self.graphs = list(self.graphs.values())
        with open(os.path.join(FILE_ROOT,
                               "test_graphs", self.GRAPHS1GV), "w",
                  encoding="utf-8") as file:
            write_graph_or_graphlist(self.graphs[0], file)
        self.assertTrue(os.path.isfile(os.path.join(FILE_ROOT, "test_graphs",
                                                    self.GRAPHS1GV)))
        with open(os.path.join(FILE_ROOT,
                               "test_graphs", self.GRAPHS2GV), "w",
                  encoding="utf-8") as file:
            write_graph_or_graphlist(self.graphs, file)
        self.assertTrue(os.path.isfile(os.path.join(FILE_ROOT, "test_graphs",
                                                    self.GRAPHS2GV)))

    def test_write_single_graph_to_gv(self):
        """Tests the write_gv class with one undirected graph and one
        directed graph."""
        self.graphs = list(self.graphs.values())
        with open(os.path.join(FILE_ROOT,
                               "test_graphs", self.GRAPHS1GV), "w",
                  encoding="utf-8") as file:
            write_single_graph_to_gv(self.graphs[1], file)
        self.assertTrue(os.path.isfile(os.path.join(FILE_ROOT, "test_graphs",
                                                    self.GRAPHS1GV)))
        with open(os.path.join(FILE_ROOT,
                               "test_graphs", self.GRAPHS2GV), "w",
                  encoding="utf-8") as file:
            write_single_graph_to_gv(self.graphs[2], file)
        self.assertTrue(os.path.isfile(os.path.join(FILE_ROOT, "test_graphs",
                                                    self.GRAPHS2GV)))

    def test_get_label_from_attributes(self):
        """Tests the get_label_from_attributes method. Transforms the attribute
        dict into a string representation for saving in a file."""
        attributes = {"Key1": "Value1",
                      "Key2": 2,
                      "Key3": "Value3"}
        result = get_label_from_attributes(attributes)
        self.assertEqual(result, json.dumps(attributes).replace('\"', '`'))
        self.assertEqual(get_label_from_attributes(None), json.dumps("{}")
                         .replace('\"', '`'))

    def test_write_graphs_to_gv_files(self):
        """Tests the write_floors_to_gv_files method. One where the dir
        already exists, one where it should create a directory."""
        self.graphs = list(self.graphs.values())
        write_graphs_to_gv_files(self.graphs, os.path.join(FILE_ROOT,
                                                           "test_graphs"))
        self.assertTrue(os.path.isdir(os.path.join(FILE_ROOT, "test_graphs")))
        write_graphs_to_gv_files(self.graphs, os.path.join(FILE_ROOT,
                                                           "test_graphs",
                                                           "Woeter"))
        self.assertTrue(os.path.isdir(os.path.join(FILE_ROOT, "test_graphs",
                                                   "Woeter")))

    def test_read_vertex_from_line(self):
        """Tests the read_vertex_from_line method."""
        graph = Graph(False, "a")
        read_vertex_from_line(graph, "asdf")
        self.assertEqual(0, len(graph.vertices))
        with self.assertRaises(GraphReadError) as context:
            read_vertex_from_line(graph, "V=,label=\"vertexlabel*\","
                                         "extra_label=\"`{}`\"")
        self.assertTrue('Vertex_id is not an alphanumeric! VID: vertexlabel*'
                        in str(context.exception))
        self.assertEqual(0, len(graph.vertices))
        read_vertex_from_line(graph, "V=,label=\"vertexlabel\","
                                     "extra_label=\"`{}`\"")
        self.assertEqual(1, len(graph.vertices))

    def test_read_gv_file(self):
        """Tests the write_floors_to_gv_files method. One where the dir
        already exists, one where it should create a directory."""
        self.graphs = list(self.graphs.values())
        with open(os.path.join(FILE_ROOT,
                               "test_graphs", self.GRAPHS2GV), "w",
                  encoding="utf-8") as file:
            write_single_graph_to_gv(self.graphs[1], file)
        with open(os.path.join(FILE_ROOT,
                               "test_graphs", self.GRAPHS2GV), "r",
                  encoding="utf-8") as file:
            graph = read_gv_file(file)
        self.assertEqual(2, len(graph.vertices))
        self.assertEqual(1, len(graph.edges))
        self.assertEqual(self.GRAPHS2GV, graph.graph_id)
        self.assertIsNotNone(graph.find_edge("0"))
        self.assertIsNotNone(graph.find_vertex("A"))
        self.assertIsNotNone(graph.find_vertex("B"))
        self.assertEqual(graph.find_vertex("B"), graph.find_edge("0").head)
        self.assertEqual(graph.find_vertex("A"), graph.find_edge("0").tail)


if __name__ == '__main__':
    unittest.main()

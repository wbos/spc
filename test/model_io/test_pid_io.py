"""
This file contains a class for testing sft_io.py
"""
import os
import os.path
import shutil
import unittest
from typing import List

from src.model.pid import Component, PID, ComponentTypeEnum, ComponentType, Link
from src.model_io.pid_io import check_for_mdp, get_component_kbi_string, \
    pid_components_to_kbi_string, get_pids_kbi_strings, file_export, \
    get_link_kbi_string, pid_links

FILE_ROOT = os.path.dirname(os.path.abspath(__file__))


class PIDIOTest(unittest.TestCase):
    """Class for testing pid_io.py"""
    PIDS1GV = "pids1.kbi"
    PIDS2GV = "pids2.kbi"

    def setUp(self) -> None:
        """Runs the same setup for each test case."""
        os.mkdir(os.path.join(FILE_ROOT, "test_pids"))

    def tearDown(self) -> None:
        """Runs the same teardown for each test case."""
        shutil.rmtree(os.path.join(FILE_ROOT, "test_pids"))

    def test_check_for_mdp(self):
        """Tests the check_for_mdp method"""
        pid = PID(False, "PID_A")
        component = Component("C_A", pid, {})
        component.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        self.assertEqual("PID_A", check_for_mdp(component, pid))
        component.component_type = ComponentType(ComponentTypeEnum.TANK)
        self.assertEqual("", check_for_mdp(component, pid))

    def test_get_component_kbi_string(self):
        """Tests the get_component_kbi_string method"""
        pid = PID(False, "PID-A")
        component = Component("C-A", pid, {})
        component.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        self.assertEqual("\t<Objet Action=\"CREER\" "
                         "Nom=\"PID-A_C-A\" "
                         "Page=\"PID-A\" "
                         "Type=\"PID-A_pump"
                         "\">\n\t\t<Position X=\"0"
                         "\" Y=\"0"
                         "\"/>\n\t</Objet>\n\n",
                         get_component_kbi_string(component,
                                                  pid,
                                                  0,
                                                  0))

    def test_get_pipe_kbi_string(self):
        """Tests the get_pipe_kbi_string method"""
        pid = PID(False, "PID-A")
        component1 = Component("C-A", pid, {})
        component2 = Component("C-B", pid, {})
        component1.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component2.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        pipe = Link(component1, component2, "0")
        self.assertEqual('\t<Objet Action="CREER" '
                         'Nom="PID-A_fl_0" Page="PID-A" Type="fluid_link">\n'
                         '\t</Objet>\n\n'
                         '\t<Objet Nom="PID-A_fl_0">\n'
                         '\t\t<Cnx CnxObj="PID-A_C-B" '
                         'CnxPt="in" MonPt="ARRIVEE"/>\n'
                         '\t\t<Cnx CnxObj="PID-A_C-A" '
                         'CnxPt="out" MonPt="DEPART"/>\n'
                         '\t</Objet>\n\n',
                         get_link_kbi_string(pipe,
                                             pid))
        component3 = Component("C-A", pid, {})
        component4 = Component("C-B", pid, {})
        component3.component_type = ComponentType(
            ComponentTypeEnum.BASIC_EVENT)
        component4.component_type = ComponentType(
            ComponentTypeEnum.BASIC_EVENT)
        pipe = Link(component3, component4, "1")
        self.assertEqual('',
                         get_link_kbi_string(pipe,
                                             pid))

    def test_pid_components_to_kbi_string(self):
        """Tests the pid_components_to_kbi_string method"""
        pid = PID(False, "PID-A")
        component_a = Component("C-A", pid, {})
        component_b = Component("C-B", pid, {})
        component_c = Component("C-C", pid, {})
        component_d = Component("C-D", pid, {})
        component_a.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_b.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_c.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_d.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        pid.add_vertex(component_a)
        pid.add_vertex(component_b)
        pid.add_vertex(component_c)
        pid.add_vertex(component_d)
        string_to_test: str = pid_components_to_kbi_string(pid)
        self.assertIn("\t<Objet Action=\"CREER\" "
                      "Nom=\"PID-A_C-A\" "
                      "Page=\"PID-A\" "
                      "Type=\"PID-A_pump",
                      string_to_test)
        self.assertIn("\t<Objet Action=\"CREER\" "
                      "Nom=\"PID-A_C-B\" "
                      "Page=\"PID-A\" "
                      "Type=\"PID-A_pump",
                      string_to_test)
        self.assertIn("\t<Objet Action=\"CREER\" "
                      "Nom=\"PID-A_C-C\" "
                      "Page=\"PID-A\" "
                      "Type=\"PID-A_pump",
                      string_to_test)
        self.assertIn("\t<Objet Action=\"CREER\" "
                      "Nom=\"PID-A_C-D\" "
                      "Page=\"PID-A\" "
                      "Type=\"PID-A_pump",
                      string_to_test)
        self.assertEqual(1, string_to_test.count("\">\n\t\t<Position X=\"80"
                                                 "\" Y=\"60"))
        self.assertEqual(1, string_to_test.count("\">\n\t\t<Position X=\"200"
                                                 "\" Y=\"60"))
        self.assertEqual(1, string_to_test.count("\">\n\t\t<Position X=\"320"
                                                 "\" Y=\"60"))
        self.assertEqual(1, string_to_test.count("\">\n\t\t<Position X=\"80"
                                                 "\" Y=\"120"))

    def test_pid_pipes_to_kbi_string(self):
        """Tests the pid_components_to_kbi_string method"""
        pid = PID(False, "PID-A")
        component_a = Component("C-A", pid, {})
        component_b = Component("C-B", pid, {})
        component_c = Component("C-C", pid, {})
        component_a.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_b.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_c.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        pipe1 = Link(component_a, component_b, "1")
        pipe2 = Link(component_b, component_c, "2")
        pid.add_vertex(component_a)
        pid.add_vertex(component_b)
        pid.add_vertex(component_c)
        pid.add_edge(pipe1)
        pid.add_edge(pipe2)
        string_to_test: str = pid_links(pid)
        self.assertIn('\t<Objet Action="CREER" '
                      'Nom="PID-A_fl_1" Page="PID-A" Type="fluid_link">\n'
                      '\t</Objet>\n\n'
                      '\t<Objet Nom="PID-A_fl_1">\n'
                      '\t\t<Cnx CnxObj="PID-A_C-B" '
                      'CnxPt="in" MonPt="ARRIVEE"/>\n'
                      '\t\t<Cnx CnxObj="PID-A_C-A" '
                      'CnxPt="out" MonPt="DEPART"/>\n'
                      '\t</Objet>\n\n',
                      string_to_test)
        self.assertIn('\t<Objet Action="CREER" '
                      'Nom="PID-A_fl_2" Page="PID-A" Type="fluid_link">\n'
                      '\t</Objet>\n\n'
                      '\t<Objet Nom="PID-A_fl_2">\n'
                      '\t\t<Cnx CnxObj="PID-A_C-C" '
                      'CnxPt="in" MonPt="ARRIVEE"/>\n'
                      '\t\t<Cnx CnxObj="PID-A_C-B" '
                      'CnxPt="out" MonPt="DEPART"/>\n'
                      '\t</Objet>\n\n',
                      string_to_test)

    def test_get_pids_kbi_strings(self):
        """Tests the get_pids_kbi_strings method"""
        pid_a = PID(False, "PID-A")
        pid_b = PID(False, "PID-B")
        pid_c = PID(False, "PID-C")
        pid_d = PID(False, "PID-D")
        component_aa = Component("C-AA", pid_a, {})
        component_ab = Component("C-AB", pid_a, {})
        component_ac = Component("C-AC", pid_a, {})
        component_ad = Component("C-AD", pid_a, {})
        component_ae = Component("C-AE", pid_a, {})
        component_ba = Component("C-BA", pid_b, {})
        component_bb = Component("C-BB", pid_b, {})
        component_bc = Component("C-BC", pid_b, {})
        component_bd = Component("C-BD", pid_b, {})
        component_be = Component("C-BE", pid_b, {})
        component_aa.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_ab.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_ac.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_ad.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_ae.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_ba.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_bb.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_bc.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_bd.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_be.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        pid_a.add_vertices([component_aa,
                            component_ab,
                            component_ac,
                            component_ad,
                            component_ae])
        pid_b.add_vertices([component_ba,
                            component_bb,
                            component_bc,
                            component_bd,
                            component_be])
        pid_a.add_edges([Link(component_aa, component_ab, "1"),
                         Link(component_ab, component_ac, "2"),
                         Link(component_ac, component_ad, "3")])
        pid_b.add_edges([Link(component_bd, component_bc, "1"),
                         Link(component_bc, component_bb, "2"),
                         Link(component_bb, component_ba, "3")])
        test_strings: (str, str, str) = get_pids_kbi_strings([pid_a, pid_b, pid_c, pid_d])
        self.assertIn("\t<Page Action=\"CREER\" "
                      "Mere=\"Main_page\" "
                      "Nom=\"PID-A\" ",
                      test_strings[0])
        self.assertIn("\t<Page Action=\"CREER\" "
                      "Mere=\"Main_page\" "
                      "Nom=\"PID-B\" ",
                      test_strings[0])
        self.assertIn("\t<Page Action=\"CREER\" "
                      "Mere=\"Main_page\" "
                      "Nom=\"PID-C\" ",
                      test_strings[0])
        self.assertIn("\t<Page Action=\"CREER\" "
                      "Mere=\"Main_page\" "
                      "Nom=\"PID-D\" ",
                      test_strings[0])
        self.assertEqual(1, test_strings[0].count("X=\"80\" Y=\"60"))
        self.assertEqual(1, test_strings[0].count("X=\"200\" Y=\"60"))
        self.assertEqual(1, test_strings[0].count("X=\"320\" Y=\"60"))
        self.assertEqual(1, test_strings[0].count("X=\"80\" Y=\"120"))
        self.assertIn("\t<Objet Action=\"CREER\" "
                      "Nom=\"PID-A_C-AA\" "
                      "Page=\"PID-A\" "
                      "Type=\"PID-A_pump",
                      test_strings[1])
        self.assertIn("\t<Objet Action=\"CREER\" "
                      "Nom=\"PID-A_C-AB\" "
                      "Page=\"PID-A\" "
                      "Type=\"PID-A_pump",
                      test_strings[1])
        self.assertIn("\t<Objet Action=\"CREER\" "
                      "Nom=\"PID-A_C-AC\" "
                      "Page=\"PID-A\" "
                      "Type=\"PID-A_pump",
                      test_strings[1])
        self.assertIn("\t<Objet Action=\"CREER\" "
                      "Nom=\"PID-A_C-AD\" "
                      "Page=\"PID-A\" "
                      "Type=\"PID-A_pump",
                      test_strings[1])
        self.assertIn("\t<Objet Action=\"CREER\" "
                      "Nom=\"PID-A_C-AE\" "
                      "Page=\"PID-A\" "
                      "Type=\"PID-A_pump",
                      test_strings[1])
        self.assertEqual(2, test_strings[1].count("\">\n\t\t<Position X=\"80"
                                                  "\" Y=\"60"))
        self.assertEqual(2, test_strings[1].count("\">\n\t\t<Position X=\"200"
                                                  "\" Y=\"60"))
        self.assertEqual(2, test_strings[1].count("\">\n\t\t<Position X=\"320"
                                                  "\" Y=\"60"))
        self.assertEqual(2, test_strings[1].count("\">\n\t\t<Position X=\"80"
                                                  "\" Y=\"120"))
        self.assertEqual(2, test_strings[1].count("\">\n\t\t<Position X=\"200"
                                                  "\" Y=\"120"))
        self.assertIn('\t<Objet Action="CREER" '
                      'Nom="PID-A_fl_1" Page="PID-A" Type="fluid_link">\n'
                      '\t</Objet>\n\n'
                      '\t<Objet Nom="PID-A_fl_1">\n'
                      '\t\t<Cnx CnxObj="PID-A_C-AB" '
                      'CnxPt="in" MonPt="ARRIVEE"/>\n'
                      '\t\t<Cnx CnxObj="PID-A_C-AA" '
                      'CnxPt="out" MonPt="DEPART"/>\n'
                      '\t</Objet>\n\n',
                      test_strings[2])
        self.assertIn('\t<Objet Action="CREER" '
                      'Nom="PID-A_fl_2" Page="PID-A" Type="fluid_link">\n'
                      '\t</Objet>\n\n'
                      '\t<Objet Nom="PID-A_fl_2">\n'
                      '\t\t<Cnx CnxObj="PID-A_C-AC" '
                      'CnxPt="in" MonPt="ARRIVEE"/>\n'
                      '\t\t<Cnx CnxObj="PID-A_C-AB" '
                      'CnxPt="out" MonPt="DEPART"/>\n'
                      '\t</Objet>\n\n',
                      test_strings[2])
        self.assertIn('\t<Objet Action="CREER" '
                      'Nom="PID-A_fl_3" Page="PID-A" Type="fluid_link">\n'
                      '\t</Objet>\n\n'
                      '\t<Objet Nom="PID-A_fl_3">\n'
                      '\t\t<Cnx CnxObj="PID-A_C-AD" '
                      'CnxPt="in" MonPt="ARRIVEE"/>\n'
                      '\t\t<Cnx CnxObj="PID-A_C-AC" '
                      'CnxPt="out" MonPt="DEPART"/>\n'
                      '\t</Objet>\n\n',
                      test_strings[2])
        self.assertIn('\t<Objet Action="CREER" '
                      'Nom="PID-B_fl_1" Page="PID-B" Type="fluid_link">\n'
                      '\t</Objet>\n\n'
                      '\t<Objet Nom="PID-B_fl_1">\n'
                      '\t\t<Cnx CnxObj="PID-B_C-BC" '
                      'CnxPt="in" MonPt="ARRIVEE"/>\n'
                      '\t\t<Cnx CnxObj="PID-B_C-BD" '
                      'CnxPt="out" MonPt="DEPART"/>\n'
                      '\t</Objet>\n\n',
                      test_strings[2])
        self.assertIn('\t<Objet Action="CREER" '
                      'Nom="PID-B_fl_2" Page="PID-B" Type="fluid_link">\n'
                      '\t</Objet>\n\n'
                      '\t<Objet Nom="PID-B_fl_2">\n'
                      '\t\t<Cnx CnxObj="PID-B_C-BB" '
                      'CnxPt="in" MonPt="ARRIVEE"/>\n'
                      '\t\t<Cnx CnxObj="PID-B_C-BC" '
                      'CnxPt="out" MonPt="DEPART"/>\n'
                      '\t</Objet>\n\n',
                      test_strings[2])
        self.assertIn('\t<Objet Action="CREER" '
                      'Nom="PID-B_fl_3" Page="PID-B" Type="fluid_link">\n'
                      '\t</Objet>\n\n'
                      '\t<Objet Nom="PID-B_fl_3">\n'
                      '\t\t<Cnx CnxObj="PID-B_C-BA" '
                      'CnxPt="in" MonPt="ARRIVEE"/>\n'
                      '\t\t<Cnx CnxObj="PID-B_C-BB" '
                      'CnxPt="out" MonPt="DEPART"/>\n'
                      '\t</Objet>\n\n',
                      test_strings[2])

    def test_file_export(self):
        """Tests the file_export method"""
        pid_a = PID(False, "PID-A")
        pid_b = PID(False, "PID-B")
        component_aa = Component("C-AA", pid_a, {})
        component_ab = Component("C-AB", pid_a, {})
        component_ac = Component("C-AC", pid_a, {})
        component_ad = Component("C-AD", pid_a, {})
        component_ba = Component("C-BA", pid_b, {})
        component_bb = Component("C-BB", pid_b, {})
        component_bc = Component("C-BC", pid_b, {})
        component_bd = Component("C-BD", pid_b, {})
        component_aa.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_ab.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_ac.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_ad.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_ba.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_bb.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_bc.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        component_bd.component_type = ComponentType(
            ComponentTypeEnum.MOTOR_DRIVEN_PUMP)
        pid_a.add_vertex(component_aa)
        pid_a.add_vertex(component_ab)
        pid_a.add_vertex(component_ac)
        pid_a.add_vertex(component_ad)
        pid_b.add_vertex(component_ba)
        pid_b.add_vertex(component_bb)
        pid_b.add_vertex(component_bc)
        pid_b.add_vertex(component_bd)
        pid_list: List[PID] = [pid_a, pid_b]
        file_export(pid_list, os.path.join(FILE_ROOT, "test_pids"), self.PIDS1GV)
        self.assertTrue(os.path.isfile(os.path.join(FILE_ROOT,
                                                    "test_pids",
                                                    self.PIDS1GV)))


if __name__ == '__main__':
    unittest.main()

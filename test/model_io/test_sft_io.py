"""
This file contains a class for testing pid_io.py
"""
import os.path
import unittest
from typing import List

from src.model.graph import Edge, Vertex
from src.model.sft import SFT, FaultCode, GateType, Gate, SFTAttributes, BasicEvent
from src.model_io.sft_io import parse_sft_label_from_ftr_rsa_line, \
    parse_sft_be_label_tuple_from_bev_rsa_line, \
    parse_sft_from_rsa_line, \
    parse_be_from_bev_rsa_line, parse_sfts_from_lines, \
    parse_gate_from_gat_rsa_line_check_whether_sft_exists, \
    SFTReadError, parse_fault_code_from_string, \
    add_gate_to_sft, parse_gate_type_and_k_value_from_gat_rsa_line, \
    parse_gate_label_from_gat_rsa_line, \
    parse_gat_rsa_line_values, \
    parse_fault_code_from_bev_rsa_line, \
    parse_gate_from_gat_rsa_line, \
    parse_input_label_from_gin_rsa_line, create_new_gate_and_link_to_existing_gate, \
    link_gate_input_to_gate, \
    attempt_linking_gate_input_to_gate, \
    parse_gate_input_from_gin_rsa_line, \
    create_new_be_and_link_to_existing_gate

FILE_ROOT = os.path.dirname(os.path.abspath(__file__))


class SFTIOTest(unittest.TestCase):
    """Class for testing graph_io.py"""

    def test_parse_sft_label_from_ftr_rsa_line(self):
        """Tests the parse_sft_label_from_ftr_rsa_line method"""
        parse_string1 = "FTR\t\" ACP"
        parse_string2 = "FTR\t\" MB-ACP"
        self.assertEqual("ACP",
                         parse_sft_label_from_ftr_rsa_line(parse_string1))
        self.assertEqual("ACP",
                         parse_sft_label_from_ftr_rsa_line(parse_string2))

    def test_parse_fault_code_from_string(self):
        """Tests the parse_fault_code_from_string method"""
        parse_string1 = "A"
        parse_string2 = "B"
        parse_string3 = "C"
        parse_string4 = "D"
        parse_string5 = "H"
        parse_string6 = "T"
        parse_string7 = "U"
        parse_string8 = "M"
        parse_string9 = "X"
        parse_string10 = "MA"
        parse_string11 = "MADFAS"
        self.assertEqual(FaultCode.A,
                         parse_fault_code_from_string(parse_string1))
        self.assertEqual(FaultCode.B,
                         parse_fault_code_from_string(parse_string2))
        self.assertEqual(FaultCode.C,
                         parse_fault_code_from_string(parse_string3))
        self.assertEqual(FaultCode.D,
                         parse_fault_code_from_string(parse_string4))
        self.assertEqual(FaultCode.H,
                         parse_fault_code_from_string(parse_string5))
        self.assertEqual(FaultCode.T,
                         parse_fault_code_from_string(parse_string6))
        self.assertEqual(FaultCode.U,
                         parse_fault_code_from_string(parse_string7))
        self.assertEqual(FaultCode.M,
                         parse_fault_code_from_string(parse_string8))
        self.assertEqual(FaultCode.X,
                         parse_fault_code_from_string(parse_string9))
        self.assertEqual(FaultCode.M,
                         parse_fault_code_from_string(parse_string10))
        self.assertEqual(FaultCode.X,
                         parse_fault_code_from_string(parse_string11))

    def test_parse_sft_be_label_tuple_from_bev_rsa_line(self):
        """Tests the parse_sft_be_label_tuple_from_bev_rsa_line method"""
        parse_string1 = "BEV\t\" ACP-MDP"
        parse_string2 = "BEV\t\" ACP_MDP"
        parse_string3 = "BEV\t\" ACP-T1-MDP"
        parse_string4 = "BEV\t\" ACP-T2-MDP"
        parse_string5 = "BEV\t\" ACP-T3-MDP"
        parse_string6 = "BEV\t\" ACP-T3-MDP-M"
        parse_string7 = "BEV\t\" ACP-MDP-M"
        string_tuple1 = parse_sft_be_label_tuple_from_bev_rsa_line(
            parse_string1)
        string_tuple2 = parse_sft_be_label_tuple_from_bev_rsa_line(
            parse_string2)
        string_tuple3 = parse_sft_be_label_tuple_from_bev_rsa_line(
            parse_string3)
        string_tuple4 = parse_sft_be_label_tuple_from_bev_rsa_line(
            parse_string4)
        string_tuple5 = parse_sft_be_label_tuple_from_bev_rsa_line(
            parse_string5)
        string_tuple6 = parse_sft_be_label_tuple_from_bev_rsa_line(
            parse_string6)
        string_tuple7 = parse_sft_be_label_tuple_from_bev_rsa_line(
            parse_string7)
        self.assertEqual("ACP", string_tuple1[0])
        self.assertEqual("ACP", string_tuple2[0])
        self.assertEqual("ACP", string_tuple3[0])
        self.assertEqual("ACP", string_tuple4[0])
        self.assertEqual("ACP", string_tuple5[0])
        self.assertEqual("MDP", string_tuple1[1])
        self.assertEqual("MDP", string_tuple2[1])
        self.assertEqual("MDPT1", string_tuple3[1])
        self.assertEqual("MDPT2", string_tuple4[1])
        self.assertEqual("MDPT3", string_tuple5[1])
        self.assertEqual("MDPT3", string_tuple6[1])
        self.assertEqual("MDP", string_tuple7[1])

    def test_parse_fault_code_from_bev_rsa_line(self):
        """Tests the parse_fault_code_from_bev_rsa_line method"""
        parse_string1 = ["ACP", "MDP"]
        parse_string2 = ["ACP", "T1", "MDP"]
        parse_string3 = ["ACP", "T2", "MDP"]
        parse_string4 = ["ACP", "T3", "MDP"]
        parse_string5 = ["ACP", "T3", "MDP", "M"]
        parse_string6 = ["ACP", "MDP", "M"]
        fault_code1 = parse_fault_code_from_bev_rsa_line(parse_string1)
        fault_code2 = parse_fault_code_from_bev_rsa_line(parse_string2)
        fault_code3 = parse_fault_code_from_bev_rsa_line(parse_string3)
        fault_code4 = parse_fault_code_from_bev_rsa_line(parse_string4)
        fault_code5 = parse_fault_code_from_bev_rsa_line(parse_string5)
        fault_code6 = parse_fault_code_from_bev_rsa_line(parse_string6)
        self.assertEqual(FaultCode.X, fault_code1)
        self.assertEqual(FaultCode.X, fault_code2)
        self.assertEqual(FaultCode.X, fault_code3)
        self.assertEqual(FaultCode.X, fault_code4)
        self.assertEqual(FaultCode.M, fault_code5)
        self.assertEqual(FaultCode.M, fault_code6)

    def test_parse_sft_from_rsa_line(self):
        """Tests the parse_sft_from_rsa_line method"""
        parse_string1 = "FTR\t\" ACP"
        parse_string2 = "FTR\t\" MB-ACP"
        sfts1: List[SFT] = []
        sfts2: List[SFT] = []
        parse_sft_from_rsa_line(parse_string1, sfts1)
        parse_sft_from_rsa_line(parse_string2, sfts2)
        self.assertEqual("ACP", sfts1[0].graph_id)
        self.assertEqual(1, len(sfts1))
        self.assertEqual("ACP", sfts2[0].graph_id)
        self.assertEqual(1, len(sfts2))
        self.assertTrue(sfts1[0].directed)
        self.assertIsInstance(sfts1[0], SFT)
        parse_sft_from_rsa_line(parse_string1, sfts1)
        self.assertEqual(1, len(sfts1))

    def test_parse_be_from_bev_rsa_line(self):
        """Tests the parse_be_from_bev_rsa_line method"""
        sfts: List[SFT] = [SFT("ACP", True)]
        parse_string1 = "BEV\t\" ACP-MDP"
        parse_string2 = "BEV\t\" ACP_MDP"
        parse_string3 = "BEV\t\" ACP-T1-MDP"
        parse_string4 = "BEV\t\" ACP-T2-MDP"
        parse_string5 = "BEV\t\" ACP-T3-MDP"
        parse_string6 = "BEV\t\" ACP"
        parse_be_from_bev_rsa_line(parse_string1, sfts)
        parse_be_from_bev_rsa_line(parse_string2, sfts)
        parse_be_from_bev_rsa_line(parse_string3, sfts)
        parse_be_from_bev_rsa_line(parse_string4, sfts)
        parse_be_from_bev_rsa_line(parse_string5, sfts)
        parse_be_from_bev_rsa_line(parse_string6, sfts)
        res_sft = sfts.pop()
        self.assertEqual(4, len(res_sft.vertices))
        res_sft_vertex_ids = {vertex.vertex_id for vertex in res_sft.vertices}
        self.assertIn("MDP", res_sft_vertex_ids)
        self.assertIn("MDPT1", res_sft_vertex_ids)
        self.assertIn("MDPT2", res_sft_vertex_ids)
        self.assertIn("MDPT3", res_sft_vertex_ids)
        self.assertNotIn("ACP", res_sft_vertex_ids)

    def test_parse_gate_from_gat_rsa_line(self):
        """Tests the parse_gate_from_gat_rsa_line method"""
        sfts: List[SFT] = []
        with self.assertRaises(SFTReadError) as context:
            parse_gate_from_gat_rsa_line('GAT	'
                                         '"@ECC-1-1                                          '
                                         '"	OR	0	'
                                         '"ECC-1                                             '
                                         '"	1	'
                                         '"Emergency Core ',
                                         sfts)
        self.assertEqual('There should exist an '
                         'SFT ECC-1 to which I can add '
                         'Gate @ECC-1-1 of type OR',
                         str(context.exception))
        sfts.append(SFT("ECC-1"))
        gate = parse_gate_from_gat_rsa_line('GAT	'
                                            '"@ECC-1-1                                          '
                                            '"	OR	0	'
                                            '"ECC-1                                             '
                                            '"	1	'
                                            '"Emergency Core ',
                                            sfts)
        self.assertIsNotNone(gate)
        self.assertEqual(GateType.OR, gate.gate_type)
        self.assertEqual("@ECC-1-1", gate.vertex_id)
        self.assertEqual(sfts[0].sft_attributes.top_event, gate)
        self.assertEqual("@ECC-1-1", sfts[0].sft_attributes.top_event.vertex_id)

    def test_parse_gat_rsa_line_values(self):
        """Tests the parse_gat_rsa_line_values method"""
        gate_label, gate_type_str, link_to_te, sft_label = \
            parse_gat_rsa_line_values('GAT	'
                                      '"@ECC-1-1                                          '
                                      '"	OR	0	'
                                      '"ECC-1                                             '
                                      '"	1	'
                                      '"Emergency Core ')
        self.assertEqual("@ECC-1-1", gate_label)
        self.assertEqual("OR", gate_type_str)
        self.assertEqual("1", link_to_te)
        self.assertEqual("ECC-1", sft_label)

    def test_parse_gate_from_gat_rsa_line_check_whether_sft_exists(self):
        """Tests the parse_gate_from_gat_rsa_line_check_whether_sft_exists method"""
        list_with_sft: List[SFT] = [SFT("ACP", True)]
        list_wihtout_sft: List[SFT] = []
        gate_label = "@ACP-1"
        gate_type_str = "AND"
        sft_label = "ACP"
        parse_gate_from_gat_rsa_line_check_whether_sft_exists(
            list_with_sft,
            gate_label,
            gate_type_str,
            sft_label)
        with self.assertRaises(SFTReadError) as context:
            parse_gate_from_gat_rsa_line_check_whether_sft_exists(
                list_wihtout_sft,
                gate_label,
                gate_type_str,
                sft_label)
        self.assertEqual('There should exist an '
                         'SFT ACP to which I can add '
                         'Gate @ACP-1 of type AND',
                         str(context.exception))

    def test_add_gate_to_sft(self):
        """Tests the add_gate_to_sft method"""
        existing_sft = SFT("ACP", True)
        te_gate = Gate("TE", existing_sft)
        existing_sft.sft_attributes = SFTAttributes(te_gate)
        self.assertEqual(GateType.AND,
                         existing_sft
                         .sft_attributes
                         .top_event
                         .gate_type)
        self.assertEqual(0, existing_sft.sft_attributes.top_event.k_value)
        self.assertEqual(0,
                         len(existing_sft.sft_attributes.gates))
        res = add_gate_to_sft(existing_sft,
                              "",
                              GateType.VOT,
                              3,
                              "0")
        self.assertEqual(res, existing_sft.sft_attributes.top_event)
        self.assertEqual(GateType.VOT,
                         existing_sft
                         .sft_attributes
                         .top_event
                         .gate_type)
        self.assertEqual(3, existing_sft.sft_attributes.top_event.k_value)
        self.assertEqual(0,
                         len(existing_sft.sft_attributes.gates))
        res = add_gate_to_sft(existing_sft,
                              "L1",
                              GateType.OR,
                              1,
                              "0")
        self.assertIn(res, existing_sft.sft_attributes.gates)
        self.assertEqual(1,
                         len(existing_sft.sft_attributes.gates))
        self.assertEqual(GateType.OR, res.gate_type)
        self.assertEqual("L1", res.vertex_id)
        self.assertEqual(1, res.k_value)
        res = add_gate_to_sft(existing_sft,
                              "L1",
                              GateType.VOT,
                              2,
                              "0")
        self.assertIn(res, existing_sft.sft_attributes.gates)
        self.assertEqual(1,
                         len(existing_sft.sft_attributes.gates))
        self.assertEqual(GateType.VOT, res.gate_type)
        self.assertEqual("L1", res.vertex_id)
        self.assertEqual(2, res.k_value)
        res = add_gate_to_sft(existing_sft,
                              "TE2",
                              GateType.VOT,
                              4,
                              "1")
        self.assertEqual(res, existing_sft.sft_attributes.top_event)
        self.assertEqual(GateType.VOT,
                         existing_sft
                         .sft_attributes
                         .top_event
                         .gate_type)
        self.assertEqual(4, existing_sft.sft_attributes.top_event.k_value)
        self.assertEqual(2,
                         len(existing_sft.sft_attributes.gates))

    def test_parse_gate_type_and_k_value_from_gat_rsa_line(self):
        """Tests the parse_gate_type_and_k_value_from_gat_rsa_line method"""
        res1 = parse_gate_type_and_k_value_from_gat_rsa_line("3")
        res2 = parse_gate_type_and_k_value_from_gat_rsa_line("AND")
        res3 = parse_gate_type_and_k_value_from_gat_rsa_line("OR")
        res4 = parse_gate_type_and_k_value_from_gat_rsa_line("AA")
        self.assertEqual(GateType.VOT, res1[0])
        self.assertEqual(3, res1[1])
        self.assertEqual(GateType.AND, res2[0])
        self.assertEqual(0, res2[1])
        self.assertEqual(GateType.OR, res3[0])
        self.assertEqual(0, res3[1])
        self.assertEqual(GateType.AND, res4[0])
        self.assertEqual(0, res4[1])

    def test_parse_gate_label_from_gat_rsa_line(self):
        """Tests the parse_gate_label_from_gat_rsa_line method"""
        res1 = parse_gate_label_from_gat_rsa_line("ACP-2\"KD\"-PDK")
        res2 = parse_gate_label_from_gat_rsa_line("ACP-2\"-KD\"-PDK")
        self.assertEqual("KD", res1)
        self.assertEqual("KD", res2)

    def test_parse_input_label_from_gin_rsa_line(self):
        """Tests the parse_input_label_from_gin_rsa_line method"""
        res = parse_input_label_from_gin_rsa_line('	GIN	BEV	"EONETOP_TK1_FO'
                                                  '                                         '
                                                  '"	0	')
        self.assertEqual("EONETOP-TK1-FO", res)

    def test_parse_gate_input_from_gin_rsa_line(self):
        """Tests the parse_gate_input_from_gin_rsa_line method"""
        sft = SFT("ECC", sft_attributes=SFTAttributes(None, [], []))
        top_event = Gate("ECC-1", sft)
        sft.add_gate(top_event)
        sft.sft_attributes.top_event = top_event
        self.assertEqual(top_event, sft.sft_attributes.top_event)
        self.assertEqual("ECC-1", top_event.vertex_id)
        self.assertEqual(GateType.AND, top_event.gate_type)
        self.assertEqual(0, top_event.k_value)
        self.assertEqual(1, len(sft.sft_attributes.gates))
        self.assertIn(top_event, sft.sft_attributes.gates)
        self.assertNotIn(Gate("ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertNotIn(Edge(top_event, Gate("ECC-1-1", sft), "1"), sft.edges)
        parse_gate_input_from_gin_rsa_line(
            top_event,
            '	GIN	GAT	"@ECC-1-1                                       "	0',
            True)
        self.assertEqual(1, len(sft.sft_attributes.gates))
        self.assertNotIn(Gate("ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertNotIn(Edge(top_event, Gate("ECC-1-1", sft), "1"), sft.edges)
        parse_gate_input_from_gin_rsa_line(
            top_event,
            '	GIN	GAT	"@MFW-1-1                                       "	0',
            False)
        self.assertEqual(1, len(sft.sft_attributes.gates))
        self.assertNotIn(Gate("ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertNotIn(Edge(top_event, Gate("ECC-1-1", sft), "1"), sft.edges)
        parse_gate_input_from_gin_rsa_line(
            top_event,
            '	GIN	GAT	"@ECC-1-1                                       "	0',
            False)
        self.assertIn(Gate("@ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertIn(Edge(top_event, Gate("@ECC-1-1", sft), "0"), sft.edges)
        self.assertEqual(Edge(top_event, Gate("@ECC-1-1", sft), "0"), sft.find_edge("0"))
        self.assertEqual(1, len(sft.edges))
        self.assertEqual(2, len(sft.sft_attributes.gates))
        parse_gate_input_from_gin_rsa_line(
            top_event,
            '	GIN	BEV	"ECC-PM01-C                                       "	0',
            False)
        self.assertIn(BasicEvent("PM01", sft), sft.sft_attributes.basic_events)
        self.assertIn(Edge(top_event, BasicEvent("PM01", sft), "0"), sft.edges)
        self.assertEqual(Edge(top_event, BasicEvent("PM01", sft), "0"), sft.find_edge("0"))
        self.assertEqual(2, len(sft.edges))
        self.assertEqual(2, len(sft.sft_attributes.gates))
        self.assertEqual(1, len(sft.sft_attributes.basic_events))
        basic_event = BasicEvent("PM02", sft)
        sft.add_basic_event(basic_event)
        parse_gate_input_from_gin_rsa_line(
            top_event,
            '	GIN	BEV	"ECC-T1-PM02-C                                       "	0',
            False)

        self.assertIn(BasicEvent("PM02T1", sft), sft.sft_attributes.basic_events)
        self.assertIn(Edge(top_event, BasicEvent("PM02T1", sft), "1"), sft.edges)
        self.assertEqual(Edge(top_event, BasicEvent("PM02T1", sft), "1"), sft.find_edge("1"))
        self.assertEqual(3, len(sft.edges))
        self.assertEqual(2, len(sft.sft_attributes.gates))
        self.assertEqual(3, len(sft.sft_attributes.basic_events))

    def test_attempt_linking_gate_input_to_gate(self):
        """Tests the attempt_linking_gate_input_to_gate method"""
        sft = SFT("ECC", sft_attributes=SFTAttributes(None, [], []))
        top_event = Gate("ECC-1", sft)
        sft.add_gate(top_event)
        sft.sft_attributes.top_event = top_event
        self.assertEqual(top_event, sft.sft_attributes.top_event)
        self.assertEqual("ECC-1", top_event.vertex_id)
        self.assertEqual(GateType.AND, top_event.gate_type)
        self.assertEqual(0, top_event.k_value)
        self.assertEqual(1, len(sft.sft_attributes.gates))
        self.assertIn(top_event, sft.sft_attributes.gates)
        self.assertNotIn(Gate("ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertNotIn(Edge(top_event, Gate("ECC-1-1", sft), "1"), sft.edges)
        attempt_linking_gate_input_to_gate("ECC-1-1",
                                           top_event,
                                           sft,
                                           False)
        self.assertIn(Gate("ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertIn(Edge(top_event, Gate("ECC-1-1", sft), "0"), sft.edges)
        self.assertEqual(Edge(top_event, Gate("ECC-1-1", sft), "0"), sft.find_edge("0"))
        self.assertEqual(1, len(sft.edges))
        self.assertEqual(2, len(sft.sft_attributes.gates))
        sft.add_gate(Gate("ECC-1-3", sft))
        attempt_linking_gate_input_to_gate("ECC-1-3",
                                           top_event,
                                           sft,
                                           False)
        self.assertIn(Gate("ECC-1-3", sft), sft.sft_attributes.gates)
        self.assertIn(Edge(top_event, Gate("ECC-1-3", sft), "1"), sft.edges)
        self.assertEqual(Edge(top_event, Gate("ECC-1-3", sft), "1"), sft.find_edge("1"))
        self.assertEqual(2, len(sft.edges))
        self.assertEqual(3, len(sft.sft_attributes.gates))

    def test_link_gate_input_to_gate(self):
        """Tests the link_gate_input_to_gate method"""
        sft = SFT("ECC", sft_attributes=SFTAttributes(None, [], []))
        top_event = Gate("ECC-1", sft)
        sft.add_gate(top_event)
        sft.sft_attributes.top_event = top_event
        self.assertEqual(top_event, sft.sft_attributes.top_event)
        self.assertEqual("ECC-1", top_event.vertex_id)
        self.assertEqual(GateType.AND, top_event.gate_type)
        self.assertEqual(0, top_event.k_value)
        self.assertEqual(1, len(sft.sft_attributes.gates))
        self.assertIn(top_event, sft.sft_attributes.gates)
        self.assertNotIn(Gate("ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertNotIn(Edge(top_event, Gate("ECC-1-1", sft), "1"), sft.edges)
        extra_vertex_list: List[Vertex] = [Gate("ECC-1-1", sft)]
        sft.add_gate(Gate("ECC-1-1", sft))
        link_gate_input_to_gate(extra_vertex_list, top_event, sft)
        self.assertIn(Gate("ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertIn(Edge(top_event, Gate("ECC-1-1", sft), "0"), sft.edges)
        self.assertEqual(Edge(top_event, Gate("ECC-1-1", sft), "0"), sft.find_edge("0"))
        self.assertEqual(1, len(sft.edges))
        self.assertEqual(2, len(sft.sft_attributes.gates))
        link_gate_input_to_gate(extra_vertex_list, top_event, sft)
        self.assertIn(Gate("ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertIn(Edge(top_event, Gate("ECC-1-1", sft), "0"), sft.edges)
        self.assertEqual(Edge(top_event, Gate("ECC-1-1", sft), "0"), sft.find_edge("0"))
        self.assertEqual(1, len(sft.edges))
        self.assertEqual(2, len(sft.sft_attributes.gates))
        extra_gate_1 = Gate("ECC-1-3", sft)
        extra_gate_2 = Gate("ECC-1-5", sft)
        sft.add_gates([extra_gate_1, extra_gate_2])
        sft.add_edge(Edge(top_event, extra_gate_1, "3"))
        self.assertIn(Gate("ECC-1-3", sft), sft.sft_attributes.gates)
        self.assertIn(Gate("ECC-1-5", sft), sft.sft_attributes.gates)
        self.assertIn(Edge(top_event, Gate("ECC-1-3", sft), "3"), sft.edges)
        self.assertEqual(Edge(top_event, Gate("ECC-1-3", sft), "3"), sft.find_edge("3"))
        self.assertEqual(2, len(sft.edges))
        self.assertEqual(4, len(sft.sft_attributes.gates))
        link_gate_input_to_gate([extra_gate_2], extra_gate_1, sft)
        self.assertIn(Edge(top_event, Gate("ECC-1-5", sft), "4"), sft.edges)
        self.assertEqual(Edge(top_event, Gate("ECC-1-5", sft), "4"), sft.find_edge("4"))
        self.assertEqual(3, len(sft.edges))

    def test_create_new_gate_and_link_to_existing_gate(self):
        """Tests the create_new_gate_and_link_to_existing_gate method"""
        sft = SFT("ECC", sft_attributes=SFTAttributes(None, [], []))
        top_event = Gate("ECC-1", sft)
        sft.add_gate(top_event)
        sft.sft_attributes.top_event = top_event
        self.assertEqual(top_event, sft.sft_attributes.top_event)
        self.assertEqual("ECC-1", top_event.vertex_id)
        self.assertEqual(GateType.AND, top_event.gate_type)
        self.assertEqual(0, top_event.k_value)
        self.assertEqual(1, len(sft.sft_attributes.gates))
        self.assertIn(top_event, sft.sft_attributes.gates)
        self.assertNotIn(Gate("ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertNotIn(Edge(top_event, Gate("ECC-1-1", sft), "1"), sft.edges)
        create_new_gate_and_link_to_existing_gate("ECC-1-1",
                                                  top_event,
                                                  sft)
        self.assertIn(Gate("ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertIn(Edge(top_event, Gate("ECC-1-1", sft), "0"), sft.edges)
        self.assertEqual(Edge(top_event, Gate("ECC-1-1", sft), "0"), sft.find_edge("0"))
        self.assertEqual(1, len(sft.edges))
        self.assertEqual(2, len(sft.sft_attributes.gates))
        create_new_gate_and_link_to_existing_gate("ECC-1-2",
                                                  top_event,
                                                  sft)
        self.assertIn(Gate("ECC-1-2", sft), sft.sft_attributes.gates)
        self.assertIn(Edge(top_event, Gate("ECC-1-2", sft), "1"), sft.edges)
        self.assertEqual(Edge(top_event, Gate("ECC-1-2", sft), "1"), sft.find_edge("1"))
        self.assertEqual(2, len(sft.edges))
        self.assertEqual(3, len(sft.sft_attributes.gates))

    def test_create_new_be_and_link_to_existing_gate(self):
        """Tests the test_create_new_be_and_link_to_existing_gate method"""
        sft = SFT("ECC", sft_attributes=SFTAttributes(None, [], []))
        top_event = Gate("ECC-1", sft)
        sft.add_gate(top_event)
        sft.sft_attributes.top_event = top_event
        self.assertEqual(top_event, sft.sft_attributes.top_event)
        self.assertEqual("ECC-1", top_event.vertex_id)
        self.assertEqual(GateType.AND, top_event.gate_type)
        self.assertEqual(0, top_event.k_value)
        self.assertEqual(1, len(sft.sft_attributes.gates))
        self.assertIn(top_event, sft.sft_attributes.gates)
        self.assertNotIn(Gate("ECC-1-1", sft), sft.sft_attributes.gates)
        self.assertNotIn(Edge(top_event, Gate("ECC-1-1", sft), "1"), sft.edges)
        create_new_be_and_link_to_existing_gate("ECC-1-1",
                                                top_event,
                                                sft)
        self.assertIn(BasicEvent("ECC-1-1", sft), sft.sft_attributes.basic_events)
        self.assertIn(Edge(top_event, BasicEvent("ECC-1-1", sft), "0"), sft.edges)
        self.assertEqual(Edge(top_event, BasicEvent("ECC-1-1", sft), "0"), sft.find_edge("0"))
        self.assertEqual(1, len(sft.edges))
        self.assertEqual(1, len(sft.sft_attributes.gates))
        self.assertEqual(1, len(sft.sft_attributes.basic_events))
        create_new_be_and_link_to_existing_gate("ECC-1-2",
                                                top_event,
                                                sft)
        self.assertIn(BasicEvent("ECC-1-2", sft), sft.sft_attributes.basic_events)
        self.assertIn(Edge(top_event, BasicEvent("ECC-1-2", sft), "1"), sft.edges)
        self.assertEqual(Edge(top_event, BasicEvent("ECC-1-2", sft), "1"), sft.find_edge("1"))
        self.assertEqual(2, len(sft.edges))
        self.assertEqual(2, len(sft.sft_attributes.basic_events))

    def test_parse_sfts_from_lines(self):
        """Tests the parse_sfts_from_lines method."""
        lines = ['//RECORD Fault Tree',
                 'FTR	"EONETOP-1                                          '
                 '"	"Insufficient Pumping"	',
                 'BEV	"EONETOP_CV1_FO'
                 '                                  '
                 '"	1	2	0	"Check Valve fails to open"	',

                 'BEV	"ACP_CV1_FO'
                 '                                  '
                 '"	1	2	0	"Check Valve fails to open"	',
                 'BEV	"EONETOP_TK1_FO'
                 '                                         '
                 '"	1	4	0	"Failure in operation of tank tank_1"',
                 'BEV	"EONETOP_MDP1_FO'
                 '                                          '
                 '"	1	4	0	"Failure in operation of pump MDP_1"',
                 'GAT	"@EONETOP_1                                          '
                 '"	OR	0	"EONETOP_1                                          '
                 '"	1	"Insufficient Pumping"	',
                 '	GIN	BEV	"EONETOP_CV1_FO'
                 '                                  '
                 '"	0	',
                 '	GIN	BEV	"EONETOP_TK1_FO'
                 '                                         '
                 '"	0	',
                 '	GIN	BEV	"EONETOP_MDP1_FO'
                 '                                          '
                 '"	0	']
        res: List[SFT] = []
        parse_sfts_from_lines(lines, res)
        sft: SFT = res.pop()
        self.assertEqual(4, len(sft.vertices))
        self.assertEqual(1, len(sft.sft_attributes.gates))
        self.assertEqual(3, len(sft.sft_attributes.basic_events))
        self.assertEqual(3, len(sft.edges))
        self.assertEqual('[Edge(label=0, head=CV1, tail=@EONETOP-1), '
                         'Edge(label=1, head=TK1, tail=@EONETOP-1), '
                         'Edge(label=2, head=MDP1, tail=@EONETOP-1)]',
                         str(sft.edges))
        lines = ['FTR	"DPS                                               '
                 '"	"Depressurisation System"	',
                 'BEV	"DPS-MAN--H                                        '
                 '"	1	3	0	'
                 '"Operators fail to depressurise manually"',
                 'BEV	"DPS-VS01-A                                        '
                 '"	1	3	0	'
                 '"Pressure relief valve fails to open"',
                 'BEV	"DPS-VS02-A                                        '
                 '"	1	3	0	'
                 '"Pressure relief valve fails to open"',
                 'BEV	"DPS-VS03-A                                        '
                 '"	1	3	0	'
                 '"Pressure relief valve fails to open"',
                 'BEV	"DPS-VS04-A                                        '
                 '"	1	3	0	'
                 '"Pressure relief valve fails to open"',
                 'BEV	"DPS-VS05-A                                        '
                 '"	1	3	0	'
                 '"Pressure relief valve fails to open"',
                 'BEV	"DPS-VS06-A                                        '
                 '"	1	3	0	'
                 '"Pressure relief valve fails to open"',
                 'GAT	"@DPS-1                                            '
                 '"	OR	0	"DPS                                               '
                 '"	1	"Depressurisation"',
                 'GIN	GAT	"@DPS-2                                            '
                 '"	0',
                 'GIN	BEV	"DPS-MAN--H                                        '
                 '"	0',
                 'GAT	"@DPS-2                                            '
                 '"	2	0	"DPS                                               '
                 '"	0	"Independent failures of 2 out of 6 valves"',
                 'GIN	BEV	"DPS-VS01-A                                        '
                 '"	0',
                 'GIN	BEV	"DPS-VS02-A                                        '
                 '"	0',
                 'GIN	BEV	"DPS-VS03-A                                        '
                 '"	0',
                 'GIN	BEV	"DPS-VS04-A                                        '
                 '"	0',
                 'GIN	BEV	"DPS-VS05-A                                        '
                 '"	0',
                 'GIN	BEV	"DPS-VS06-A                                        '
                 '"	0	',
                 'GAT	"@ACP-2                                            '
                 '"	2	0	"DPS                                               '
                 '"	0	"Independent failures of 2 out of 6 valves"',
                 'GIN	BEV	"DPS-VS01-A                                        '
                 '"	0']
        res: List[SFT] = []
        parse_sfts_from_lines(lines, res)
        sft: SFT = res.pop()
        self.assertEqual(9, len(sft.vertices))
        self.assertEqual(2, len(sft.sft_attributes.gates))
        self.assertEqual(7, len(sft.sft_attributes.basic_events))
        self.assertEqual(8, len(sft.edges))
        self.assertEqual('[Edge(label=0, head=@DPS-2, tail=@DPS-1), '
                         'Edge(label=1, head=MAN, tail=@DPS-1), '
                         'Edge(label=2, head=VS01, tail=@DPS-2), '
                         'Edge(label=3, head=VS02, tail=@DPS-2), '
                         'Edge(label=4, head=VS03, tail=@DPS-2), '
                         'Edge(label=5, head=VS04, tail=@DPS-2), '
                         'Edge(label=6, head=VS05, tail=@DPS-2), '
                         'Edge(label=7, head=VS06, tail=@DPS-2)]', str(sft.edges))
        self.assertEqual(2, sft.find_gate("@DPS-2").k_value)


if __name__ == '__main__':
    unittest.main()

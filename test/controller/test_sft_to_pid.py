"""
This file contains a class for testing sft_to_pid.py
"""
import unittest

from src.controller.sft_to_pid import SFTToPIDConverter
from src.model.graph import Edge
from src.model.pid import Component, ComponentTypeEnum, PID
from src.model.sft import SFT, BasicEvent, SFTAttributes, Gate, GateType, FaultCode


def setup_simple_sft_bes_gate_edges(sft: SFT = None) -> None:
    """
    Helper function to set this test file up with SFTs.
    """
    top_event = Gate("GATE1", sft)
    sft.add_gate(top_event)
    sft.sft_attributes.top_event = top_event
    basic_events = [BasicEvent("HX", sft),
                    BasicEvent("VC", sft),
                    BasicEvent("VM", sft),
                    BasicEvent("VH", sft),
                    BasicEvent("VI", sft),
                    BasicEvent("VS", sft),
                    BasicEvent("PM", sft),
                    BasicEvent("TK", sft),
                    BasicEvent("GT", sft),
                    BasicEvent("AA", sft),
                    BasicEvent("AB", sft, FaultCode.M)]
    sft.add_basic_events(basic_events)
    sft.add_edges([Edge(top_event, basic_events[0], "1"),
                   Edge(top_event, basic_events[1], "2"),
                   Edge(top_event, basic_events[2], "3"),
                   Edge(top_event, basic_events[3], "4"),
                   Edge(top_event, basic_events[4], "5"),
                   Edge(top_event, basic_events[5], "6"),
                   Edge(top_event, basic_events[6], "7"),
                   Edge(top_event, basic_events[7], "8"),
                   Edge(top_event, basic_events[8], "9"),
                   Edge(top_event, basic_events[9], "10"),
                   Edge(top_event, basic_events[10], "11")])


class SFTToPIDTest(unittest.TestCase):
    """Class for testing sft_to_pid.py"""

    def setUp(self) -> None:
        """Sets up the test cases for sft_to_pid.py"""
        self.sft1: SFT = SFT("A-1")
        self.sft2: SFT = SFT("A-2")
        self.sft3: SFT = SFT("B-1")
        setup_simple_sft_bes_gate_edges(self.sft1)
        setup_simple_sft_bes_gate_edges(self.sft2)
        self.sft2.delete_basic_event(BasicEvent("AA", self.sft2))
        self.sft2.del_edge(Edge(self.sft2.sft_attributes.top_event,
                                BasicEvent("AA", self.sft2),
                                "10"))
        setup_simple_sft_bes_gate_edges(self.sft3)
        self.sfts: [SFT] = [self.sft1,
                            self.sft2,
                            self.sft3]
        self.sft_to_pid_converter = SFTToPIDConverter(self.sfts)
        self.sft_e1 = SFT("ACP-1", True)
        self.sft_e1.sft_attributes.top_event = BasicEvent("ACP-1",
                                                          self.sft_e1)
        self.gate_e1 = Gate("G1", self.sft_e1)
        basic_events_e1 = [BasicEvent("ACP-DG01-A", self.sft_e1),
                           BasicEvent("ACP-DG02-A", self.sft_e1),
                           BasicEvent("ACP-DG03-A", self.sft_e1)]
        edges_e1 = [Edge(self.sft_e1.sft_attributes.top_event,
                         self.gate_e1,
                         "1"),
                    Edge(self.gate_e1, basic_events_e1[0], "2"),
                    Edge(self.gate_e1, basic_events_e1[1], "3"),
                    Edge(self.gate_e1, basic_events_e1[2], "4")]
        self.sft_e1.sft_attributes = SFTAttributes(Gate("ACP-1",
                                                        self.sft_e1,
                                                        GateType.OR),
                                                   [self.gate_e1],
                                                   basic_events_e1)
        self.sft_e1.add_edges(edges_e1)

    def test_pids(self) -> None:
        """Tests the pids property of the SFTToPIDConverter"""
        self.assertNotIn(self.sft_to_pid_converter
                         .process_sft_to_fresh_pid(self.sft1),
                         self.sft_to_pid_converter.pids)
        self.assertEqual(0, len(self.sft_to_pid_converter.pids))

    def test_sfts(self) -> None:
        """Tests the pids property of the SFTToPIDConverter"""
        self.assertEqual(self.sfts, self.sft_to_pid_converter.sfts)

    def test_node_counter(self) -> None:
        """Tests the node_counter property of the SFTToPIDConverter"""
        self.assertEqual(1, self.sft_to_pid_converter.node_counter)
        self.sft_to_pid_converter.node_counter = 3
        self.assertEqual(3, self.sft_to_pid_converter.node_counter)

    def test_infer_pids_from_sfts(self):
        """Tests the infer_pids_from_sfts method of SFTToPIDConverter"""
        pids: {PID} = self.sft_to_pid_converter.infer_pids_from_sfts()
        self.assertEqual(2, len(pids))
        pid_a_set: {PID} = {p for p in pids if "A" in p.graph_id}
        pid_b_set: {PID} = {p for p in pids if "B" in p.graph_id}
        self.assertEqual(1, len(pid_a_set))
        self.assertEqual(1, len(pid_b_set))
        pid_a: PID = pid_a_set.pop()
        pid_b: PID = pid_b_set.pop()
        self.assertEqual(15, len(pid_a.vertices))
        self.assertEqual(13, len(pid_b.vertices))

    def test_process_sft_to_fresh_pid(self) -> None:
        """Tests the process_sft_to_fresh_pi() of sft_to_pid.py"""
        pid1 = self.sft_to_pid_converter.process_sft_to_fresh_pid(self.sft1)
        self.assertFalse(pid1.directed)
        self.assertEqual("A", pid1.graph_id)
        self.assertEqual(13, len(pid1.vertices))
        self.assertIn(Component("GT", pid1, {}), pid1)
        self.assertIn(Component("HX", pid1, {}), pid1)
        self.assertIn(Component("VC", pid1, {}), pid1)
        self.assertIn(Component("VM", pid1, {}), pid1)
        self.assertIn(Component("VH", pid1, {}), pid1)
        self.assertIn(Component("VI", pid1, {}), pid1)
        self.assertIn(Component("VS", pid1, {}), pid1)
        self.assertIn(Component("PM", pid1, {}), pid1)
        self.assertIn(Component("TK", pid1, {}), pid1)
        self.assertIn(Component("AA", pid1, {}), pid1)
        self.assertIn(Component("AB", pid1, {}), pid1)
        self.assertEqual(ComponentTypeEnum.TURBINE, pid1.find_component(
            "GT").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.HEAT_EXCHANGER, pid1.find_component(
            "HX").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.CHECK_VALVE, pid1.find_component(
            "VC").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.EXPSA_MO_VALVE, pid1.find_component(
            "VM").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.MANUAL_VALVE, pid1.find_component(
            "VH").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.MFW_ISOLATION_VALVE,
                         pid1
                         .find_component("VI")
                         .component_type
                         .component_type_enum)
        self.assertEqual(ComponentTypeEnum.DPS_PRESSURE_RELIEF_VALVE,
                         pid1.find_component("VS").component_type
                         .component_type_enum)
        self.assertEqual(ComponentTypeEnum.MOTOR_DRIVEN_PUMP,
                         pid1.find_component("PM").
                         component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.TANK, pid1.find_component(
            "TK").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.NODE, pid1.find_component(
            "AA").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.MAINTENANCE_GROUP, pid1.find_component(
            "AB").component_type.component_type_enum)

    def test_insert_pid_components_based_on_sft(self):
        """Tests the insert_pid_components_based_on_sft method of SFTToPIDConverter"""
        pid1 = PID(pid_id="A")
        self.sft_to_pid_converter.insert_pid_components_based_on_sft(
            pid1,
            self.sft_to_pid_converter.sfts[0])
        self.assertFalse(pid1.directed)
        self.assertEqual("A", pid1.graph_id)
        self.assertEqual(11, len(pid1.vertices))
        self.assertIn(Component("GT", pid1, {}), pid1)
        self.assertIn(Component("HX", pid1, {}), pid1)
        self.assertIn(Component("VC", pid1, {}), pid1)
        self.assertIn(Component("VM", pid1, {}), pid1)
        self.assertIn(Component("VH", pid1, {}), pid1)
        self.assertIn(Component("VI", pid1, {}), pid1)
        self.assertIn(Component("VS", pid1, {}), pid1)
        self.assertIn(Component("PM", pid1, {}), pid1)
        self.assertIn(Component("TK", pid1, {}), pid1)
        self.assertIn(Component("AA", pid1, {}), pid1)
        self.assertIn(Component("AB", pid1, {}), pid1)
        self.assertEqual(ComponentTypeEnum.TURBINE, pid1.find_component(
            "GT").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.HEAT_EXCHANGER, pid1.find_component(
            "HX").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.CHECK_VALVE, pid1.find_component(
            "VC").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.EXPSA_MO_VALVE, pid1.find_component(
            "VM").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.MANUAL_VALVE, pid1.find_component(
            "VH").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.MFW_ISOLATION_VALVE,
                         pid1
                         .find_component("VI")
                         .component_type
                         .component_type_enum)
        self.assertEqual(ComponentTypeEnum.DPS_PRESSURE_RELIEF_VALVE,
                         pid1.find_component("VS").component_type
                         .component_type_enum.DPS_PRESSURE_RELIEF_VALVE)
        self.assertEqual(ComponentTypeEnum.MOTOR_DRIVEN_PUMP,
                         pid1.find_component("PM").
                         component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.TANK, pid1.find_component(
            "TK").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.NODE, pid1.find_component(
            "AA").component_type.component_type_enum)
        self.assertEqual(ComponentTypeEnum.MAINTENANCE_GROUP, pid1.find_component(
            "AB").component_type.component_type_enum)

    def test_delete_sft_maintenance_groups_to_prevent_linking(self):
        """Tests the delete_sft_maintenance_groups_to_prevent_linking method of SFTToPIDConverter"""
        special_basic_event = self.sft_to_pid_converter.sfts[0].find_basic_event("AB")
        self.assertIn(special_basic_event,
                      self.sft_to_pid_converter.sfts[0].sft_attributes.basic_events)
        self.sft_to_pid_converter.delete_sft_maintenance_groups_to_prevent_linking(
            self.sft_to_pid_converter.sfts[0])
        self.assertNotIn(special_basic_event,
                         self.sft_to_pid_converter.sfts[0].sft_attributes.basic_events)
        self.sft_to_pid_converter.delete_sft_maintenance_groups_to_prevent_linking(
            self.sft_to_pid_converter.sfts[0])
        self.assertNotIn(special_basic_event,
                         self.sft_to_pid_converter.sfts[0].sft_attributes.basic_events)

    def test_insert_pid_pipes_based_on_sft(self):
        """Tests the insert_pid_pipes_based_on_sft method of SFTToPIDConverter"""
        pid1 = PID(pid_id="A")
        self.sft_to_pid_converter.insert_pid_components_based_on_sft(
            pid1,
            self.sft_to_pid_converter.sfts[0])
        self.sft_to_pid_converter \
            .delete_sft_maintenance_groups_to_prevent_linking(
                self.sft_to_pid_converter.sfts[0])
        self.assertEqual(0, len(pid1.edges))
        self.assertEqual(11, len(pid1.vertices))
        self.sft_to_pid_converter.insert_pid_links_based_on_sft(
            pid1,
            self.sft_to_pid_converter.sfts[0])
        self.assertEqual(20, len(pid1.edges))
        self.assertEqual(20, len({e.edge_id for e in pid1.edges}))

    def test_extra_(self):
        """Tests example 1 of the examples in doc/test_examples"""
        self.sft_e1.del_edge(self.sft_e1.find_edge("3"))
        self.sft_e1.del_edge(self.sft_e1.find_edge("4"))
        self.sft_e1.delete_basic_event(self.sft_e1.find_basic_event("ACP-DG02-A"))
        self.sft_e1.delete_basic_event(self.sft_e1.find_basic_event("ACP-DG03-A"))
        gate = Gate("G2", self.sft_e1)
        self.sft_e1.add_gate(gate)
        self.sft_e1.add_edge(Edge(self.sft_e1.find_basic_event("ACP-DG01-A"), gate, "3"))
        self.sft_to_pid_converter = SFTToPIDConverter([self.sft_e1])
        inputs, outputs, pipes = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(self.gate_e1)
        self.sft_to_pid_converter.node_counter = 1
        self.assertEqual("ACP-DG01-A", inputs)
        self.assertEqual("ACP-DG01-A", outputs)
        self.assertEqual(0, len(pipes))

    def test_example1(self):
        """Tests example 1 of the examples in doc/test_examples"""
        self.sft_to_pid_converter = SFTToPIDConverter([self.sft_e1])
        inputs, outputs, pipes = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(self.gate_e1)
        self.sft_to_pid_converter.node_counter = 1
        self.assertEqual("ACP-NODE1", inputs)
        self.assertEqual("ACP-NODE2", outputs)
        self.assertEqual(6, len(pipes))
        self.assertEqual({("ACP-NODE1", "ACP-DG01-A"),
                          ("ACP-DG01-A", "ACP-NODE2"),
                          ("ACP-NODE1", "ACP-DG02-A"),
                          ("ACP-DG02-A", "ACP-NODE2"),
                          ("ACP-NODE1", "ACP-DG03-A"),
                          ("ACP-DG03-A", "ACP-NODE2")}, pipes)

    def test_example2(self):
        """Tests example 2 of the examples in doc/test_examples"""
        self.gate_e1.gate_type = GateType.OR
        self.sft_to_pid_converter = SFTToPIDConverter([self.sft_e1])
        inputs, outputs, pipes = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(self.gate_e1)
        self.sft_to_pid_converter.node_counter = 1
        self.assertEqual("ACP-DG01-A", inputs)
        self.assertEqual("ACP-DG03-A", outputs)
        self.assertEqual(2, len(pipes))
        self.assertEqual({("ACP-DG01-A", "ACP-DG02-A"),
                          ("ACP-DG02-A", "ACP-DG03-A")}, pipes)

    def test_example3(self):
        """Tests example 3 of the examples in doc/test_examples"""
        self.sft_e1 = SFT("ACP-3")
        self.gate_e1 = Gate("G1", self.sft_e1)
        gate_e2 = Gate("G2", self.sft_e1, GateType.OR)
        basic_events_e1 = [BasicEvent("ACP-DG01-A", self.sft_e1),
                           BasicEvent("ACP-DG02-A", self.sft_e1),
                           BasicEvent("ACP-DG03-A", self.sft_e1),
                           BasicEvent("ACP-DG04-A", self.sft_e1),
                           BasicEvent("ACP-DG05-A", self.sft_e1)]
        self.sft_e1.sft_attributes = SFTAttributes(Gate("ACP-1",
                                                        self.sft_e1,
                                                        GateType.OR),
                                                   [self.gate_e1,
                                                    gate_e2],
                                                   basic_events_e1)
        edges_e1 = [Edge(self.sft_e1.sft_attributes.top_event,
                         self.gate_e1,
                         "1"),
                    Edge(self.gate_e1, basic_events_e1[0], "2"),
                    Edge(self.gate_e1, gate_e2, "3"),
                    Edge(self.gate_e1, basic_events_e1[4], "4"),
                    Edge(gate_e2, basic_events_e1[1], "5"),
                    Edge(gate_e2, basic_events_e1[2], "6"),
                    Edge(gate_e2, basic_events_e1[3], "7")]
        self.sft_e1.add_edges(edges_e1)
        self.sft_to_pid_converter = SFTToPIDConverter([self.sft_e1])
        inputs1, outputs1, pipes1 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(self.gate_e1)
        self.sft_to_pid_converter.node_counter = 1
        inputs2, outputs2, pipes2 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(gate_e2)
        self.sft_to_pid_converter.node_counter = 1
        self.assertEqual("ACP-DG02-A", inputs2)
        self.assertEqual("ACP-DG04-A", outputs2)
        self.assertEqual(2, len(pipes2))
        self.assertEqual({("ACP-DG02-A", "ACP-DG03-A"),
                          ("ACP-DG03-A", "ACP-DG04-A")}, pipes2)
        self.assertEqual("ACP-NODE1", inputs1)
        self.assertEqual("ACP-NODE2", outputs1)
        self.assertEqual(8, len(pipes1))
        self.assertEqual({("ACP-NODE1", "ACP-DG01-A"),
                          ("ACP-DG01-A", "ACP-NODE2"),
                          ("ACP-NODE1", "ACP-DG02-A"),
                          ("ACP-DG02-A", "ACP-DG03-A"),
                          ("ACP-DG03-A", "ACP-DG04-A"),
                          ("ACP-DG04-A", "ACP-NODE2"),
                          ("ACP-NODE1", "ACP-DG05-A"),
                          ("ACP-DG05-A", "ACP-NODE2")}, pipes1)

    def test_example4(self):
        """Tests example 4 of the examples in doc/test_examples"""
        self.sft_e1 = SFT("ACP-3")
        self.gate_e1 = Gate("G1", self.sft_e1, GateType.OR)
        gate_e2 = Gate("G2", self.sft_e1)
        basic_events_e1 = [BasicEvent("ACP-DG01-A", self.sft_e1),
                           BasicEvent("ACP-DG02-A", self.sft_e1),
                           BasicEvent("ACP-DG03-A", self.sft_e1),
                           BasicEvent("ACP-DG04-A", self.sft_e1),
                           BasicEvent("ACP-DG05-A", self.sft_e1)]
        self.sft_e1.sft_attributes = SFTAttributes(Gate("ACP-1",
                                                        self.sft_e1,
                                                        GateType.OR),
                                                   [self.gate_e1,
                                                    gate_e2],
                                                   basic_events_e1)
        edges_e1 = [Edge(self.sft_e1.sft_attributes.top_event,
                         self.gate_e1,
                         "1"),
                    Edge(self.gate_e1, basic_events_e1[0], "2"),
                    Edge(self.gate_e1, gate_e2, "3"),
                    Edge(self.gate_e1, basic_events_e1[4], "4"),
                    Edge(gate_e2, basic_events_e1[1], "5"),
                    Edge(gate_e2, basic_events_e1[2], "6"),
                    Edge(gate_e2, basic_events_e1[3], "7")]
        self.sft_e1.add_edges(edges_e1)
        self.sft_to_pid_converter = SFTToPIDConverter([self.sft_e1])
        inputs1, outputs1, pipes1 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(self.gate_e1)
        self.sft_to_pid_converter.node_counter = 1
        inputs2, outputs2, pipes2 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(gate_e2)
        self.sft_to_pid_converter.node_counter = 1
        self.assertEqual("ACP-NODE1", inputs2)
        self.assertEqual("ACP-NODE2", outputs2)
        self.assertEqual(6, len(pipes2))
        self.assertEqual({("ACP-NODE1", "ACP-DG02-A"),
                          ("ACP-DG02-A", "ACP-NODE2"),
                          ("ACP-NODE1", "ACP-DG03-A"),
                          ("ACP-DG03-A", "ACP-NODE2"),
                          ("ACP-NODE1", "ACP-DG04-A"),
                          ("ACP-DG04-A", "ACP-NODE2")}, pipes2)
        self.assertEqual("ACP-DG01-A", inputs1)
        self.assertEqual("ACP-DG05-A", outputs1)
        self.assertEqual(8, len(pipes1))
        self.assertEqual({('ACP-DG01-A', 'ACP-NODE1'),
                          ('ACP-NODE1', 'ACP-DG02-A'),
                          ('ACP-DG02-A', 'ACP-NODE2'),
                          ('ACP-NODE1', 'ACP-DG03-A'),
                          ('ACP-DG03-A', 'ACP-NODE2'),
                          ('ACP-NODE1', 'ACP-DG04-A'),
                          ('ACP-DG04-A', 'ACP-NODE2'),
                          ('ACP-NODE2', 'ACP-DG05-A')}, pipes1)

    def test_example5(self):
        """Tests example 5 of the examples in doc/test_examples"""
        self.sft_e1 = SFT("ACP-3")
        self.gate_e1 = Gate("G1", self.sft_e1)
        gate_e2 = Gate("G2", self.sft_e1)
        basic_events_e1 = [BasicEvent("ACP-DG01-A", self.sft_e1),
                           BasicEvent("ACP-DG02-A", self.sft_e1),
                           BasicEvent("ACP-DG03-A", self.sft_e1),
                           BasicEvent("ACP-DG04-A", self.sft_e1),
                           BasicEvent("ACP-DG05-A", self.sft_e1)]
        self.sft_e1.sft_attributes = SFTAttributes(Gate("ACP-1",
                                                        self.sft_e1,
                                                        GateType.OR),
                                                   [self.gate_e1,
                                                    gate_e2],
                                                   basic_events_e1)
        edges_e1 = [Edge(self.sft_e1.sft_attributes.top_event,
                         self.gate_e1,
                         "1"),
                    Edge(self.gate_e1, basic_events_e1[0], "2"),
                    Edge(self.gate_e1, gate_e2, "3"),
                    Edge(self.gate_e1, basic_events_e1[4], "4"),
                    Edge(gate_e2, basic_events_e1[1], "5"),
                    Edge(gate_e2, basic_events_e1[2], "6"),
                    Edge(gate_e2, basic_events_e1[3], "7")]
        self.sft_e1.add_edges(edges_e1)
        self.sft_to_pid_converter = SFTToPIDConverter([self.sft_e1])
        inputs1, outputs1, pipes1 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(self.gate_e1)
        self.sft_to_pid_converter.node_counter = 1
        inputs2, outputs2, pipes2 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(gate_e2)
        self.sft_to_pid_converter.node_counter = 1
        self.assertEqual("ACP-NODE1", inputs2)
        self.assertEqual("ACP-NODE2", outputs2)
        self.assertEqual(6, len(pipes2))
        self.assertEqual({("ACP-NODE1", "ACP-DG02-A"),
                          ("ACP-DG02-A", "ACP-NODE2"),
                          ("ACP-NODE1", "ACP-DG03-A"),
                          ("ACP-DG03-A", "ACP-NODE2"),
                          ("ACP-NODE1", "ACP-DG04-A"),
                          ("ACP-DG04-A", "ACP-NODE2")}, pipes2)
        self.assertEqual("ACP-NODE1", inputs1)
        self.assertEqual("ACP-NODE2", outputs1)
        self.assertEqual(12, len(pipes1))
        self.assertEqual({('ACP-NODE1', 'ACP-DG01-A'),
                          ('ACP-DG01-A', 'ACP-NODE2'),
                          ('ACP-NODE1', 'ACP-NODE3'),
                          ('ACP-NODE3', 'ACP-DG02-A'),
                          ('ACP-DG02-A', 'ACP-NODE4'),
                          ('ACP-NODE3', 'ACP-DG03-A'),
                          ('ACP-DG03-A', 'ACP-NODE4'),
                          ('ACP-NODE3', 'ACP-DG04-A'),
                          ('ACP-DG04-A', 'ACP-NODE4'),
                          ('ACP-NODE4', 'ACP-NODE2'),
                          ('ACP-NODE1', 'ACP-DG05-A'),
                          ('ACP-DG05-A', 'ACP-NODE2')}, pipes1)

    def test_example6(self):
        """Tests example 6 of the examples in doc/test_examples"""
        self.sft_e1 = SFT("ACP-3")
        self.gate_e1 = Gate("G1", self.sft_e1, GateType.OR)
        gate_e2 = Gate("G2", self.sft_e1, GateType.OR)
        basic_events_e1 = [BasicEvent("ACP-DG01-A", self.sft_e1),
                           BasicEvent("ACP-DG02-A", self.sft_e1),
                           BasicEvent("ACP-DG03-A", self.sft_e1),
                           BasicEvent("ACP-DG04-A", self.sft_e1),
                           BasicEvent("ACP-DG05-A", self.sft_e1)]
        self.sft_e1.sft_attributes = SFTAttributes(Gate("ACP-1",
                                                        self.sft_e1,
                                                        GateType.OR),
                                                   [self.gate_e1,
                                                    gate_e2],
                                                   basic_events_e1)
        edges_e1 = [Edge(self.sft_e1.sft_attributes.top_event,
                         self.gate_e1,
                         "1"),
                    Edge(self.gate_e1, basic_events_e1[0], "2"),
                    Edge(self.gate_e1, gate_e2, "3"),
                    Edge(self.gate_e1, basic_events_e1[4], "4"),
                    Edge(gate_e2, basic_events_e1[1], "5"),
                    Edge(gate_e2, basic_events_e1[2], "6"),
                    Edge(gate_e2, basic_events_e1[3], "7")]
        self.sft_e1.add_edges(edges_e1)
        self.sft_to_pid_converter = SFTToPIDConverter([self.sft_e1])
        inputs1, outputs1, pipes1 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(self.gate_e1)
        self.sft_to_pid_converter.node_counter = 1
        inputs2, outputs2, pipes2 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(gate_e2)
        self.sft_to_pid_converter.node_counter = 1
        self.assertEqual("ACP-DG02-A", inputs2)
        self.assertEqual("ACP-DG04-A", outputs2)
        self.assertEqual(2, len(pipes2))
        self.assertEqual({("ACP-DG02-A", "ACP-DG03-A"),
                          ("ACP-DG03-A", "ACP-DG04-A")}, pipes2)
        self.assertEqual("ACP-DG01-A", inputs1)
        self.assertEqual("ACP-DG05-A", outputs1)
        self.assertEqual(4, len(pipes1))
        self.assertEqual({("ACP-DG01-A", "ACP-DG02-A"),
                          ("ACP-DG02-A", "ACP-DG03-A"),
                          ("ACP-DG03-A", "ACP-DG04-A"),
                          ("ACP-DG04-A", "ACP-DG05-A")}, pipes1)

    def test_example9(self):
        """Tests example 3 of the examples in doc/test_examples"""
        self.sft_e1 = SFT("ACP-3")
        self.gate_e1 = Gate("G1", self.sft_e1, GateType.OR)
        extra_events = [Gate("G2", self.sft_e1),
                        Gate("G3", self.sft_e1, GateType.OR),
                        Gate("G4", self.sft_e1)]
        basic_events_e1 = [BasicEvent("ACP-DG01-A-A1", self.sft_e1),
                           BasicEvent("ACP-DG02-A-A3", self.sft_e1),
                           BasicEvent("ACP-DG03-A-B1", self.sft_e1),
                           BasicEvent("ACP-DG04-A-B3", self.sft_e1),
                           BasicEvent("ACP-DG05-A-C1", self.sft_e1),
                           BasicEvent("ACP-DG06-A-C3", self.sft_e1),
                           BasicEvent("ACP-DG07-A-D1", self.sft_e1),
                           BasicEvent("ACP-DG08-A-D2", self.sft_e1),
                           BasicEvent("ACP-DG09-A-D3", self.sft_e1)]
        self.sft_e1.sft_attributes = SFTAttributes(Gate("ACP-1",
                                                        self.sft_e1,
                                                        GateType.OR),
                                                   [],
                                                   [])
        self.sft_e1.add_basic_events(basic_events_e1)
        self.sft_e1.add_gates([self.gate_e1,
                               extra_events[0],
                               extra_events[1],
                               extra_events[2]])
        self.sft_e1.add_edges([Edge(self.sft_e1.sft_attributes.top_event,
                                    self.gate_e1,
                                    "1"),
                               Edge(self.gate_e1, basic_events_e1[0], "2"),
                               Edge(self.gate_e1, extra_events[0], "3"),
                               Edge(self.gate_e1, basic_events_e1[1], "4"),
                               Edge(extra_events[0], basic_events_e1[2], "5"),
                               Edge(extra_events[0], extra_events[1], "6"),
                               Edge(extra_events[0], basic_events_e1[3], "7"),
                               Edge(extra_events[1], basic_events_e1[4], "8"),
                               Edge(extra_events[1], extra_events[2], "9"),
                               Edge(extra_events[1], basic_events_e1[5], "10"),
                               Edge(extra_events[2], basic_events_e1[6], "11"),
                               Edge(extra_events[2], basic_events_e1[7], "12"),
                               Edge(extra_events[2], basic_events_e1[8], "13")])
        self.sft_to_pid_converter = SFTToPIDConverter([self.sft_e1])
        inputs1, outputs1, pipes1 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(self.gate_e1)
        self.sft_to_pid_converter.node_counter = 1
        inputs2, outputs2, pipes2 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(extra_events[0])
        self.sft_to_pid_converter.node_counter = 1
        inputs3, outputs3, pipes3 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(extra_events[1])
        self.sft_to_pid_converter.node_counter = 1
        inputs4, outputs4, pipes4 = self \
            .sft_to_pid_converter \
            .post_order_depth_first_traverse_element(extra_events[2])
        self.sft_to_pid_converter.node_counter = 1
        self.assertEqual("ACP-NODE1", inputs4)
        self.assertEqual("ACP-NODE2", outputs4)
        self.assertEqual(6, len(pipes4))
        self.assertEqual({("ACP-NODE1", "ACP-DG07-A-D1"),
                          ("ACP-DG07-A-D1", "ACP-NODE2"),
                          ("ACP-NODE1", "ACP-DG08-A-D2"),
                          ("ACP-DG08-A-D2", "ACP-NODE2"),
                          ("ACP-NODE1", "ACP-DG09-A-D3"),
                          ("ACP-DG09-A-D3", "ACP-NODE2")}, pipes4)
        self.assertEqual('ACP-DG05-A-C1', inputs3)
        self.assertEqual('ACP-DG06-A-C3', outputs3)
        self.assertEqual(8, len(pipes3))
        self.assertEqual({('ACP-DG05-A-C1', 'ACP-NODE1'),
                          ('ACP-NODE1', 'ACP-DG07-A-D1'),
                          ('ACP-DG07-A-D1', 'ACP-NODE2'),
                          ('ACP-NODE1', 'ACP-DG08-A-D2'),
                          ('ACP-DG08-A-D2', 'ACP-NODE2'),
                          ('ACP-NODE1', 'ACP-DG09-A-D3'),
                          ('ACP-DG09-A-D3', 'ACP-NODE2'),
                          ('ACP-NODE2', 'ACP-DG06-A-C3')}, pipes3)
        self.assertEqual("ACP-NODE1", inputs2)
        self.assertEqual("ACP-NODE2", outputs2)
        self.assertEqual(14, len(pipes2))
        self.assertEqual({('ACP-NODE1', 'ACP-DG03-A-B1'),
                          ('ACP-DG03-A-B1', 'ACP-NODE2'),
                          ('ACP-NODE1', 'ACP-DG05-A-C1'),
                          ('ACP-DG05-A-C1', 'ACP-NODE3'),
                          ('ACP-NODE3', 'ACP-DG07-A-D1'),
                          ('ACP-DG07-A-D1', 'ACP-NODE4'),
                          ('ACP-NODE3', 'ACP-DG08-A-D2'),
                          ('ACP-DG08-A-D2', 'ACP-NODE4'),
                          ('ACP-NODE3', 'ACP-DG09-A-D3'),
                          ('ACP-DG09-A-D3', 'ACP-NODE4'),
                          ('ACP-NODE4', 'ACP-DG06-A-C3'),
                          ('ACP-DG06-A-C3', 'ACP-NODE2'),
                          ('ACP-NODE1', 'ACP-DG04-A-B3'),
                          ('ACP-DG04-A-B3', 'ACP-NODE2')}, pipes2)
        self.assertEqual('ACP-DG01-A-A1', inputs1)
        self.assertEqual('ACP-DG02-A-A3', outputs1)
        self.assertEqual(16, len(pipes1))
        self.assertEqual({('ACP-DG01-A-A1', 'ACP-NODE1'),
                          ('ACP-NODE1', 'ACP-DG03-A-B1'),
                          ('ACP-DG03-A-B1', 'ACP-NODE2'),
                          ('ACP-NODE1', 'ACP-DG05-A-C1'),
                          ('ACP-DG05-A-C1', 'ACP-NODE3'),
                          ('ACP-NODE3', 'ACP-DG07-A-D1'),
                          ('ACP-DG07-A-D1', 'ACP-NODE4'),
                          ('ACP-NODE3', 'ACP-DG08-A-D2'),
                          ('ACP-DG08-A-D2', 'ACP-NODE4'),
                          ('ACP-NODE3', 'ACP-DG09-A-D3'),
                          ('ACP-DG09-A-D3', 'ACP-NODE4'),
                          ('ACP-NODE4', 'ACP-DG06-A-C3'),
                          ('ACP-DG06-A-C3', 'ACP-NODE2'),
                          ('ACP-NODE1', 'ACP-DG04-A-B3'),
                          ('ACP-DG04-A-B3', 'ACP-NODE2'),
                          ('ACP-NODE2', 'ACP-DG02-A-A3')}, pipes1)


if __name__ == '__main__':
    unittest.main()

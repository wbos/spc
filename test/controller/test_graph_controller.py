"""
This file contains a class for testing graph_computation.py
"""
import unittest

from test.util.graph_test_helpers import setup_graph_vertex_edge_vocabularies, \
    fill_graphs_bare
from src.model.graph import Graph, Vertex, Edge
from src.controller.graph_controller import union, subtraction, intersection, union_graph_edges


class GraphComputationTest(unittest.TestCase):
    """Class for testing graph_computation.py"""

    def setUp(self) -> None:
        """Sets up the test cases for graph_computation.py"""
        self.graphs, self.vertices, self.edges = \
            setup_graph_vertex_edge_vocabularies()
        fill_graphs_bare(self.graphs, self.vertices, self.edges)

    def fill_graphs_for_substract_and_intersect(self) -> None:
        """Extra setup for test cases substraction and intersection"""
        self.graphs["a_test"].add_vertex(self.vertices["c2"])
        self.graphs["a_test"].add_edge(self.edges["bc2"])
        self.graphs["a_test"].add_edge(self.edges["ac2"])

        self.graphs["b_test"].add_vertex(self.vertices["b2"])
        self.graphs["b_test"].add_edge(self.edges["b2c"])
        self.graphs["b_test"].add_edge(self.edges["b2d"])

    def test_union(self) -> None:
        """Tests the union() of graph_computation.py"""
        graph = union(self.graphs["a_test"], self.graphs["b_test"])

        self.assertEqual(len(graph.vertices), 4)
        self.assertEqual(len(graph.edges), 2)
        self.assertEqual(len(set(graph.vertices)
                             .difference({self.vertices["a"],
                                          self.vertices["b"],
                                          self.vertices["c"]})), 1)
        self.assertEqual(len(set(graph.edges).difference({self.edges["ab"],
                                                          self.edges[
                                                              "bc"]})), 1)

    def test_union2(self) -> None:
        """Tests the union() of graph_computation.py"""
        self.graphs["a_test"].add_vertex(self.vertices["c2"])
        self.graphs["a_test"].add_edge(self.edges["bc2"])
        self.graphs["a_test"].add_edge(self.edges["ac2"])

        self.graphs["b_test"].add_vertex(self.vertices["d"])
        self.graphs["b_test"].add_edge(self.edges["b2d"])

        graph = union(self.graphs["a_test"], self.graphs["b_test"])

        self.assertEqual(len(graph.vertices), 4)
        self.assertEqual(len(graph.edges), 5)
        self.assertEqual(len(set(graph.vertices).difference(
            {self.vertices["a"], self.vertices["b"], self.vertices["c"],
             self.vertices["d"]})), 0)
        self.assertEqual(len(set(graph.edges).difference(
            {self.edges["ab"], self.edges["ac"], self.edges["bc"],
             self.edges["b2d"]})), 1)

    def test_subtraction(self) -> None:
        """Tests the subtraction() of graph_computation.py"""
        self.fill_graphs_for_substract_and_intersect()

        graph = subtraction(self.graphs["a_test"], self.graphs["b_test"])

        self.assertEqual(len(graph.vertices), 1)
        self.assertEqual(len(graph.edges), 2)
        self.assertEqual(len(set(graph.vertices).difference({self.vertices["a"]})), 0)
        self.assertEqual(len(set(graph.edges).difference({self.edges["ab"], self.edges["ac"]})), 0)

    def test_intersection(self) -> None:
        """Tests the intersection() of graph_computation.py"""
        self.fill_graphs_for_substract_and_intersect()

        graph = intersection(self.graphs["a_test"], self.graphs["b_test"])

        self.assertEqual(len(graph.vertices), 2)
        self.assertEqual(len(graph.edges), 1)
        self.assertEqual(len(set(graph.vertices).difference({self.vertices["b"],
                                                             self.vertices["c"]})), 0)
        self.assertEqual(len(set(graph.edges).difference({self.edges["bc"]})), 0)

    def test_union_graph_edges(self) -> None:
        """Tests the union_graph_edges method of graph_controller.py"""
        res_graph = Graph(False, "res_graph")
        graph_a = Graph(False, "a_test")
        graph_b = Graph(False, "b_test")
        vertex_a = Vertex("a", graph_b,{})
        vertex_b = Vertex("b", graph_b,{})
        vertex_c = Vertex("c", graph_a, {})
        vertex_d = Vertex("d", graph_a, {})
        vertex_e = Vertex("a", res_graph, {})
        vertex_f = Vertex("b", res_graph, {})
        vertex_g = Vertex("c", res_graph, {})
        vertex_h = Vertex("d", res_graph, {})
        edge_a = Edge(vertex_a, vertex_b, "0")
        edge_b = Edge(vertex_c, vertex_d, "1")
        graph_b.add_vertices([vertex_a, vertex_b])
        graph_a.add_vertices([vertex_c, vertex_d])
        res_graph.add_vertices([vertex_e, vertex_f, vertex_g, vertex_h])
        graph_b.add_edge(edge_a)
        graph_a.add_edge(edge_b)
        union_graph_edges(res_graph, graph_a, graph_b)
        self.assertEqual(graph_b.edges[0].head.vertex_id, res_graph.edges[1].head.vertex_id)
        self.assertEqual(graph_b.edges[0].tail.vertex_id, res_graph.edges[1].tail.vertex_id)
        self.assertEqual(graph_a.edges[0].tail.vertex_id, res_graph.edges[0].tail.vertex_id)
        self.assertEqual(graph_a.edges[0].tail.vertex_id, res_graph.edges[0].tail.vertex_id)


if __name__ == '__main__':
    unittest.main()

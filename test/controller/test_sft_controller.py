"""
This file contains a class for testing sft_controller.py
"""
import unittest
from typing import List, Set

from src.model.graph import Edge
from src.controller.sft_controller import SFTController, deep_copy_sft_gates, \
    deep_copy_sft_basic_events, union
from src.model.sft import SFT, Gate, BasicEvent


class SFTControllerTest(unittest.TestCase):
    """Class for testing sft_controller.py"""

    def setUp(self) -> None:
        """Sets up the test cases for sft_controller.py"""
        sfts = [SFT("ECC"),
                SFT("ECC-1"),
                SFT("ECC-2"),
                SFT("MFW-133"),
                SFT("MFW-233"),
                SFT("MFW")]
        sfts[0].add_gate(Gate("ECC", sfts[0]))
        sfts[1].add_gates([Gate("ECC-1", sfts[1]),
                           Gate("ECC-dofijco", sfts[1])])
        sfts[2].add_gates([Gate("ECC-2", sfts[2]),
                           Gate("ECC-5857383", sfts[2])])
        sfts[3].add_gate(Gate("MFW-133", sfts[3]))
        sfts[4].add_gate(Gate("MFW-233", sfts[4]))
        sfts[5].add_gates([Gate("MFW", sfts[5]),
                           Gate("MFW-133", sfts[5]),
                           Gate("MFW-233", sfts[5])])
        sfts[0].add_basic_event(BasicEvent("ECC-TK03", sfts[0]))
        sfts[1].add_basic_events([BasicEvent("ECC-TK01", sfts[1]),
                                  BasicEvent("ECC-TK02", sfts[1])])
        sfts[2].add_basic_events([BasicEvent("ECC-VC01", sfts[2]),
                                  BasicEvent("ECC-VC02", sfts[2])])
        sfts[3].add_basic_event(BasicEvent("MFW-PM01", sfts[3]))
        sfts[4].add_basic_event(BasicEvent("MFW-PM02", sfts[4]))
        sfts[0].sft_attributes.top_event = sfts[0].find_gate("ECC")
        sfts[1].sft_attributes.top_event = sfts[1].find_gate("ECC-1")
        sfts[2].sft_attributes.top_event = sfts[2].find_gate("ECC-2")
        sfts[3].sft_attributes.top_event = sfts[3].find_gate("MFW-133")
        sfts[4].sft_attributes.top_event = sfts[4].find_gate("MFW-233")
        sfts[5].sft_attributes.top_event = sfts[5].find_gate("MFW")
        sfts[0].add_edge(Edge(sfts[0].find_gate("ECC"),
                              sfts[0].find_basic_event("ECC-TK03"),
                              "0"))
        sfts[1].add_edges([Edge(sfts[1].find_gate("ECC-1"),
                                sfts[1].find_basic_event("ECC-TK01"),
                                "0"),
                           Edge(sfts[1].find_gate("ECC-1"),
                                sfts[1].find_gate("ECC-dofijco"),
                                "2")])
        sfts[2].add_edge(Edge(sfts[2].find_gate("ECC-2"),
                              sfts[2].find_basic_event("ECC-VC01"),
                              "0"))
        sfts[3].add_edge(Edge(sfts[3].find_gate("MFW-133"),
                              sfts[3].find_basic_event("MFW-PM01"),
                              "0"))
        sfts[4].add_edge(Edge(sfts[4].find_gate("MFW-233"),
                              sfts[4].find_basic_event("MFW-PM02"),
                              "0"))
        sfts[5].add_edges([Edge(sfts[5].find_gate("MFW"),
                                sfts[5].find_gate("MFW-133"),
                                "0"),
                           Edge(sfts[5].find_gate("MFW"),
                                sfts[5].find_gate("MFW-233"),
                                "1")])
        self.sft_controller1 = SFTController(sfts)

    def test_sfts(self) -> None:
        """Tests the sfts property of the SFTController class"""
        self.assertEqual(6, len(self.sft_controller1.sfts))
        self.assertEqual("ECC", self.sft_controller1.sfts[0].graph_id)
        self.assertEqual("ECC-1", self.sft_controller1.sfts[1].graph_id)
        self.assertEqual("ECC-2", self.sft_controller1.sfts[2].graph_id)
        self.assertEqual("MFW-133", self.sft_controller1.sfts[3].graph_id)
        self.sft_controller1.sfts = [SFT("RHR")]
        self.assertEqual(1, len(self.sft_controller1.sfts))
        self.assertEqual("RHR", self.sft_controller1.sfts[0].graph_id)

    def test_pre_process_sfts(self) -> None:
        """Tests the pre_process_sfts method of the SFTController class"""
        self.sft_controller1.pre_process_sfts()
        self.assertEqual(4, len(self.sft_controller1.sfts))
        self.assertEqual("ECC",
                         self.sft_controller1.sfts[0].sft_attributes.top_event.vertex_id)
        self.assertEqual("ECC-1",
                         self.sft_controller1.sfts[1].sft_attributes.top_event.vertex_id)
        self.assertEqual("ECC-2",
                         self.sft_controller1.sfts[2].sft_attributes.top_event.vertex_id)
        self.assertEqual("MFW",
                         self.sft_controller1.sfts[3].sft_attributes.top_event.vertex_id)
        self.assertEqual(1, len(self.sft_controller1.sfts[0].sft_attributes.gates))
        self.assertEqual(2, len(self.sft_controller1.sfts[1].sft_attributes.gates))
        self.assertEqual(1, len(self.sft_controller1.sfts[2].sft_attributes.gates))
        self.assertEqual(3, len(self.sft_controller1.sfts[3].sft_attributes.gates))
        self.assertEqual(1, len(self.sft_controller1.sfts[0].sft_attributes.basic_events))
        self.assertEqual(1, len(self.sft_controller1.sfts[1].sft_attributes.basic_events))
        self.assertEqual(1, len(self.sft_controller1.sfts[2].sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[3].sft_attributes.basic_events))
        self.assertEqual(1, len(self.sft_controller1.sfts[0].edges))
        self.assertEqual(2, len(self.sft_controller1.sfts[1].edges))
        self.assertEqual(1, len(self.sft_controller1.sfts[2].edges))
        self.assertEqual(4, len(self.sft_controller1.sfts[3].edges))

    def test_check_sfts_for_internal_transfer_gates(self) -> None:
        """Tests the check_sfts_for_internal_transfer_gates method of the SFTController class"""
        self.sft_controller1.check_sfts_for_internal_transfer_gates()
        self.assertEqual(4, len(self.sft_controller1.sfts))
        self.assertEqual("ECC",
                         self.sft_controller1.sfts[0].sft_attributes.top_event.vertex_id)
        self.assertEqual("ECC-1",
                         self.sft_controller1.sfts[1].sft_attributes.top_event.vertex_id)
        self.assertEqual("ECC-2",
                         self.sft_controller1.sfts[2].sft_attributes.top_event.vertex_id)
        self.assertEqual("MFW",
                         self.sft_controller1.sfts[3].sft_attributes.top_event.vertex_id)
        self.assertEqual(1, len(self.sft_controller1.sfts[0].sft_attributes.gates))
        self.assertEqual(2, len(self.sft_controller1.sfts[1].sft_attributes.gates))
        self.assertEqual(2, len(self.sft_controller1.sfts[2].sft_attributes.gates))
        self.assertEqual(3, len(self.sft_controller1.sfts[3].sft_attributes.gates))
        self.assertEqual(1, len(self.sft_controller1.sfts[0].sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[1].sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[2].sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[3].sft_attributes.basic_events))
        self.assertEqual(1, len(self.sft_controller1.sfts[0].edges))
        self.assertEqual(2, len(self.sft_controller1.sfts[1].edges))
        self.assertEqual(1, len(self.sft_controller1.sfts[2].edges))
        self.assertEqual(4, len(self.sft_controller1.sfts[3].edges))

    def test_check_sft_for_internal_transfer_gates(self) -> None:
        """Tests the check_sft_for_internal_transfer_gates method of the SFTController class"""
        res_sfts: List[SFT] = []
        del_sfts: Set[str] = set()
        self.sft_controller1 \
            .check_sft_for_internal_transfer_gates(
                res_sfts,
                self.sft_controller1.sfts[5],
                del_sfts,
                {"MFW",
                 "MFW-133",
                 "MFW-233"})
        self.assertEqual(1, len(res_sfts))
        self.assertEqual(2, len(del_sfts))
        self.assertIn("MFW-133", del_sfts)
        self.assertIn("MFW-233", del_sfts)
        self.assertEqual("MFW", res_sfts[0].graph_id)
        self.assertEqual(2, len(res_sfts[0].sft_attributes.basic_events))
        self.assertEqual(3, len(res_sfts[0].sft_attributes.gates))
        self.assertEqual(4, len(res_sfts[0].edges))
        res_sfts: List[SFT] = []
        del_sfts: Set[str] = set()
        self.sft_controller1 \
            .check_sft_for_internal_transfer_gates(
                res_sfts,
                self.sft_controller1.sfts[0],
                del_sfts,
                {"MFW",
                 "MFW-133",
                 "MFW-233"})
        self.assertEqual(0, len(res_sfts))
        self.assertEqual(0, len(del_sfts))

    def test_reduce_sfts(self) -> None:
        """Tests the reduce_sfts method of the SFTController class"""
        self.sft_controller1.reduce_sfts()
        self.assertEqual(1, len(self.sft_controller1.sfts[1]
                                .sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[1]
                                .sft_attributes.gates))
        self.assertEqual(2, len(self.sft_controller1.sfts[1].edges))
        self.assertIn(Gate("ECC-dofijco",
                           self.sft_controller1.sfts[1]),
                      self.sft_controller1.sfts[1].sft_attributes.gates)
        self.assertIn(Gate("ECC-1",
                           self.sft_controller1.sfts[1]),
                      self.sft_controller1.sfts[1].sft_attributes.gates)
        self.assertNotIn(BasicEvent("ECC-TK02",
                                    self.sft_controller1.sfts[1]),
                         self.sft_controller1.sfts[2].sft_attributes.basic_events)
        self.assertIn(BasicEvent("ECC-TK01",
                                 self.sft_controller1.sfts[1]),
                      self.sft_controller1.sfts[1].sft_attributes.basic_events)
        self.assertEqual(1, len(self.sft_controller1.sfts[2]
                                .sft_attributes.basic_events))
        self.assertEqual(1, len(self.sft_controller1.sfts[2]
                                .sft_attributes.gates))
        self.assertNotIn(Gate("ECC-5857383",
                              self.sft_controller1.sfts[2]),
                         self.sft_controller1.sfts[2].sft_attributes.gates)
        self.assertIn(Gate("ECC-2",
                           self.sft_controller1.sfts[2]),
                      self.sft_controller1.sfts[2].sft_attributes.gates)
        self.assertNotIn(BasicEvent("ECC-VC02",
                                    self.sft_controller1.sfts[2]),
                         self.sft_controller1.sfts[2].sft_attributes.basic_events)
        self.assertIn(BasicEvent("ECC-VC01",
                                 self.sft_controller1.sfts[2]),
                      self.sft_controller1.sfts[2].sft_attributes.basic_events)

    def test_reduce_sft(self) -> None:
        """Tests the reduce_sft method of the SFTController class"""
        self.assertEqual(2, len(self.sft_controller1.sfts[1]
                                .sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[1]
                                .sft_attributes.gates))
        self.assertEqual(2, len(self.sft_controller1.sfts[1].edges))
        self.sft_controller1.reduce_sft(self.sft_controller1.sfts[1],
                                        True)
        self.assertEqual(1, len(self.sft_controller1.sfts[1]
                                .sft_attributes.basic_events))
        self.assertEqual(1, len(self.sft_controller1.sfts[1]
                                .sft_attributes.gates))
        self.assertEqual(1, len(self.sft_controller1.sfts[1].edges))
        self.assertNotIn(Gate("ECC-dofijco",
                              self.sft_controller1.sfts[1]),
                         self.sft_controller1.sfts[1].sft_attributes.gates)
        self.assertIn(Gate("ECC-1",
                           self.sft_controller1.sfts[1]),
                      self.sft_controller1.sfts[1].sft_attributes.gates)
        self.assertNotIn(BasicEvent("ECC-TK02",
                                    self.sft_controller1.sfts[1]),
                         self.sft_controller1.sfts[2].sft_attributes.basic_events)
        self.assertIn(BasicEvent("ECC-TK01",
                                 self.sft_controller1.sfts[1]),
                      self.sft_controller1.sfts[1].sft_attributes.basic_events)
        # Second case deletes gates without incidence
        self.assertEqual(2, len(self.sft_controller1.sfts[2]
                                .sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[2]
                                .sft_attributes.gates))
        self.assertEqual(1, len(self.sft_controller1.sfts[2].edges))
        self.sft_controller1.reduce_sft(self.sft_controller1.sfts[2],
                                        False)
        self.assertEqual(1, len(self.sft_controller1.sfts[2]
                                .sft_attributes.basic_events))
        self.assertEqual(1, len(self.sft_controller1.sfts[2]
                                .sft_attributes.gates))
        self.assertNotIn(Gate("ECC-5857383",
                              self.sft_controller1.sfts[2]),
                         self.sft_controller1.sfts[2].sft_attributes.gates)
        self.assertIn(Gate("ECC-2",
                           self.sft_controller1.sfts[2]),
                      self.sft_controller1.sfts[2].sft_attributes.gates)
        self.assertNotIn(BasicEvent("ECC-VC02",
                                    self.sft_controller1.sfts[2]),
                         self.sft_controller1.sfts[2].sft_attributes.basic_events)
        self.assertIn(BasicEvent("ECC-VC01",
                                 self.sft_controller1.sfts[2]),
                      self.sft_controller1.sfts[2].sft_attributes.basic_events)

    def test_delete_gates_without_children(self) -> None:
        """Tests the delete_gates_without_children method of the SFTController class"""
        # Two cases. First case deletes gates without children.
        self.assertEqual(2, len(self.sft_controller1.sfts[1]
                                .sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[1]
                                .sft_attributes.gates))
        self.assertEqual(2, len(self.sft_controller1.sfts[1].edges))
        self.sft_controller1.delete_gates_without_children(
            self.sft_controller1.sfts[1]
        )
        self.assertEqual(2, len(self.sft_controller1.sfts[1]
                                .sft_attributes.basic_events))
        self.assertEqual(1, len(self.sft_controller1.sfts[1]
                                .sft_attributes.gates))
        self.assertEqual(1, len(self.sft_controller1.sfts[1].edges))
        self.assertNotIn(Gate("ECC-dofijco",
                              self.sft_controller1.sfts[1]),
                         self.sft_controller1.sfts[1].sft_attributes.gates)
        self.assertIn(Gate("ECC-1",
                           self.sft_controller1.sfts[1]),
                      self.sft_controller1.sfts[1].sft_attributes.gates)

    def test_delete_gates_without_incidence(self) -> None:
        """Tests the delete_gates_without_incidence method of the SFTController class"""
        self.assertEqual(2, len(self.sft_controller1.sfts[2]
                                .sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[2]
                                .sft_attributes.gates))
        self.assertEqual(1, len(self.sft_controller1.sfts[2].edges))
        self.sft_controller1.delete_gates_without_incidence(
            self.sft_controller1.sfts[2]
        )
        self.assertEqual(2, len(self.sft_controller1.sfts[2]
                                .sft_attributes.basic_events))
        self.assertEqual(1, len(self.sft_controller1.sfts[2]
                                .sft_attributes.gates))
        self.assertNotIn(Gate("ECC-5857383",
                              self.sft_controller1.sfts[2]),
                         self.sft_controller1.sfts[2].sft_attributes.gates)
        self.assertIn(Gate("ECC-2",
                           self.sft_controller1.sfts[2]),
                      self.sft_controller1.sfts[2].sft_attributes.gates)

    def test_delete_basic_events_without_incidence(self) -> None:
        """Tests the delete_basic_events_without_incidence method of the SFTController class"""
        self.assertEqual(2, len(self.sft_controller1.sfts[2]
                                .sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[2]
                                .sft_attributes.gates))
        self.assertEqual(1, len(self.sft_controller1.sfts[2].edges))
        self.sft_controller1.delete_basic_events_without_incidence(
            self.sft_controller1.sfts[2]
        )
        self.assertEqual(1, len(self.sft_controller1.sfts[2]
                                .sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[2]
                                .sft_attributes.gates))
        self.assertNotIn(BasicEvent("ECC-VC02",
                                    self.sft_controller1.sfts[2]),
                         self.sft_controller1.sfts[2].sft_attributes.basic_events)
        self.assertIn(BasicEvent("ECC-VC01",
                                 self.sft_controller1.sfts[2]),
                      self.sft_controller1.sfts[2].sft_attributes.basic_events)

    def test_union(self) -> None:
        """Tests the union method of the SFTController class"""
        res_sft = union(self.sft_controller1.sfts[5],
                        self.sft_controller1.sfts[3])
        self.assertEqual(3, len(res_sft.sft_attributes.gates))
        self.assertEqual(1, len(res_sft.sft_attributes.basic_events))
        self.assertEqual(3, len(res_sft.edges))
        self.assertEqual(3, len(self.sft_controller1.sfts[5].sft_attributes.gates))
        self.assertEqual(0, len(self.sft_controller1.sfts[5].sft_attributes.basic_events))
        self.assertEqual(2, len(self.sft_controller1.sfts[5].edges))
        self.assertEqual(1, len(self.sft_controller1.sfts[3].sft_attributes.gates))
        self.assertEqual(1, len(self.sft_controller1.sfts[3].sft_attributes.basic_events))
        self.assertEqual(1, len(self.sft_controller1.sfts[3].edges))

    def test_deep_copy_sft_basic_events(self) -> None:
        """Tests the deep_copy_sft_basic_events method of the SFTController class"""
        res_sft = SFT("MFW")
        self.assertEqual(0, len(res_sft.sft_attributes.basic_events))
        deep_copy_sft_basic_events(res_sft,
                                   self.sft_controller1.sfts[3],
                                   self.sft_controller1.sfts[4])
        self.assertEqual(2, len(res_sft.sft_attributes.basic_events))
        self.assertEqual(1,
                         len(self.sft_controller1.sfts[3].sft_attributes.basic_events))
        self.assertEqual(1,
                         len(self.sft_controller1.sfts[4].sft_attributes.basic_events))
        self.assertIn(BasicEvent("MFW-PM01", res_sft), res_sft.sft_attributes.basic_events)
        self.assertIn(BasicEvent("MFW-PM02", res_sft), res_sft.sft_attributes.basic_events)

    def test_deep_copy_sft_gates(self) -> None:
        """Tests the deep_copy_sft_basic_events method of the SFTController class"""
        res_sft = SFT("MFW")
        self.assertEqual(0, len(res_sft.sft_attributes.gates))
        deep_copy_sft_gates(res_sft,
                            self.sft_controller1.sfts[3],
                            self.sft_controller1.sfts[4])
        self.assertEqual("MFW-133", res_sft.sft_attributes.top_event.vertex_id)
        self.assertEqual(2, len(res_sft.sft_attributes.gates))
        self.assertEqual(Gate("MFW-233", res_sft), res_sft.find_gate("MFW-233"))
        self.assertEqual(1,
                         len(self.sft_controller1.sfts[3].sft_attributes.gates))
        self.assertEqual(1,
                         len(self.sft_controller1.sfts[4].sft_attributes.gates))

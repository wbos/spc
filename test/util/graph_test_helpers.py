"""
This file contains helper methods for our testing classes
"""
import sys
from contextlib import contextmanager
from io import StringIO

from src.model.graph import Graph, Edge, Vertex


def setup_graph_vertex_edge_vocabularies() -> (dict[str, Graph],
                                               dict[str, Vertex], dict[str, Edge]):
    """Sets up a vocabulary of graphs, vertices and edges to use in tests."""
    graphs = {"g_test": Graph(directed=True, graph_id="g_test"),
              "a_test": Graph(directed=False, graph_id="a_test"),
              "b_test": Graph(directed=True, graph_id="b_test")}

    vertices = {"a": Vertex(vertex_id="A", attributes={"name": "A"},
                            graph=graphs["a_test"]),
                "a2": Vertex(vertex_id="A", attributes={"name": "A"},
                             graph=graphs["b_test"]),
                "a3": Vertex(vertex_id="A", attributes={"name": "A"},
                             graph=graphs["g_test"]),
                "b": Vertex(vertex_id="B", attributes={"name": "B"},
                            graph=graphs["a_test"]),
                "b2": Vertex(vertex_id="B", attributes={"name": "B"},
                             graph=graphs["b_test"]),
                "b2+desc": Vertex(vertex_id="B",
                                  attributes={"name": "B",
                                              "description": "aaa"},
                                  graph=graphs["b_test"]),
                "b3": Vertex(vertex_id="B", attributes={"name": "B"},
                             graph=graphs["g_test"]),
                "c": Vertex(vertex_id="C", attributes={"name": "C"},
                            graph=graphs["b_test"]),
                "c2": Vertex(vertex_id="C", attributes={"name": "C"},
                             graph=graphs["a_test"]),
                "c3": Vertex(vertex_id="C", attributes={"name": "C"},
                             graph=graphs["g_test"]),
                "d": Vertex(vertex_id="D", attributes={"name": "D"},
                            graph=graphs["b_test"])}

    edges = {
        "ab": Edge(edge_id="AB", head=vertices["a"],
                   tail=vertices["b"]),
        "ac": Edge(edge_id="AC", head=vertices["a"],
                   tail=vertices["c"]),
        "ac2": Edge(edge_id="AC", head=vertices["a"],
                    tail=vertices["c2"]),
        "a2b2": Edge(edge_id="AB", head=vertices["a2"],
                     tail=vertices["b2"]),
        "a3b3": Edge(edge_id="AB", head=vertices["a3"],
                     tail=vertices["b3"]),
        "cd": Edge(edge_id="CD", head=vertices["c"],
                   tail=vertices["d"]),
        "bc": Edge(edge_id="BC", head=vertices["b"],
                   tail=vertices["c"]),
        "b2c": Edge(edge_id="BC", head=vertices["b2"],
                    tail=vertices["c"]),
        "b2c+desc": Edge(edge_id="BC", head=vertices["b2"],
                         tail=vertices["c"]),
        "bc2": Edge(edge_id="BC", head=vertices["b"],
                    tail=vertices["c2"]),
        "b3c3": Edge(edge_id="BC", head=vertices["b3"],
                     tail=vertices["c3"]),
        "b2d": Edge(edge_id="BD", head=vertices["b2"],
                    tail=vertices["d"]),
        "ad": Edge(edge_id="AD", head=vertices["a"], tail=vertices["d"]),
    }

    return graphs, vertices, edges


def fill_graphs_bare(graphs: dict[str, Graph],
                     vertices: dict[str, Vertex],
                     edges: dict[str, Edge]) -> None:
    """Fills a minimum of vertices and edges into graphs"""
    graphs["a_test"].add_vertex(vertices["a"])
    graphs["a_test"].add_vertex(vertices["b"])
    graphs["a_test"].add_edge(edges["ab"])

    graphs["b_test"].add_vertex(vertices["c"])
    graphs["b_test"].add_vertex(vertices["d"])
    graphs["b_test"].add_edge(edges["cd"])


@contextmanager
def captured_output():
    """Captures the output of stout and sterr so that we can assert what is
    printed there."""
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

"""
Class to test the utils.py file
"""
import unittest

from src.util.utils import string_starts_with_t1_t2_t3_or, append_to_list_if_not_present, string_strip_startswith_mb_tag


class UtilsTest(unittest.TestCase):
    """Class to test the methods in the utils.py file."""

    def setUp(self) -> None:
        """Sets up this class for our test cases."""

    def test_string_starts_with_t1_t2_t3_or(self):
        """Tests the string_starts_with_t1_t2_t3_or method."""
        self.assertTrue(string_starts_with_t1_t2_t3_or("T1"))
        self.assertTrue(string_starts_with_t1_t2_t3_or("T2"))
        self.assertTrue(string_starts_with_t1_t2_t3_or("T3"))
        self.assertFalse(string_starts_with_t1_t2_t3_or("t3"))
        self.assertFalse(string_starts_with_t1_t2_t3_or("t2"))
        self.assertFalse(string_starts_with_t1_t2_t3_or("t1"))

    def test_append_to_list_if_not_present(self):
        """Tests the append_to_list_if_not_present method."""
        test_list = []
        self.assertEqual(0, len(test_list))
        append_to_list_if_not_present(test_list, "x")
        self.assertEqual(1, len(test_list))
        self.assertEqual("x", test_list[0])
        append_to_list_if_not_present(test_list, "x")
        self.assertEqual(1, len(test_list))
        self.assertEqual("x", test_list[0])
        append_to_list_if_not_present(test_list, "y")
        self.assertEqual(2, len(test_list))
        self.assertEqual("x", test_list[0])
        self.assertEqual("y", test_list[1])

    def test_string_strip_startswith_mb_tag(self):
        """Tests the string_strip_startswith_mb_tag method."""
        test_string1 = string_strip_startswith_mb_tag("MB-AAB")
        test_string2 = string_strip_startswith_mb_tag("AAA")
        self.assertEqual("AAB", test_string1)
        self.assertEqual("AAA", test_string2)


if __name__ == '__main__':
    unittest.main()

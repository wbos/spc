"""
Setup.py for package information. Contains metadata about the python package.
"""

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="spc",
    version="0.0.1",
    author="Wouter Bos",
    author_email="w.f.a.bos@student.utwente.nl",
    description="Python repo for SPC",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.utwente.nl/wouwouwou/spc",
    project_urls={
        "Bug Tracker": "https://gitlab.utwente.nl/wouwouwou/spc/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU GPLv3",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.9",
)

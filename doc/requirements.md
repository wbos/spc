# MoSCoW Requirements SPC

Below you can find the requirements for the SPC. They are prioritized with the MoSCoW principle. Note that this is a living document and hence, subject to change.


## Must
- The SPC must be able to import .RSA filetypes
- The SPC must be able to convert an SFT to a P&ID

## Should
- The SPC should handle the "MB-" prefix 
- The SPC should handle the "T1", "T2" and "T3" prefixes.


## Could
- The SPC could be able to export P&IDs to a .gv fileforma.
- The SPC could be able to export SFTs to a .gv fileformat.

## Won't
- The SPC won't be able to import SFTs of P&IDs based on a different KB than the EXPSA one.
- The SPC won't be able to adapt P&IDs such that they conform to the EXPSA metamodel.
- The SPC won't be able to adapt SFTs fo P&IDs such that they conform to the EXPSA metamodel.



# SPC

This is a Python repository for the SFT to P&ID Converter (SPC).
The SPC converts Static Fault Trees to Piping & Instrumentation Diagrams.
Details about the SPC are publicly available as part of a [Masters' Thesis](https://essay.utwente.nl/view/programme/60300.html#group_2024).
## Installation

- Install [Python 3.10](https://www.python.org/downloads/release/python-31013/)
- Install [pip](https://pip.pypa.io/en/stable/installation/)
- Install [pipenv](https://pypi.org/project/pipenv/)
## Run Locally

Clone the project.

```bash
  git clone https://link-to-project
```

Install a version of Python, pip and pipenv.
Then, go to the project directory.

```bash
  cd my-project
```

Install dependencies.

```bash
  pipenv install
```

Run the program to gegenerate P&IDs from SFTs.

```bash
  python ./src/main.py
```


## Running Tests

To run tests, run the following command

```bash
  pytest ./test
```


## Authors

- [@wbos](https://www.gitlab.com/wbos)


## Acknowledgements

 - Prof. dr. Mariëlle I. A. Stoelinga
 - Prof. Anne K. I. Remke
 - Dr. Matthias Volk
 - Dr. Pavel Krčál
 - Marc Bouissou, HDR

## License

[EUPL-1.2](https://choosealicense.com/licenses/eupl-1.2/)


## FAQ

#### How can I add more SFTs to generate P&IDs from?

Currently, the SPC transforms SFTs described in RSA files in the `resources/SFTs` directory to KBI files in the `resources/PIDs_results_generated` folder. Simply add more RSA files to `resources/SFTs` to transform them.

### What is the difference between KBI and KBE files?

KBE files correspond to full studies within RiskSpectrum ModelBuilder (RSMB), whereas KBI files only import "pages" into a single study. The XML schema for both file types are significantly different. The SPC can only export to KBI files.
